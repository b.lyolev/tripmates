/** @type {import('tailwindcss').Config} */

// const defaultTheme = require('tailwindcss/defaultTheme');
export default {
  content: [
    './index.html',
    './src/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{html,tsx,ts,jsx,js}',
    './src/components/**/*.{html,tsx,ts,jsx,js}',
    './node_modules/react-tailwindcss-datepicker/dist/index.esm.js',
  ],
  darkMode: 'class',
  theme: {
    extend: {
      screens: {
        'sm-custom': '680px', 
        'md-custom': '900px',
        'xm': '560px',
        'xmm': '450px'  
      },
    },
  },
  plugins: [],
};
