// useAuthData.js
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { getUserData } from '../services/user.service';
import { setUser, setUserData } from '../reducers/authActions';

const useAuthData = (userLoaded) => {
  const dispatch = useDispatch();

  useEffect(() => {
    if (userLoaded) {
      dispatch(setUser(userLoaded));
    }

    if (userLoaded && userLoaded.uid) {
      getUserData(userLoaded.uid)
        .then((snapshot) => {
          if (snapshot.exists()) {
            dispatch(
              setUserData(snapshot.val()[Object.keys(snapshot.val())[0]])
            );
          }
        })
        .catch((e) => alert(e.message));
    }
  }, [userLoaded, dispatch]);

  const user = useSelector((state) => state.auth.user);
  const userData = useSelector((state) => state.auth.userData);

  return { user, userData };
};

export default useAuthData;
