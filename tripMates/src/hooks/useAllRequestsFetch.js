import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import {
  getAllFriendRequests,
  getAllFriends,
  getSentRequests,
} from '../services/contacts.service';
import {
  setUserRequests,
  setUserSentRequests,
  setUserFriends,
} from '../reducers/authActions';

const useAllUsersFetching = (userData) => {
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchRequests = async () => {
      getAllFriendRequests(userData?.handle, (friendRequests) => {
        dispatch(setUserRequests(friendRequests));
      });

      getSentRequests(userData?.handle, (sentRequests) => {
        dispatch(setUserSentRequests(sentRequests));
      });

      getAllFriends(userData?.handle, (friends) => {
        dispatch(setUserFriends(friends));
      });
    };

    fetchRequests();
  }, [dispatch, userData]);
};
export default useAllUsersFetching;
