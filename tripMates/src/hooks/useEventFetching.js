import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { getAllEvents } from '../services/event.service';
import { setAllEvents } from '../reducers/eventActions';

// Define the generateRecurringOccurrences function
function generateRecurringOccurrences(event, numOccurrences) {
  const occurrences = [];
  const oneDay = 24 * 60 * 60 * 1000; // One day in milliseconds
  const oneWeek = 7 * oneDay; // One week in milliseconds
  const oneMonth = 30 * oneDay; // Approximately one month in milliseconds

  // Start generating occurrences from the event's start date
  let currentStart = new Date(event.startDate);
  let currentEnd = new Date(event.endDate);

  for (let i = 0; i < numOccurrences; i++) {
    occurrences.push({
      ...event,
      startDate: currentStart.getTime(),
      endDate: currentEnd.getTime(),
    });

    // Move to the next recurrence based on the recurrence type
    if (event.eventRecurrence === 'daily') {
      currentStart = new Date(currentStart.getTime() + oneDay);
      currentEnd = new Date(currentEnd.getTime() + oneDay);
    } else if (event.eventRecurrence === 'weekly') {
      currentStart = new Date(currentStart.getTime() + oneWeek);
      currentEnd = new Date(currentEnd.getTime() + oneWeek);
    } else if (event.eventRecurrence === 'monthly') {
      currentStart = new Date(currentStart.getTime() + oneMonth);
      currentEnd = new Date(currentEnd.getTime() + oneMonth);
    }
  }

  return occurrences;
}

const useEventFetching = (userData) => {
  const dispatch = useDispatch();

  useEffect(() => {
      // console.log('Fetching events...');
      const unsubscribe = getAllEvents((allEvents) => {
        const updatedEvents = [];

        // console.log(allEvents);
        allEvents.forEach((event) => {
          // console.log(event.title);
          const recurrenceType = event.eventRecurrence || 'none';
          const repeat = event.eventRepeat || 1;

          if (
            recurrenceType === 'daily' ||
            recurrenceType === 'weekly' ||
            recurrenceType === 'monthly'
          ) {
            const recurringOccurrences = generateRecurringOccurrences(
              event,
              repeat
            );
            updatedEvents.push(...recurringOccurrences);
          } else {
            // console.log(event.title);
            updatedEvents.push(event);
          }
        });

        // console.log(updatedEvents);
        dispatch(setAllEvents(updatedEvents));
      });

      return () => {
        unsubscribe();
      };
    }
, [userData, dispatch]);
};

export default useEventFetching;
