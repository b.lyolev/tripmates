import { useState } from 'react';
import { ref, get, push, remove, update, set } from 'firebase/database';
import { database } from '../config/firebase-config.js';

export const useDB = () => {
  const [error, setError] = useState(null);

  const setDB = async (path, data) => {
    try {
      const reference = ref(database, path);
      await set(reference, data);
      console.log('Data set successfully.');
    } catch (error) {
      console.error('Error setting data:', error);
      setError(error);
    }
  };

  const getDB = async (path, ...constraints) => {
    try {
      const reference = ref(database, path);
      let snapshot;

      if (constraints.length > 0) {
        snapshot = await get(reference, ...constraints);
      } else {
        snapshot = await get(reference);
      }

      if (snapshot.exists()) {
        const val = snapshot.val();
        console.log('Retrieved data:', val);
        return val;
      } else {
        console.log('No data available.');
        return null;
      }
    } catch (error) {
      console.error('Error retrieving data:', error);
      setError(error);
    }
  };

  const pushDB = async (path, data) => {
    try {
      const reference = ref(database, path);
      const key = (await push(reference, data)).key;
      console.log('Data pushed successfully.');
      return key;
    } catch (error) {
      console.error('Error pushing data:', error);
      setError(error);
    }
  };

  const removeDB = async (path) => {
    try {
      const reference = ref(database, path);
      await remove(reference);
      console.log('Data removed successfully.');
    } catch (error) {
      console.error('Error removing data:', error);
      setError(error);
    }
  };

  const updateDB = async (path, newData) => {
    try {
      const reference = ref(database, path);
      await update(reference, newData);
      console.log('Data updated successfully.');
    } catch (error) {
      console.error('Error updating data:', error);
      setError(error);
    }
  };

  return {
    setDB,
    getDB,
    pushDB,
    removeDB,
    updateDB,
    error,
  };
};
