import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { getAllUsers } from '../services/user.service';
import { setAllUsers } from '../reducers/usersActions';

const useAllUsersFetching = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchUsers = async () => {
      const users = await getAllUsers();
      dispatch(setAllUsers(users));
    };

    fetchUsers();
  }, [dispatch]);
};

export default useAllUsersFetching;
