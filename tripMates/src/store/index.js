import { configureStore } from '@reduxjs/toolkit';
import authReducer from '../reducers/authReducer';
import eventsReducer from '../reducers/eventsReducer';
import {
  filtersReducer,
  priceRangeReducer,
  sortReducer,
} from '../reducers/filterReducers';
import usersReducer from '../reducers/usersReducer';
import selectedDateReducer from '../reducers/selectedDateReducer';
import { currentWeekReducer } from '../reducers/currentWeekReducer';
import viewsReducer from '../reducers/viewReducer';

const store = configureStore({
  reducer: {
    auth: authReducer,
    events: eventsReducer,
    filters: filtersReducer,
    priceRange: priceRangeReducer,
    sort: sortReducer,
    allUsers: usersReducer,
    selectedDate: selectedDateReducer,
    currentWeek: currentWeekReducer,
    currentView: viewsReducer,
  },
});

export default store;
