import Logo from '../../components/Logo/Logo';

export default function InvalidPage() {
  return (
    <>
      <div className='min-h-full pt-16 pb-12 flex flex-col'>
        <main className='flex-grow flex flex-col justify-center max-w-7xl w-full mx-auto px-4 sm:px-6 lg:px-8'>
          <div className='py-16'>
            <div className='text-center'>
              <p className='text-4xl font-semibold text-blue-600 dark:text-[#c2df2b] uppercase tracking-wide'>
                404 error
              </p>
              <h1 className='mt-2 text-4xl font-extrabold text-gray-900 dark:text-white tracking-tight sm:text-5xl'>
                Page not found.
              </h1>
              <p className='mt-2 text-base text-gray-500 dark:text-gray-400'>
                Sorry, we couldn’t find the page you’re looking for.
              </p>
              <div className='mt-6'>
                <a
                  href='/'
                  className='text-base font-medium text-blue-600 dark:text-[#c2df2b] hover:text-blue-500 dark:hover:text-[#afdf2b] '
                >
                  Go back home<span aria-hidden='true'> &rarr;</span>
                </a>
              </div>
            </div>
          </div>
        </main>
        <footer className='flex-shrink-0 max-w-7xl w-full mx-auto px-4 sm:px-6 lg:px-8'>
          <nav className='flex justify-center space-x-4'>
            <a
              href='#'
              className='text-sm font-medium text-gray-500 dark:text-gray-300 hover:text-gray-600 dark:hover:text-gray-100'
            >
              Contact Support
            </a>
            <span
              className='inline-block border-l border-gray-300'
              aria-hidden='true'
            />
            <a
              href='#'
              className='text-sm font-medium text-gray-500 dark:text-gray-300 hover:text-gray-600 dark:hover:text-gray-100'
            >
              Status
            </a>
            <span
              className='inline-block border-l border-gray-300'
              aria-hidden='true'
            />
            <a
              href='#'
              className='text-sm font-medium text-gray-500 dark:text-gray-300 hover:text-gray-600 dark:hover:text-gray-100'
            >
              Twitter
            </a>
          </nav>
        </footer>
      </div>
    </>
  );
}
