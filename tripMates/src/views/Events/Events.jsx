import React, { useEffect, useState } from 'react';
import EventCard from '../../components/EventCard/EventCard';
import GridCards from '../../components/GridCards/GridCards';
import { motion, AnimatePresence } from 'framer-motion';
import { useSelector } from 'react-redux';
import EventSortBar from '../../components/EventSortBar/EventSortBar';
import { eventImage } from '../../common/constants/constants';

export default function Events() {
  const allEvents = useSelector((state) => state.events.events);

  const userData = useSelector((state) => state.auth.userData);

  const events = allEvents?.filter((event) => {
    // Check if the author handle matches the user handle or if the user handle is in eventParticipants
    return (
      event.authorHandle === userData?.handle ||
      event.eventParticipants.split(',').includes(userData?.handle) ||
      event.eventType === 'public' ||
      userData?.isAdmin
    );
  });

  const filteredEvents = events?.reduce((result, event) => {
    const existingEvent = result.find((e) => e.uid === event.uid);
    if (!existingEvent || event.startDate < existingEvent.startDate) {
      return [...result.filter((e) => e.uid !== event.uid), event];
    }
    return result;
  }, []);

  // console.log(filteredEvents);

  const [sortedEvents, setSortedEvents] = useState(filteredEvents);
  const [showAllEvents, setShowAllEvents] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const [itemsPerPage] = useState(8);
  const [previousSortedEvents, setPreviousSortedEvents] = useState(filteredEvents);

  useEffect(() => {
    setSortedEvents(filteredEvents);
    setPreviousSortedEvents(filteredEvents);
  }, [allEvents]);

  const handleShowAllEvents = () => {
    setSortedEvents(filteredEvents);
    setPreviousSortedEvents(filteredEvents);
  };

  const handleSearch = (query) => {
    let sorted = [...sortedEvents];
    if (query) {
      sorted = previousSortedEvents.filter((event) =>
        event.title.toLowerCase().includes(query.toLowerCase())
      );
      setPreviousSortedEvents(sorted);
    } else {
      sorted = [...filteredEvents];
      setPreviousSortedEvents(filteredEvents); 
    }
    setSortedEvents(sorted);
  };

  const handleSort = (sortBy, sortOrder, eventType) => {
    let sorted = [...sortedEvents];
    if (sortBy === 'date') {
      sorted.sort((a, b) => a.startDate - b.startDate);
    }
    if (sortOrder === 'desc') {
      sorted.reverse();
    }
    if (eventType) {
      sorted = previousSortedEvents.filter(
        (event) => event.eventType === eventType
      );
      setPreviousSortedEvents(sortedEvents);
    }
    setSortedEvents(sorted);
  };


  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentItems = sortedEvents
    ? sortedEvents.slice(indexOfFirstItem, indexOfLastItem)
    : [];

  return (
    <>
      <div className=' pb-32'>
        <header className='py-10'>
          <div className='max-w-7xl mx-auto px-4 sm:px-5 lg:px-8'>
            <h1 className='text-4xl font-bold text-slate-800 dark:text-white text-center'>
              Events
            </h1>
          </div>
        </header>
      </div>

      <div className='border-t-4 border-blue-500 dark:border-[#c2df2b] sm:flex-row  '>
        <main className='-mt-32'>
          <div className='max-w-7xl mx-auto pb-12 px-4 sm:px-6 lg:px-8'>
            <div className='bg-slate-50 dark:bg-[#171717] rounded-2xl shadow border-4  border-blue-500 dark:border-[#c2df2b] p-4'>
              <EventSortBar
                onSearch={handleSearch}
                onSort={handleSort}
                onShowAll={handleShowAllEvents}
              />
              <GridCards>
                <AnimatePresence key='eventsAnimate'>
                  {currentItems?.map((event, i) => (
                    <motion.div
                      key={event.uid}
                      initial={{ opacity: 0 }}
                      animate={{ opacity: 1 }}
                      transition={{ delay: i * 0.1 }}
                    >
                      <EventCard
                        authorUID={event.authorUID}
                        uid={event.uid}
                        authorHandle={event.authorHandle}
                        title={event.title}
                        description={event.description}
                        startDate={event.startDate}
                        endDate={event.endDate}
                        eventType={event.eventType}
                        eventRecurrence={event.eventRecurrence}
                        eventRepeat={event.eventRepeat}
                        color={event.color}
                        eventParticipants={event.eventParticipants}
                        img={event.isHotel ? event.images[0].image : eventImage}
                        isHotel={event.isHotel}
                        eventLocation={event.eventLocation || null}
                        link={event.link || null}
                      />
                    </motion.div>
                  ))}
                </AnimatePresence>
              </GridCards>
              <div className='flex justify-center items-center space-x-2 mt-10'>
                {Array.from(
                  {
                    length: Math.ceil(sortedEvents?.length / itemsPerPage),
                  },
                  (_, index) => (
                    <button
                      key={index}
                      onClick={() => setCurrentPage(index + 1)}
                      className={`${
                        currentPage === index + 1
                          ? 'bg-blue-500 text-white dark:bg-[#c2df2b] dark:hover:bg-[#adc050] dark:text-black'
                          : 'bg-gray-200 text-gray-600 hover:bg-gray-300 '
                      } px-4 py-2 rounded-md font-bold `}
                    >
                      {index + 1}
                    </button>
                  )
                )}
              </div>
            </div>
          </div>
        </main>
      </div>
    </>
  );
}
