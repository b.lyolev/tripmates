import FriendSuggestions from '../../components/FriendSuggestions/FriendSuggestions';
import Friends from '../../components/Friends/Friends';

const Contacts = () => {
  return (
    <div className='max-w-7xl mx-auto pb-12 px-4 sm:px-6 lg:px-8 '>
      <div className=' mt-10'>
        <FriendSuggestions />
        <Friends />
      </div>
    </div>
  );
};

export default Contacts;
