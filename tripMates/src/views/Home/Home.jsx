import React, { useEffect, useRef, useState } from 'react';
import Hero from '../../components/Hero/Hero';
import CategoryCarousel from '../../components/CategoryCarousel/CategoryCarousel';
import HotelCard from '../../components/HotelCard/HotelCard';
import { hotels } from '../../common/hotels';
import GridCards from '../../components/GridCards/GridCards';
import { motion, AnimatePresence } from 'framer-motion';
import SortMenu from '../../components/SortMenu/SortMenu';
import { useSelector } from 'react-redux';
import LoadingSpinner from '../../components/LoadingSpinner/LoadingSpinner';
import PublicEventsSwiper from '../../components/PublicEventsSwiper/PublicEventsSwiper';
import 'swiper/swiper-bundle.css';

export default function Home() {
  const selectedCategories = useSelector((state) => state.filters);
  const priceRange = useSelector((state) => state.priceRange);
  const sortOption = useSelector((state) => state.sort);

  const [currentPage, setCurrentPage] = useState(1);
  const [itemsPerPage] = useState(8);
  const [loading, setLoading] = useState(false);
  const [searchedName, setSearchedName] = useState('');

  // based on this, we can sort the hotels by Best Rating, Most Popular, Newest

  const filterAndSortHotels = (
    hotels,
    selectedCategories,
    priceRange,
    searchedName,
    sortOption
  ) => {
    let filteredHotels = [...hotels];

    if (priceRange) {
      filteredHotels = filteredHotels.filter(
        (hotel) => hotel.price >= priceRange[0] && hotel.price <= priceRange[1]
      );
    }

    if (
      selectedCategories &&
      Object.values(selectedCategories.category).some((value) => value)
    ) {
      const selectedCategoriesArray = Object.keys(selectedCategories.category);
      filteredHotels = filteredHotels.filter((hotel) =>
        selectedCategoriesArray.some(
          (category) =>
            selectedCategories.category[category] && hotel.category === category
        )
      );
    }

    if (searchedName) {
      filteredHotels = filteredHotels.filter((hotel) =>
        hotel.name.toLowerCase().includes(searchedName.toLowerCase())
      );
    }

    // Apply sorting based on the selected sort option
    switch (sortOption) {
      case 'Best Rating':
        filteredHotels.sort((a, b) => b.rating - a.rating);
        break;
      case 'Most Popular':
        filteredHotels.sort((a, b) => b.views - a.views);
        break;
      case 'Newest':
        filteredHotels.sort((a, b) => b.id - a.id);
        break;
      default:
      // No sorting or unknown sorting option
    }

    return filteredHotels;
  };
  const filteredAndSortedHotels = filterAndSortHotels(
    hotels,
    selectedCategories,
    priceRange,
    searchedName,
    sortOption
  );
  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentItems = filteredAndSortedHotels.slice(
    indexOfFirstItem,
    indexOfLastItem
  );

  const scrollContainerRef = useRef(null);

  const scrollToTop = () => {
    if (scrollContainerRef.current) {
      scrollContainerRef.current.scrollTop = 0;
    }
  };

  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber);
    scrollToTop();

    if (scrollContainerRef.current) {
      scrollContainerRef.current.scrollIntoView({ behavior: 'smooth' });
    }
  };

  const handleSearch = (name) => {
    setSearchedName(name);
    setCurrentPage(1);
  };

  return (
    <>
      <div className='flex items-center justify-center mb-6'>
        <div className='flex flex-col w-full items-center'>
          <div className='w-full'>
            <Hero handleSearch={handleSearch} />
          </div>
          <div className='flex flex-col mt-36 max-w-7xl px-8 w-full'>
            <PublicEventsSwiper />
            <div ref={scrollContainerRef}>
              <SortMenu key={'sortMenu'} />
              {filteredAndSortedHotels.length === 0 ? (
                <div className='flex justify-center items-center h-72 text-2xl font-semibold dark:text-white'>
                  No hotels found matching these criteria.
                </div>
              ) : (
                <GridCards>
                  <AnimatePresence>
                    {currentItems.map((hotel, index) => (
                      <motion.div
                        key={hotel.id + index}
                        initial={{ opacity: 0 }}
                        animate={{ opacity: 1 }}
                        // exit={{ opacity: 0 }}
                        transition={{ delay: index * 0.1 }}
                      >
                        <HotelCard
                          key={hotel.id}
                          images={hotel.images}
                          name={hotel.name}
                          location={hotel.location}
                          price={hotel.price}
                          rating={hotel.rating}
                          id={hotel.id}
                        />
                      </motion.div>
                    ))}
                  </AnimatePresence>
                </GridCards>
              )}
              {filteredAndSortedHotels.length > itemsPerPage && (
                <div className='flex justify-center items-center space-x-2 mt-10'>
                  {Array.from(
                    {
                      length: Math.ceil(
                        filteredAndSortedHotels.length / itemsPerPage
                      ),
                    },
                    (_, index) => (
                      <button
                        key={index}
                        onClick={() => paginate(index + 1)}
                        className={`${
                          currentPage === index + 1
                            ? 'bg-blue-500 text-white dark:bg-[#c2df2b] dark:hover:bg-[#adc050] dark:text-black'
                            : 'bg-gray-200 text-gray-600 hover:bg-gray-300 '
                        } px-4 py-2 rounded-md font-bold `}
                      >
                        {index + 1}
                      </button>
                    )
                  )}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
