import React from 'react';
import WeekView from '../../components/Calendar/WeekView/WeekView';
import MonthView from '../../components/Calendar/MonthView/MonthView';

export default function Calendar() {
  return (
    <div>
      <div className='flex justify-center items-center sm:p-10 '>
        <div className='w-full max-w-7xl sm:border-4  border-blue-500  dark:border-[#c2df2b] rounded-xl '>
          <div className=' h-auto  rounded-b-lg '>
            <MonthView />
            <WeekView />
          </div>
        </div>
      </div>
    </div>
  );
}
