import React from 'react';
import ProfileEdit from '../../components/ProfileEdit/ProfileEdit';
import UserProfiles from '../../components/UserProfiles/UserProfile';
import { useSelector } from 'react-redux';

export default function Profile() {
  const userData = useSelector((state) => state.auth.userData);
  return (
    <div className='mx-auto max-w-7xl p-8'>
      <ProfileEdit />
      {userData?.isAdmin && (
        <div className='mt-5'>
          <UserProfiles />
        </div>
      )}
    </div>
  );
}
