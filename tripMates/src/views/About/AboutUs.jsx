import ValioPhoto from '../../assets/images/Valio-photo.jpg';
import BozhoPhoto from '../../assets/images/Bojidar-photo.png';
import RiazPhoto from '../../assets/images/Riaz-photo.jpg';
import LinkedInIcon from '../../assets/images/linkedIn.png';
import GitlabIcon from '../../assets/images/gitLab.png';
import PhoneIcon from '../../assets/images/phone.png';
import EmailIcon from '../../assets/images/email.png';
import { Link } from 'react-router-dom';
import Logo from '../../components/Logo/Logo';


export default function AboutUs() {
  return (
    <div className="lg:mb-16" id='About'>
      <dh-component>
        <div className="container flex justify-center mx-auto lg:pt-16 mb:pt-4">
          <div className='w-4/5'>
            <div className='flex justify-center w-full items-center'>
              <hr className='bg-gray-500 h-px w-2/6 mb-3'/>
              <p className="text-3xl md:text-4xl lg:text-5xl text-center pb-3 mr-4 ml-4 lg:mb-4 dark:text-white">About us</p>
              <hr className='bg-gray-500 h-px w-2/6 mb-3'/>
            </div>
            <h1 className="xl:text-3xl text-2xl text-center text-gray-800 pb-6 sm:w-3/5 w-5/6 mx-auto lg:mb-8 dark:text-white">
            <div className="text-center">
              <span>The Talented People Behind</span>
            </div>
          </h1>
          </div>
        </div>
        <div className="w-full px-10">
          <div className="container mx-auto">
            <div role="list" aria-label="Behind the scenes People " className="lg:flex md:flex sm:flex items-center xl:justify-between flex-wrap md:justify-around sm:justify-around lg:justify-around">
              <div role="listitem" className="xl:w-1/3 sm:w-3/4 md:w-2/5 relative mt-16 mb-32 sm:mb-24 xl:max-w-sm lg:w-2/5">
                <div className="rounded overflow-hidden shadow-md bg-white dark:bg-[#202020]">
                  <div className="absolute -mt-20 w-full flex justify-center">
                    <div className="h-32 w-32">
                      <img src={BozhoPhoto} alt="Display Picture of Bozhidar Lyolev" role="img" className="rounded-full object-cover h-full w-full shadow-md" />
                    </div>
                  </div>
                  <div className="px-6 mt-16">
                    <h1 className="font-bold text-3xl text-center mb-1 dark:text-white">Bozhidar Lyolev</h1>
                    <p className="text-gray-800 dark:text-gray-500 text-lg text-center font-medium">Junior Frontend Developer</p>
                    <p className="text-center text-gray-600 dark:text-gray-300 text-base pt-3 font-normal mb-4">Motivated Software Developer, with 6 months experience in front-end development, seeking a position that incorporates critical thinking, problem-solving, leadership skills and expands and improves on current skill sets.</p>
                    <div className="flex flex-col justify-center items-center">
                      <hr className='bg-black h-px w-4/5 mb-3'/>
                      <p className='font-bold text-lg mb-2 dark:text-white '>Contact details</p>
                      <div className='flex'><img className='w-4 mr-1' src={PhoneIcon} alt="Phone icon" />
                        <p className='dark:text-white'>+359 895 749 586</p></div>
                      <div className='flex mb-3'><img className='w-4 mr-2' src={EmailIcon} alt="Email icon" />
                        <p className='dark:text-white'>bobi_liolev@abv.bg</p></div>
                    </div>
                    <div className="w-full flex justify-center pt-5 pb-5">
                      <a href="https://gitlab.com/b.lyolev" className="mx-5" target="_blank" rel="noreferrer">
                        <div aria-label="GitLab" role="img">
                          <img className='w-6' src={GitlabIcon} alt="GitLab icon" />
                        </div>
                      </a>
                      <a href="https://www.linkedin.com/in/blyolev/" className="mx-5" target="_blank" rel="noreferrer">
                        <div aria-label="LinkedIn" role="img">
                          <img className='w-7' src={LinkedInIcon} alt="LinkedIn icon" />
                        </div>
                      </a>
                      {/* <a href="" className="mx-5" target="_blank">
                        <div aria-label="Instagram" role="img">
                          <img className='w-6' src={cvIcon} alt="Instagram icon" />
                        </div>
                      </a> */}
                    </div>
                  </div>
                </div>
              </div>

              <div role="listitem" className="xl:w-1/3 sm:w-3/4 md:w-2/5 relative mt-16 mb-32 sm:mb-24 xl:max-w-sm lg:w-2/5">
                <div className="rounded overflow-hidden shadow-md bg-white dark:bg-[#202020]">
                  <div className="absolute -mt-20 w-full flex justify-center">
                    <div className="h-32 w-32">
                      <img src={RiazPhoto} alt="Display Picture of Riaz Bhuiyan" role="img" className="rounded-full object-cover h-full w-full shadow-md" />
                    </div>
                  </div>
                  <div className="px-6 mt-16">
                    <h1 className="font-bold text-3xl text-center mb-1 dark:text-white">Riaz Bhuiyan</h1>
                    <p className="text-gray-800 dark:text-gray-500 text-lg text-center font-medium">Junior Frontend Developer</p>
                    <p className="text-center text-gray-600 dark:text-gray-300 text-base pt-3 font-normal mb-4">Passionate about coding and design. Excited to deliver high quality digital solutions on a global scale that enable positive change. Motivated by challenges and keen on continuously learning and developing in my new career.</p>
                    <div className="flex flex-col justify-center items-center">
                      <hr className='bg-black h-px w-4/5 mb-3'/>
                      <p className='font-bold text-lg mb-2 dark:text-white'>Contact details</p>
                      <div className='flex'><img className='w-4 mr-1' src={PhoneIcon} alt="Phone icon" />
                        <p className='dark:text-white'>+359 885 111 222</p></div>
                      <div className='flex mb-3'><img className='w-4 mr-2' src={EmailIcon} alt="Email icon" />
                        <p className='dark:text-white'>riaz.s.bhuiyan@gmail.com</p></div>
                    </div>
                    <div className="w-full flex justify-center pt-5 pb-5">
                      <a href="https://gitlab.com/RiazBhuiyan" className="mx-5" target="_blank" rel="noreferrer">
                        <div aria-label="GitLab" role="img">
                          <img className='w-6' src={GitlabIcon} alt="GitLab icon" />
                        </div>
                      </a>
                      <a href="https://www.linkedin.com/in/riaz/" className="mx-5" target="_blank" rel="noreferrer">
                        <div aria-label="LinkedIn" role="img">
                          <img className='w-7' src={LinkedInIcon} alt="LinkedIn icon" />
                        </div>
                      </a>
                      {/* <a href="https://drive.google.com/file/d/1EdW7ag8ITFg6PFHgFrovERV3AUtYrTaJ/view?usp=sharing" className="mx-5" target="_blank" rel="noreferrer">
                        <div aria-label="Instagram" role="img">
                          <img className='w-6' src={cvIcon} alt="Instagram icon" />
                        </div>
                      </a> */}
                    </div>
                  </div>
                </div>
              </div>

              <div role="listitem" className="xl:w-1/3 sm:w-3/4 md:w-2/5 relative mt-16 mb-32 sm:mb-24 xl:max-w-sm lg:w-2/5">
                <div className="rounded overflow-hidden shadow-md bg-white dark:bg-[#202020]">
                  <div className="absolute -mt-20 w-full flex justify-center">
                    <div className="h-32 w-32">
                      <img src={ValioPhoto} alt="Display Picture of Ivan Gechev" role="img" className="rounded-full object-cover h-full w-full shadow-md" />
                    </div>
                  </div>
                  <div className="px-6 mt-16">
                    <h1 className="font-bold text-3xl text-center mb-1 dark:text-white">Valentin Petkov</h1>
                    <p className="text-gray-800 dark:text-gray-500 text-lg text-center font-medium">Junior Frontend Developer</p>
                    <p className="text-center text-gray-600 dark:text-gray-300 text-base pt-3 font-normal mb-4">Passionate junior front-end developer with a desire to learn and grow in a collaborative team environment. I am keen to learn new technologies and improve my current skill set to be able to satisfy the needs of the clients.</p>
                    <div className="flex flex-col justify-center items-center">
                      <hr className='bg-black h-px w-4/5 mb-3'/>
                      <p className='font-bold text-lg mb-2 dark:text-white'>Contact details</p>
                      <div className='flex'><img className='w-4 mr-1' src={PhoneIcon} alt="Phone icon" />
                        <p className='dark:text-white'>+359 887 322 611</p></div>
                      <div className='flex mb-3'><img className='w-4 mr-2' src={EmailIcon} alt="Email icon" />
                        <p className='dark:text-white'>valentin.h.petkov@gmail.com</p></div>
                    </div>
                    <div className="w-full flex justify-center pt-5 pb-5">
                      <a href="https://gitlab.com/valentin.petkov" className="mx-5" target="_blank" rel="noreferrer" >
                        <div aria-label="GitLab" role="img">
                          <img className='w-6' src={GitlabIcon} alt="GitLab icon" />
                        </div>
                      </a>
                      <a href="https://www.linkedin.com/in/valentin-petkov-b2465517b/" className="mx-5" target="_blank" rel="noreferrer" >
                        <div aria-label="LinkedIn" role="img">
                          <img className='w-7' src={LinkedInIcon} alt="LinkedIn icon" />
                        </div>
                      </a>
                      {/* <a href="" className="mx-5">
                        <div aria-label="Instagram" role="img">
                          <img className='w-6' src={cvIcon} alt="Instagram icon" />
                        </div>
                      </a> */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </dh-component>
    </div>);
}
