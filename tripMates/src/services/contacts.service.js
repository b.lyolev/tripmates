import { get, set, ref, update, remove, onValue } from 'firebase/database';
import { db } from '../config/firebase-config';
import { getUserData } from './user.service';

export const fetchUsersFromUIDs = async (uids) => {
  try {
    // Create an array to store user objects
    const users = [];

    // Fetch user profiles for each UID in the array
    const profilePromises = uids.map(async (uid) => {
      const userRef = ref(db, `users/${uid}`);
      const userSnapshot = await get(userRef);

      if (userSnapshot.exists()) {
        const userData = userSnapshot.val();
        users.push(userData);
      }
    });

    // Wait for all profile fetches to complete
    await Promise.all(profilePromises);

    return users;
  } catch (error) {
    console.error('Error fetching users from UIDs:', error);
    return []; // Return an empty array in case of an error
  }
};

export const deleteInvitation = async (userHandle, uidToDelete) => {
  try {
    // Get a reference to the user's data in the database
    const userRef = ref(db, `users/${userHandle.handle}`);

    // Fetch the current user data
    const userSnapshot = await get(userRef);
    const userData = userSnapshot.val();

    if (
      !userData ||
      !userData.invitations ||
      !userData.invitations[uidToDelete]
    ) {
      // The invitation with the specified UID doesn't exist, or the data is incomplete
      return false;
    }

    // Remove the UID from invitations
    delete userData.invitations[uidToDelete];

    // Update the user's data in the database with the modified invitations
    await update(userRef, { invitations: userData.invitations });

    return true; // Successfully deleted the UID from invitations
  } catch (error) {
    console.error('Error deleting invitation:', error);
    return false; // Error occurred while deleting the UID
  }
};

export const acceptInvitation = async (userHandle, uidToAdd) => {
  try {
    // Get a reference to your contactsList
    const yourContactsListRef = ref(
      db,
      `users/${userHandle.handle}/contactsList`
    );

    const snapshot = await getUserData(uidToAdd);
    const userData = snapshot.val()[Object.keys(snapshot.val())[0]];

    // Get a reference to the other user's contactsList
    const otherUserContactsListRef = ref(
      db,
      `users/${userData.handle}/contactsList`
    );

    // Fetch the current contactsList data for both users
    const yourContactsListSnapshot = await get(yourContactsListRef);
    const otherUserContactsListSnapshot = await get(otherUserContactsListRef);

    // Get the existing contactsLists as objects or initialize them as empty objects
    const yourContactsList = yourContactsListSnapshot.exists()
      ? yourContactsListSnapshot.val()
      : {};
    const otherUserContactsList = otherUserContactsListSnapshot.exists()
      ? otherUserContactsListSnapshot.val()
      : {};
    console.log(otherUserContactsList);

    // Add the UID to your contactsList and the other user's contactsList
    yourContactsList[uidToAdd] = true;
    otherUserContactsList[userHandle.uid] = true;

    // Update your contactsList in the database
    //   await update(yourContactsListRef, {
    //     contactsList: yourContactsList // Pass the updated contactsList as an object
    //   });

    await update(ref(db, `users/${userHandle.handle}`), {
      contactsList: yourContactsList, // Pass the updated contactsList as an object
    });

    // Update the other user's contactsList in the database
    //   await update(otherUserContactsListRef, {
    //     contactsList: otherUserContactsList // Pass the updated contactsList as an object
    //   });

    await update(ref(db, `users/${userData.handle}`), {
      contactsList: otherUserContactsList, // Pass the updated contactsList as an object
    });

    deleteInvitation(userHandle, uidToAdd);

    return true; // UID added to contactsLists successfully for both users
  } catch (error) {
    console.error('Error adding UID to contactsLists:', error);
    return false; // Error occurred while adding the UID
  }
};

export const updateAvailability = async (handle, status) => {
  switch (status) {
    case unavailability.ON:
      await update(ref(db), {
        [`users/${handle}/available`]: false,
      });
      break;
    case unavailability.OFF:
      await update(ref(db), {
        [`users/${handle}/available`]: true,
      });
      break;
  }
};

export const addFriendRequest = async (currentUserHandle, targetUserHandle) => {
  try {
    // Add the target user's handle to the current user's requests field
    await set(
      ref(db, `users/${currentUserHandle}/sentRequests/${targetUserHandle}`),
      true
    );

    // Add the current user's handle to the target user's requests field
    await set(
      ref(db, `users/${targetUserHandle}/requests/${currentUserHandle}`),
      true
    );

    return true; // Indicates success
  } catch (error) {
    console.error('Error adding friend request:', error);
    return false; // Indicates failure
  }
};

export const removeFriendRequest = async (
  currentUserHandle,
  targetUserHandle
) => {
  try {
    // Remove the entry for the target user's handle in the current user's requests field
    await remove(
      ref(db, `users/${currentUserHandle}/sentRequests/${targetUserHandle}`)
    );

    // Remove the entry for the current user's handle in the target user's requests field
    await remove(
      ref(db, `users/${targetUserHandle}/requests/${currentUserHandle}`)
    );

    return true; // Indicates success
  } catch (error) {
    console.error('Error removing friend request:', error);
    return false; // Indicates failure
  }
};
export const getAllFriendRequests = (currentUserHandle, callback) => {
  const requestsRef = ref(db, `users/${currentUserHandle}/requests`);

  // Set up a real-time listener
  onValue(requestsRef, (snapshot) => {
    if (snapshot.exists()) {
      const requests = snapshot.val();
      const requestHandles = Object.keys(requests);
      // Call the callback function with the updated data
      callback(requestHandles);
    } else {
      // Call the callback function with an empty array if no data exists
      callback([]);
    }
  });
};

export const getSentRequests = (currentUserHandle, callback) => {
  const sentRequestsRef = ref(db, `users/${currentUserHandle}/sentRequests`);

  // Set up a real-time listener
  onValue(sentRequestsRef, (snapshot) => {
    if (snapshot.exists()) {
      const sentRequests = snapshot.val();
      const requestHandles = Object.keys(sentRequests);
      // Call the callback function with the updated data
      callback(requestHandles);
    } else {
      // Call the callback function with an empty array if no data exists
      callback([]);
    }
  });
};

export const getAllFriends = (currentUserHandle, callback) => {
  const friendsRef = ref(db, `users/${currentUserHandle}/friends`);

  // Set up a real-time listener
  onValue(friendsRef, (snapshot) => {
    if (snapshot.exists()) {
      const friends = snapshot.val();
      const friendHandles = Object.keys(friends);
      // Call the callback function with the updated data
      callback(friendHandles);
    } else {
      // Call the callback function with an empty array if no data exists
      callback([]);
    }
  });
};

export const acceptFriendRequest = async (
  currentUserHandle,
  targetUserHandle
) => {
  try {
    // Add the target user to the current user's friends list
    await set(
      ref(db, `users/${currentUserHandle}/friends/${targetUserHandle}`),
      true
    );

    // Add the current user to the target user's friends list
    await set(
      ref(db, `users/${targetUserHandle}/friends/${currentUserHandle}`),
      true
    );

    // Remove the friend request from both users' requests/sentRequests lists
    await remove(
      ref(db, `users/${currentUserHandle}/requests/${targetUserHandle}`)
    );
    await remove(
      ref(db, `users/${targetUserHandle}/sentRequests/${currentUserHandle}`)
    );

    return true; // Indicates success
  } catch (error) {
    console.error('Error accepting friend request:', error);
    return false; // Indicates failure
  }
};

export const rejectFriendRequest = async (
  currentUserHandle,
  targetUserHandle
) => {
  try {
    // Remove the friend request from both users' requests/sentRequests lists
    await remove(
      ref(db, `users/${currentUserHandle}/requests/${targetUserHandle}`)
    );
    await remove(
      ref(db, `users/${targetUserHandle}/sentRequests/${currentUserHandle}`)
    );

    return true; // Indicates success
  } catch (error) {
    console.error('Error rejecting friend request:', error);
    return false; // Indicates failure
  }
};

export const removeFriend = async (currentUserHandle, targetUserHandle) => {
  try {
    // Remove the entry for the target user's handle in the current user's requests field
    await remove(
      ref(db, `users/${currentUserHandle}/friends/${targetUserHandle}`)
    );

    // Remove the entry for the current user's handle in the target user's requests field
    await remove(
      ref(db, `users/${targetUserHandle}/friends/${currentUserHandle}`)
    );

    return true; // Indicates success
  } catch (error) {
    console.error('Error removing friend :', error);
    return false; // Indicates failure
  }
};
