import {
  get,
  set,
  ref,
  query,
  equalTo,
  orderByChild,
  update,
  onValue,
} from 'firebase/database';
import { updateEmail, updatePassword, updateProfile } from 'firebase/auth';
import { auth, db, storage } from '../config/firebase-config';
import {
  deleteObject,
  getDownloadURL,
  uploadBytes,
  ref as uRef,
} from 'firebase/storage';


export const createUserHandle = (
  handle,
  uid,
  firstName,
  lastName,
  email,
  phoneNumber
) => {
  const now = new Date();
  const createdOnTimestamp = now.getTime(); // Unix timestamp in milliseconds
  const createdOnDateTime = now.toISOString(); // ISO string representation of date

  return set(ref(db, `users/${handle}`), {
    handle,
    uid,
    firstName,
    lastName,
    email,
    phoneNumber,
    createdOn: {
      timestamp: createdOnTimestamp,
      dateTime: createdOnDateTime,
    },
    profileImage: null,
  });
};

export const getUserData = (uid) => {
  return get(query(ref(db, 'users'), orderByChild('uid'), equalTo(uid)));
};

export const getUserByHandle = (handle) => {
  return get(ref(db, `users/${handle}`));
};

// export const getAllUsers = async () => {
//   const snapshot = await get(ref(db, 'users'));
//   if (snapshot.exists()) {
//     return Object.keys(snapshot.val()).map((handle) => snapshot.val()[handle]);
//   }
//   return [];
// };

export const getAllUsers = async () => {
  const postsRef = ref(db, 'users');

  return new Promise((resolve, reject) => {
    onValue(
      postsRef,
      (snapshot) => {
        const posts = [];
        snapshot.forEach((childSnapshot) => {
          posts.push(childSnapshot.val());
        });

        resolve(posts);
      },
      (error) => {
        reject(error);
      }
    );
  });
};
export const getUserById = async (uid) => {
  const snapshot = await get(
    query(ref(db, '/users'), orderByChild('uid'), equalTo(uid))
  );

  const { contactsList, events, ...rest } =
    snapshot.val()?.[Object.keys(snapshot.val())?.[0]];

  return {
    ...rest,
    contactsListId: contactsList ? Object.keys(contactsList) : [],
  };
};

export const getUserByEmail = async (email) => {
  const snapshot = await get(query(ref(db, '/users'), orderByChild('email'), equalTo(email)));

  const userData = snapshot.val();

  if (userData) {
    const user = userData[Object.keys(userData)[0]];

    const { contactsList, events, ...rest } = user;

    return {
      ...rest,
      contactsListId: contactsList ? Object.keys(contactsList) : [],
    };
  } else {
    return null; // User not found
  }
};

export const updateUserProfile = async (handle, updatedData) => {
  const { email, password, ...otherUpdatedData } = updatedData;

  try {
    const userRef = ref(db, `users/${handle}`);
    // If the email is being updated, use the Firebase Auth API to update it
    if (email) {
      await updateEmail(auth.currentUser, email);
      await update(userRef, { email });
    }

    // If the password is being updated, use the Firebase Auth API to update it
    if (password) {
      await updatePassword(auth.currentUser, password);
    }

    // Construct the reference to the user's data in the database

    // Update the user's profile data excluding email and password using the database update function
    await update(userRef, otherUpdatedData);

    // If the email or password were updated successfully, update the user's profile object
    if (email || password) {
      await updateProfile(auth.currentUser, { email });
    }

    return true; // Indicate successful update
  } catch (error) {
    console.log('Error updating user profile:', error);
    return false; // Indicate failed update
  }
};

export const updateProfileImage = (handle, imageUrl) => {
  return update(ref(db, `users/${handle}`), { profileImage: imageUrl });
};
export const uploadImageAndSaveLink = async (handle, selectedFile) => {
  const newStorageRef = uRef(
    storage,
    `profileImages/${handle}/${selectedFile.name}`
  );

  await uploadBytes(newStorageRef, selectedFile);
  const imageUrl = await getDownloadURL(newStorageRef);
  const userSnapshot = await getUserByHandle(handle);
  const userData = userSnapshot.val();

  if (userData.profileImage) {
    const prevImageRef = uRef(storage, userData.profileImage);
    await deleteObject(prevImageRef);
  }

  return updateProfileImage(handle, imageUrl);
};

export const blockUser = async (userHandle) => {
  try {
    const userRef = ref(db, `users/${userHandle}`);
    await update(userRef, {
      isBlocked: true,
    });
    return true;
  } catch (error) {
    console.error('Error blocking user:', error);
    throw error;
  }
};

export const unblockUser = async (userHandle) => {
  try {
    const userRef = ref(db, `users/${userHandle}`);
    await update(userRef, {
      isBlocked: false,
    });
    return true;
  } catch (error) {
    console.error('Error unblocking user:', error);
    throw error;
  }
};
