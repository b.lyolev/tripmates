import { auth, db } from '../config/firebase-config';
import {
  ref,
  push,
  set,
  get,
  update,
  orderByChild,
  onValue,
  onChildAdded,
} from 'firebase/database';
import { v4 as uuidv4 } from 'uuid';

export const addEventToDatabase = async (event) => {
  try {
    const eventRef = push(ref(db, 'events'), event);
    alert('Event successfully added!');
    console.log('Event added with ID: ', eventRef.key);
  } catch (error) {
    console.error('An error occurred while adding the event:', error);
    throw error;
  }
};

/**
 * Creates an event in the Firebase Realtime Database.
 * @param {string} authorUID The UID of the user who created the event.
 * @param {string} authorHandle The handle of the user who created the event.
 * @param {string} startDate The start date of the event.
 * @param {string} endDate The end date of the event.
 * @param {string} title The title of the event.
 * @param {string} description The description of the event.
 * @param {string} eventType The type of the event.
 * @param {string} eventParticipants The participants of the event.
 * @param {string} eventRecurrence The recurrence of the event.
 * @param {string} eventRepeat The repeat of the event.
 * @param {string} color The color of the event.
 * @returns {string} The UID of the created event.
 * @throws Will throw an error if the event could not be created.
 */
export const createEvent = async (
  authorUID,
  authorHandle,
  startDate,
  endDate,
  title,
  description,
  eventType,
  eventParticipants,
  eventRecurrence,
  eventRepeat,
  eventLocation,
  color,
  isHotel,
  images,
  link
) => {
  const uid = uuidv4();
  const now = new Date();
  const createdOnTimestamp = now.getTime();
  const createdOnDateTime = now.toISOString();

  const eventRef = ref(db, `events/${uid}`);
  const eventData = {
    uid,
    authorUID,
    authorHandle,
    startDate,
    endDate,
    title,
    description,
    eventType,
    eventParticipants,
    eventRecurrence,
    eventRepeat,
    eventLocation,
    color,
    createdOn: {
      timestamp: createdOnTimestamp,
      dateTime: createdOnDateTime,
    },
    isHotel,
    images,
    link,
  };

  try {
    await set(eventRef, eventData);
    const userCreatedEventsRef = ref(db, `users/${authorHandle}/createdEvents`);
    await update(userCreatedEventsRef, {
      [uid]: true,
    });

    return uid;
  } catch (error) {
    console.error('Error creating event:', error);
    throw error;
  }
};

/**
 * Retrieves all events from the Firebase Realtime Database.
 * @returns {Array} An array of event objects.
 */

export const getAllEvents = (updateEventsCallback) => {
  const eventsRef = ref(db, 'events');

  const unsubscribe = onValue(eventsRef, (snapshot) => {
    const events = [];
    snapshot.forEach((childSnapshot) => {
      events.push(childSnapshot.val());
    });

    updateEventsCallback(events);
  });

  return unsubscribe;
};

/**
 * Deletes an event from the Firebase Realtime Database by UID.
 * @param {string} eventUID The UID of the event to delete.
 * @param {string} authorHandle The handle of the user who created the event.
 * @returns {Promise<void>} A promise that resolves when the event is deleted.
 * @throws Will throw an error if the event could not be deleted.
 */
export const deleteEventByUID = async (eventUID, authorHandle) => {
  const eventRef = ref(db, `events/${eventUID}`);
  const userCreatedEventsRef = ref(db, `users/${authorHandle}/createdEvents`);

  try {
    // Remove the event from the events node
    await set(eventRef, null);

    // Remove the event from the user's createdEvents node
    await update(userCreatedEventsRef, {
      [eventUID]: null,
    });

    console.log('Event deleted successfully:', eventUID);
  } catch (error) {
    console.error('Error deleting event:', error);
    throw error;
  }
};

/**
 * Updates an event in the Firebase Realtime Database.
 * @param {string} eventUID The UID of the event to update.
 * @param {Object} updatedData An object containing the fields to update.
 * @returns {Promise<void>} A promise that resolves when the event is updated.
 * @throws Will throw an error if the event could not be updated.
 */
export const updateEvent = async (eventUID, updatedData) => {
  const eventRef = ref(db, `events/${eventUID}`);

  console.log(eventUID, updatedData);

  try {
    const snapshot = await get(eventRef);
    const eventData = snapshot.val();

    const updatedEvent = { ...eventData, ...updatedData };

    await set(eventRef, updatedEvent);

    console.log('Event updated successfully:', eventUID);
  } catch (error) {
    console.error('Error updating event:', error);
    throw error;
  }
};
