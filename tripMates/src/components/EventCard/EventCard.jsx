import {
  faCircle,
  faCircleInfo,
  faEdit,
  faInfo,
  faMapMarkedAlt,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { MailIcon, PhoneIcon } from '@heroicons/react/solid';
import AvatarsGroup from '../AvatarsGroup/AvatarsGroup';
import { useDispatch, useSelector } from 'react-redux';
import { useState } from 'react';
import ViewLocationModal from '../ViewLocationModal/ViewLocationModal';
import EditEvent from '../EditEvent/EditEvent';
import { Link } from 'react-router-dom';
import { unixTimeToCustomFormat } from '../../utils/helpers';

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

// function calculateTimeDifference(startDate, endDate) {
//   const millisecondsPerHour = 3600000; // 1 hour = 3600000 milliseconds
//   const millisecondsPerDay = 86400000; // 1 day = 86400000 milliseconds

//   const timeDifference = endDate - startDate;
//   const absoluteTimeDifference = Math.abs(timeDifference);

//   if (absoluteTimeDifference >= millisecondsPerDay) {
//     const days = Math.floor(absoluteTimeDifference / millisecondsPerDay);
//     return days === 1 ? `${days} day` : `${days} days`;
//   } else {
//     const hours = Math.floor(absoluteTimeDifference / millisecondsPerHour);
//     return hours === 1 ? `${hours} hour` : `${hours} hours`;
//   }
// }
const isMine = (currentUserUID, eventAuthorUID) => {
  return currentUserUID === eventAuthorUID;
};

function calculateTimeDifference(startDate, endDate) {
  const msPerMinute = 60000;
  const msPerHour = 3600000;
  const msPerDay = 86400000;
  const diff = endDate - startDate;
  const absDiff = Math.abs(diff);

  if (absDiff >= msPerDay) {
    const days = Math.floor(absDiff / msPerDay);
    return `${days} day${days === 1 ? '' : 's'}`;
  } else if (absDiff >= msPerHour) {
    const hours = Math.floor(absDiff / msPerHour);
    const minutes = Math.floor((absDiff % msPerHour) / msPerMinute);

    if (minutes >= 30) {
      return `${hours} H ${minutes} MIN`;
    } else {
      return `${hours} hours`;
    }
  } else {
    const minutes = Math.floor(absDiff / msPerMinute);
    return `${minutes} MIN`;
  }
}

export default function EventCard({
  authorUID,
  uid,
  authorHandle,
  title,
  description,
  startDate,
  endDate,
  eventType,
  eventRecurrence,
  eventRepeat,
  color,
  eventParticipants,
  img,
  isHotel,
  eventLocation,
  link,
}) {
  const userData = useSelector((state) => state.auth.userData);

  const currentEvent = {
    authorUID,
    uid,
    authorHandle,
    title,
    description,
    startDate,
    endDate,
    eventType,
    eventRecurrence,
    eventRepeat,
    color,
    eventParticipants,
    img,
    isHotel,
    eventLocation,
  };

  const [openLocationModal, setOpenLocationModal] = useState(false);
  const [openEditModal, setOpenEditModal] = useState(false);

  const handleOpenLocationModal = () => {
    setOpenLocationModal(true);
  };

  const handleOpenEditModal = () => {
    setOpenEditModal(true);
  };

  return (
    <li
      className={classNames(
        'col-span-1 flex flex-col text-left rounded-xl shadow-md divide-y divide-gray-200 dark:divide-gray-400 dark:bg-[#252525] dark:border border-[#353535] bg-white'
      )}
    >
      <div
        className='flex-1 flex flex-col cursor-pointer'
        onClick={handleOpenEditModal}
      >
        <img className='w-full mx-auto rounded-xl h-44 ' src={img} alt='' />
        <div className='p-2'>
          <div className='flex flex-row justify-between'>
            <p className=' text-gray-600 text-sm font-medium dark:text-gray-300'>
              {unixTimeToCustomFormat(startDate, 3)}
            </p>
            <span
              className={classNames(
                eventType === 'private' ? 'bg-red-300' : 'bg-green-300',
                'px-2 py-1  text-[10px] font-bold  rounded-xl'
              )}
            >
              {eventType.toUpperCase()}
            </span>
          </div>
          {isHotel ? (
            <Link
              to={`/hotel-page/${link}`}
              className=' text-gray-900 dark:text-gray-200 text-lg font-medium'
            >
              {title}
            </Link>
          ) : (
            <p className=' text-gray-900 dark:text-gray-200 text-lg font-medium'>
              {title}
            </p>
          )}
          <div className='flex-grow flex flex-col justify-between dark:text-gray-200 mt-2'>
            <div className='flex justify-between items-center'>
              <span className='font-bold'>
                <span className='font-medium'>Duration: </span>
                {calculateTimeDifference(startDate, endDate).toUpperCase()}
              </span>
              <AvatarsGroup eventParticipants={eventParticipants} />
            </div>
          </div>
        </div>
      </div>
      <div>
        {!isHotel && isMine(userData?.uid, authorUID) && (
          <div className='-mt-px flex divide-x divide-gray-200 dark:divide-gray-400'>
            <div className='w-0 flex-1 flex'>
              <button
                onClick={handleOpenLocationModal}
                className='relative -mr-px w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-blue-500 dark:text-[#c2df2b] font-medium border border-transparent rounded-bl-lg hover:text-blue-700'
              >
                <FontAwesomeIcon className='w-5 h-5' icon={faMapMarkedAlt} />
                <span className='ml-3'>Location</span>
              </button>
            </div>
            {(userData?.isAdmin || isMine(userData?.uid, authorUID)) && (
              <div className='-ml-px w-0 flex-1 flex'>
                <button
                  onClick={handleOpenEditModal}
                  className='relative w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-blue-500  dark:text-[#c2df2b] font-medium border border-transparent rounded-br-lg hover:text-blue-700'
                >
                  <FontAwesomeIcon className='w-5 h-5' icon={faEdit} />
                  <span className='ml-3'>Edit</span>
                </button>
              </div>
            )}
          </div>
        )}
        {!isHotel && !isMine(userData?.uid, authorUID) && (
          <div className='-mt-px flex divide-x divide-gray-200 dark:divide-gray-400'>
            <div className='w-0 flex-1 flex'>
              <button
                onClick={handleOpenLocationModal}
                className='relative -mr-px w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-blue-500 dark:text-[#c2df2b] font-medium border border-transparent rounded-bl-lg hover:text-blue-700'
              >
                <FontAwesomeIcon className='w-5 h-5' icon={faMapMarkedAlt} />
                <span className='ml-3'>Location</span>
              </button>
            </div>
            {userData?.isAdmin ? (
              <div className='-ml-px w-0 flex-1 flex'>
                <button
                  onClick={handleOpenEditModal}
                  className='relative w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-blue-500  dark:text-[#c2df2b] font-medium border border-transparent rounded-br-lg hover:text-blue-700'
                >
                  <FontAwesomeIcon className='w-5 h-5' icon={faEdit} />
                  <span className='ml-3'>Edit</span>
                </button>
              </div>
            ) : (
              <div className='-ml-px w-0 flex-1 flex'>
                <button
                  onClick={handleOpenEditModal}
                  className='relative w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-blue-500  dark:text-[#c2df2b] font-medium border border-transparent rounded-br-lg hover:text-blue-700'
                >
                  <FontAwesomeIcon className='w-5 h-5' icon={faCircleInfo} />
                  <span className='ml-3'>Details</span>
                </button>
              </div>
            )}
          </div>
        )}
        {isHotel && (
          <div className='-mt-px flex divide-x divide-gray-200 dark:divide-gray-400'>
            <div className='w-0 flex-1 flex'>
              <button
                onClick={handleOpenLocationModal}
                className='relative -mr-px w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-blue-500 dark:text-[#c2df2b] font-medium border border-transparent rounded-bl-lg hover:text-blue-700'
              >
                <FontAwesomeIcon className='w-5 h-5' icon={faMapMarkedAlt} />
                <span className='ml-3'>Location</span>
              </button>
            </div>
            {userData?.isAdmin ? (
              <div className='-ml-px w-0 flex-1 flex'>
                <button
                  onClick={handleOpenEditModal}
                  className='relative w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-blue-500  dark:text-[#c2df2b] font-medium border border-transparent rounded-br-lg hover:text-blue-700'
                >
                  <FontAwesomeIcon className='w-5 h-5' icon={faEdit} />
                  <span className='ml-3'>Edit</span>
                </button>
              </div>
            ) : (
              <div className='-ml-px w-0 flex-1 flex'>
                <button
                  onClick={handleOpenEditModal}
                  className='relative w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-blue-500  dark:text-[#c2df2b] font-medium border border-transparent rounded-br-lg hover:text-blue-700'
                >
                  <FontAwesomeIcon className='w-5 h-5' icon={faCircleInfo} />
                  <span className='ml-3'>Details</span>
                </button>
              </div>
            )}
          </div>
        )}
        <ViewLocationModal
          open={openLocationModal}
          setOpen={setOpenLocationModal}
          geoLocation={eventLocation || null}
        />

        <EditEvent
          open={openEditModal}
          setOpen={setOpenEditModal}
          initialEvent={currentEvent}
        />
      </div>
    </li>
  );
}
