import React from 'react';
import ImageGrid from './ImageGrid';
import ReactDatePicker from 'react-datepicker';
import { MultiSelect } from 'primereact/multiselect';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import LocationMap from './LocationMap';
import AmenitiesList from './Amenity';
import HotelReviews from './HotelReviews';
import { hotels } from '../../common/hotels';
import { calculateMaxTime, calculateMinTime } from '../../utils/helpers';
import { useSelector } from 'react-redux';
import { colors } from '../../common/colors';
import { createEvent } from '../../services/event.service';
import ScrollToTop from '../ScrollToTop/ScrollToTop';
import { ReactMarkdown } from 'react-markdown/lib/react-markdown';
import Weather from './Weather';
import './MultiSelect.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { fetchUsersFromUIDs } from '../../services/contacts.service';

const HotelPage = () => {
  const { userData } = useSelector((state) => state.auth);
  const [selectedParticipants, setSelectedParticipants] = useState('');
  const [form, setForm] = useState({
    startDate: '',
    endDate: '',
  });
  const [eventType, setEventType] = useState('public');
  const { hotelId } = useParams();

  const selectedHotel = hotels.find((hotel) => String(hotel.id) === hotelId);

  const updateForm = (prop) => (e) => {
    const value = e.target.value;

    setForm({
      ...form,
      [prop]: value,
    });
  };

  // const participants = [
  //   { name: 'Dimitar Ivanov', code: 'dimitar ivanov' },
  //   { name: 'Ivan Dimitrov', code: 'ivan dimitrov' },
  //   { name: 'Petar Petrov', code: 'petar petrov' },
  //   { name: 'Georgi Georgiev', code: 'georgi georgiev' },
  //   { name: 'Stefan Stefanov', code: 'stefan stefanov' },
  //   { name: 'Marin Ivanov', code: 'marin ivanov' },
  //   { name: 'Georgi Ivanov', code: 'georgi ivanov' },
  //   { name: 'Bozhidar Georgiev', code: 'bozhidar georgiev' },
  //   { name: 'Iliyan Dimitrov', code: 'iliyan dimitrov' },
  // ];

  const userFriends = useSelector((state) => state.auth.userFriends);

  const [currentUserFriends, setCurrentUserFriends] = useState([]);

  useEffect(() => {
    if (!userFriends) return;
    const fetchFriends = async () => {
      const friends = await fetchUsersFromUIDs(userFriends);
      setCurrentUserFriends(friends);
    };

    fetchFriends();
  }, [userFriends]);

  const participantsOptions = currentUserFriends.map((friend) => ({
    name: `${friend.firstName} ${friend.lastName}`,
    handle: friend.handle,
  }));

  // const [currentParticipants, setCurrentParticipants] = useState([]);

  // useEffect(() => {
  //   if (!selectedParticipants) return;
  //   const fetchFriends = async () => {
  //     const participants = await fetchUsersFromUIDs(
  //       selectedParticipants.map((person) => person.handle)
  //     );
  //     setCurrentParticipants(participants);
  //   };

  //   fetchFriends();
  // }, [selectedParticipants]);


  console.log(selectedHotel);
  const handleBookEvent = () => {
    createEvent(
      userData.uid,
      userData.handle,
      new Date(form.startDate).setHours(12, 30, 0, 0),
      new Date(form.endDate).setHours(12, 30, 0, 0),
      selectedHotel.name,
      'none',
      eventType,
      selectedParticipants.length > 0
        ? userData.handle +
            ',' +
            selectedParticipants.map((person) => person.handle).join(',')
        : userData.handle,
      'none',
      1,
      selectedHotel.geoLocation,
      colors[1],
      true,
      selectedHotel.images,
      selectedHotel.id
    ).then(() => {
      toast.success(`${selectedHotel.name} booked successfully!`);

      console.log('Event booked:', {
        selectedParticipants,
        eventType,
      });

      setForm({
        startDate: '',
        endDate: '',
      });
      setSelectedParticipants([]);
      setEventType('public');
    })
    .catch((error) => {
      toast.error('Failed to book the event. Please try again.');
    });
  };

  if (!selectedHotel) {
    return <div>Hotel not found.</div>;
  }

  return (
    <>
      <ScrollToTop />
      <div className='items-center flex justify-center '>
        <div className='max-w-5xl'>
          <div className='flex flex-col '>
            <div>
              <ImageGrid images={selectedHotel.images} />
            </div>
          </div>
          <div className='flex flex-col lg:flex-row'>
            <div className='lg:w-1/2 flex flex-col w-full'>
              <div className='py-4 px-4 rounded-xl my-4 mx-4'>
                <h2 className='text-xl font-semibold mb-2 dark:text-white'>
                  {selectedHotel.name}
                </h2>
                <p className='text-sm text-gray-600 dark:text-white'>
                  <ReactMarkdown>{selectedHotel.description}</ReactMarkdown>
                </p>
              </div>
              <div className='py-4 px-8 rounded-xl my-4 w-full'>
                <h2 className='text-xl font-semibold  dark:text-white mb-3'>
                  What this place offers
                </h2>
                <AmenitiesList />
              </div>
              <div className='grid'>
                <div className=' self end'>
                  <Weather
                    latitude={selectedHotel.geoLocation.lat}
                    longitude={selectedHotel.geoLocation.lng}
                  />
                </div>
              </div>
            </div>
            <div className='lg:w-1/2 flex flex-col w-full'>
              {/* Booking Panel */}
              <div className='max-h-1/2 p-4 bg-white dark:bg-[#202020] rounded-xl m-4'>
                <h2 className='text-xl font-semibold mb-2 dark:text-white'>
                  Add dates for prices
                </h2>
                <div className='grid grid-cols-2'>
                  <div className='rounded-tl-lg rounded-bl-lg border border-gray-700 text-black dark:text-white px-4 py-2'>
                    <span className='font-semibold flex justify-center items-center'>
                      Check-in
                    </span>
                    <ReactDatePicker
                      placeholderText='Pick start date'
                      wrapperClassName='w-full'
                      selected={form.startDate}
                      onChange={(date) =>
                        updateForm('startDate')({
                          target: { value: date },
                        })
                      }
                      dateFormat='dd.MM.yyyy'
                      minDate={new Date()}
                      className='w-full text-center bg-white dark:bg-[#202020] dark:text-white focus:outline-none focus:border-gray-900 sm:text-'
                    />
                  </div>
                  <div className='rounded-tr-lg rounded-br-lg border border-l-0 border-gray-700 text-black dark:text-white px-4 py-2'>
                    <span className='font-semibold flex justify-center items-center'>
                      Check-out
                    </span>
                    <ReactDatePicker
                      minTime={calculateMinTime(form.startDate, form.endDate)}
                      maxTime={
                        calculateMinTime(form.startDate, form.endDate)
                          ? calculateMaxTime(form.startDate)
                          : null
                      }
                      placeholderText='Pick end date'
                      wrapperClassName='w-full'
                      selected={form.endDate}
                      onChange={(date) =>
                        updateForm('endDate')({
                          target: { value: date },
                        })
                      }
                      dateFormat='dd.MM.yyyy'
                      minDate={
                        form.startDate
                          ? new Date(
                              form.startDate.getTime() + 24 * 60 * 60 * 1000
                            )
                          : new Date()
                      }
                      className='w-full text-center bg-white dark:bg-[#202020] dark:text-white focus:outline-none focus:border-gray-900 sm:text-'
                    />
                  </div>
                </div>
                <div className='items-center border-gray-700 rounded-tr-lg mt-4 max-w-full'>
                  <MultiSelect
                    value={selectedParticipants}
                    onChange={(e) => setSelectedParticipants(e.value)}
                    options={participantsOptions}
                    optionLabel='name'
                    filter
                    placeholder='Invite Friends'
                    maxSelectedLabels={2}
                    display='chip'
                    className='w-full dark:bg-[#202020] dark:text-white'
                  />
                  <div className='mt-4'>
                    <label className='block text-lg font-medium text-gray-700 dark:text-white'>
                      Event Type
                    </label>
                    <div className='mt-1'>
                      <label className='inline-flex items-center'>
                        <input
                          type='radio'
                          className=' h-4 w-4 accent-blue dark:accent-[#c2df2b]'
                          checked={eventType === 'public'}
                          onChange={() => setEventType('public')}
                        />
                        <span className='ml-2 dark:text-white'>Public</span>
                      </label>
                      <label className='inline-flex items-center ml-6'>
                        <input
                          type='radio'
                          className=' h-4 w-4 accent-blue dark:accent-[#c2df2b]'
                          checked={eventType === 'private'}
                          onChange={() => setEventType('private')}
                        />
                        <span className='ml-2 dark:text-white'>Private</span>
                      </label>
                      <div className='mt-4'>
                        <button
                          onClick={handleBookEvent}
                          className='w-full text-white dark:text-black font-bold bg-blue-600 dark:bg-[#c2df2b] py-3 px-4 rounded-lg'
                        >
                          Book
                        {/* <ToastContainer position='bottom-right' autoClose={2000} /> */}
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* Location Map */}
              <div className='max-h-1/2 rounded-xl m-4 z-50'>
                <LocationMap hotelLocation={selectedHotel.geoLocation} />
              </div>
            </div>
          </div>
          <div className=' rounded-xl mt-8 m-4 mt-[-2rem]'>
            <HotelReviews />
          </div>
        </div>
      </div>
    </>
  );
};

export default HotelPage;
