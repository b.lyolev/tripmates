import { useState, useEffect, Fragment } from 'react';
import { WEATHER_API_KEY } from '../../common/constants/apiKeys';
import {
  FaSun,
  FaCloud,
  FaCloudSun,
  FaCloudRain,
  FaSnowflake,
} from 'react-icons/fa';
import { Dialog, Transition } from '@headlessui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faClose } from '@fortawesome/free-solid-svg-icons';
import Logo from '../Logo/Logo';

const Weather = ({ latitude, longitude }) => {
  const [city, setCity] = useState(null);
  const [weather, setWeather] = useState(null);
  const [backgroundImage, setBackgroundImage] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [forecast, setForecast] = useState(null);

  useEffect(() => {
    fetchCityAndWeather(latitude, longitude);
  }, [latitude, longitude]);

  const fetchCityAndWeather = (latitude, longitude) => {
    const weatherApiUrl = `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${WEATHER_API_KEY}&units=metric`;
    const nominatimApiUrl = `https://nominatim.openstreetmap.org/reverse?lat=${latitude}&lon=${longitude}&format=json`;

    const forecastApiUrl = `https://api.openweathermap.org/data/2.5/forecast?lat=${latitude}&lon=${longitude}&appid=${WEATHER_API_KEY}&units=metric`;

    fetch(forecastApiUrl)
      .then((response) => response.json())
      .then((data) => {
        setForecast(data);
      })
      .catch((error) => {
        console.error('Error fetching forecast data:', error);
      });

    fetch(weatherApiUrl)
      .then((response) => response.json())
      .then((data) => {
        setWeather(data);

        if (data.weather && data.weather[0]) {
          const weatherCode = data.weather[0].id;
          setBackgroundImage(getBackgroundImageForWeather(weatherCode));
        }
      })
      .catch((error) => {
        console.error('Error fetching weather data:', error);
      });

    fetch(nominatimApiUrl)
      .then((response) => response.json())
      .then((data) => {
        const city =
          data.address.city ||
          data.address.town ||
          data.address.village ||
          data.address.hamlet ||
          'Unknown';
        setCity(city);
      })
      .catch((error) => {
        console.error('Error fetching city data:', error);
      });
  };

  const getBackgroundImageForWeather = (weatherCode) => {
    switch (true) {
      case weatherCode >= 200 && weatherCode < 300:
        return 'https://images.pexels.com/photos/1906932/pexels-photo-1906932.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2';
      case weatherCode >= 300 && weatherCode < 600:
        return 'https://images.pexels.com/photos/1906932/pexels-photo-1906932.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2';
      case weatherCode >= 600 && weatherCode < 700:
        return 'https://images.pexels.com/photos/7245306/pexels-photo-7245306.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2';
      case weatherCode >= 700 && weatherCode < 800:
        return 'https://images.pexels.com/photos/209831/pexels-photo-209831.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2';
      case weatherCode === 800:
        return 'https://images.unsplash.com/photo-1622278647429-71bc97e904e8?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2874&q=80';
      case weatherCode > 800:
        return 'https://images.pexels.com/photos/209831/pexels-photo-209831.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2';
      default:
        return null;
    }
  };

  let weatherIcon;
  if (weather && weather.weather && weather.weather[0]) {
    const weatherCode = weather.weather[0].id;
    switch (true) {
      case weatherCode >= 200 && weatherCode < 300:
        weatherIcon = <FaCloudRain size={50} />;
        break;
      case weatherCode >= 300 && weatherCode < 600:
        weatherIcon = <FaCloudRain size={50} />;
        break;
      case weatherCode >= 600 && weatherCode < 700:
        weatherIcon = <FaSnowflake size={50} />;
        break;
      case weatherCode >= 700 && weatherCode < 800:
        weatherIcon = <FaCloud size={50} />;
        break;
      case weatherCode === 800:
        weatherIcon = <FaSun size={50} />;
        break;
      case weatherCode > 800:
        weatherIcon = <FaCloudSun size={50} />;
        break;
      default:
        weatherIcon = null;
    }
  }

  const toggleModal = () => {
    setShowModal(!showModal);
  };

  console.log(forecast)
  return (
    <div
      className="flex items-left justify-left mb-2 ml-6 mr-6 pt-4 pb-4 rounded-3xl shadow-md shadow-zinc-500 dark:shadow-zinc-950 text-center"
      style={{
        backgroundImage: `url(${backgroundImage})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        cursor: 'pointer',
      }}
      onClick={toggleModal} // Attach click event handler
    >
      {city && weather ? (
        <div className="text-left ml-8 text-gray-100">
          <h2 className="text-xl font-bold mb-2">Weather in {city}</h2>
          <p className="text-md mb-3">Temperature: {Math.round(weather.main.temp)}°C </p>
          <p className="text-md">{weatherIcon || 'No Icon'}</p>
        </div>
      ) : (
        <div className="text-center">
          <p className="text-xl" />
          Loading data...
        </div>
      )}
  
      {/* Modal */}
      <Transition.Root show={showModal} as={Fragment}>
        <Dialog
          as="div"
          className="fixed z-50 inset-0 overflow-y-auto"
          onClose={toggleModal}
        >
          <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0 bg-slate-600 bg-opacity-50 backdrop-blur-[2px] transition-opacity" />
            </Transition.Child>
  
            <span
              className="hidden sm:inline-block sm:align-middle sm:h-screen"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <div className="relative inline-block align-bottom dark:bg-[#171717] bg-white rounded-2xl text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-3xl sm:w-full sm:p-6 max-sm:w-full max-sm:p-0 max-sm:m-0 ring ring-blue-500 dark:ring-[#c2df2b] ">
                <div className="">
                  <button
                    onClick={toggleModal}
                    className="absolute top-3 right-3 text-2xl rounded-md bg-blue-500 dark:bg-[#c2df2b] px-2 text-white dark:text-black"
                  >
                    <FontAwesomeIcon icon={faClose} />
                  </button>
                  <div className="items-center justify-center ">
                    <div className="min-h-full flex flex-col justify-center sm:px-6 lg:px-8 ">
                      <div className="space-y-6 sm:px-6 lg:px-0 lg:col-span-9 ">
                        <section>
                          <div className="sm:rounded-md sm:overflow-hidden ">
                            <div className="py-3 px-4 ">
                              <div className="flex flex-row items-center justify-center sm:mx-auto sm:w-full sm:max-w-md max-sm:mt-3 ">
                                {/* You can add your custom logo here */}
                                <Logo></Logo>
                                <h2 className="ml-2 text-center text-3xl font-extrabold text-blue-500 dark:text-[#c2df2b] ">
                                  Weather forecast for the next 5 days
                                </h2>
                              </div>
                            </div>
                          </div>
                        </section>
  
                        {/* Display forecast data here */}
                        {forecast && (
                      <div className="p-4">
                        <h3 className="text-xl font-semibold mb-2">
                          5-Day Forecast for {city}
                      </h3>
                      <div className="grid grid-cols-1 gap-4 sm:grid-cols-2">
                        {forecast.list
                          .filter((item, index, self) => {
                            const currentDate = new Date(item.dt_txt).getDate();
                            return (
                              index ===
                              self.findIndex(
                                (i) => new Date(i.dt_txt).getDate() === currentDate
                              )
                            );
                          })
                          .map((item) => (
                            <div
                              key={item.dt}
                              className="bg-white dark:bg-[#171717] rounded-lg p-4 border border-gray-300 dark:border-gray-700"
                              style={{
                                backgroundImage: `url(${getBackgroundImageForWeather(
                                  item.weather[0].id
                                )})`,
                                backgroundSize: 'cover',
                                backgroundPosition: 'center',
                              }}
                            >
                              <h4 className="text-lg font-semibold text-gray-100 dark:text-gray-100">
                                {new Intl.DateTimeFormat('en-US', {
                                  weekday: 'long',
                                }).format(new Date(item.dt_txt))}
                              </h4>
                              <p className="text-gray-100 dark:text-gray-100">
                                Temperature: {Math.round(item.main.temp)}°C
                              </p>
                              <p className="text-gray-100 dark:text-gray-100">
                                Min Temp: {Math.round(item.main.temp_min)}°C
                              </p>
                              <p className="text-gray-100 dark:text-gray-100">
                                Max Temp: {Math.round(item.main.temp_max)}°C
                              </p>
                              <p className="text-gray-100 dark:text-gray-100">
                                Weather: {item.weather[0].main}
                                <img
                                  src={`https://openweathermap.org/img/w/${item.weather[0].icon}.png`}
                                  alt={item.weather[0].description}
                                />
                              </p>
                            </div>
                          ))}
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </Transition.Child>
    </div>
  </Dialog>
</Transition.Root>
    </div>
  );
  };

export default Weather;


















