import React, { useState } from 'react';
import DatePicker from './DatePicker';
import Button from './Button'; 

const HotelBookingForm = () => {
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);

  const handleDateChange = (range) => {
    setStartDate(range.startDate);
    setEndDate(range.endDate);
  };

  const handleBookClick = () => {
    // Implement your booking logic here
    console.log('Booking:', startDate, endDate);
  };

  return (
    <div className="p-6 bg-white rounded-lg shadow-md">
      <h2 className="text-xl font-semibold mb-4">Book a Hotel</h2>
      <div className="mb-4">
        <h3 className="text-lg font-medium">Select Dates:</h3>
        <DatePicker value={{ startDate, endDate }} onChange={handleDateChange} />
      </div>
      <Button onClick={handleBookClick} disabled={!startDate || !endDate}>
        Book Now
      </Button>
    </div>
  );
};

export default HotelBookingForm;