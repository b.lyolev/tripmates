import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faWifi,
  faParking,
  faSnowflake,
  faMountain,
  faTv,
  faUtensils,
  faSwimmingPool,
  faFire,
} from '@fortawesome/free-solid-svg-icons';

const Amenity = ({ icon, text }) => (
  <div className='flex items-center mb-2 border-2 p-2 rounded-lg border-gray-200'>
    <FontAwesomeIcon
      icon={icon}
      className='text-gray-500 mr-2 dark:text-white'
    />
    <span className='text-gray-700 dark:text-white'>{text}</span>
  </div>
);

const AmenitiesList = () => (
  <div className=' grid sm:grid-cols-2 gap-x-5 gap-y-3 grid-cols-1 '>
    <Amenity icon={faWifi} text='Free Wi-Fi' />
    <Amenity icon={faParking} text='Free Parking' />
    <Amenity icon={faSnowflake} text='Air Conditioning' />
    <Amenity icon={faMountain} text='Mountain View' />
    <Amenity icon={faTv} text='Flat-screen TV' />
    <Amenity icon={faUtensils} text='Equipped Kitchen' />
    <Amenity icon={faSwimmingPool} text='Pool View' />
    <Amenity icon={faFire} text='Barbecue Facilities' />
  </div>
);

export default AmenitiesList;
