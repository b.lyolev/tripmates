import React, { useState } from 'react';
import { useEffect } from 'react';
import './ImageGrid.css';

const ImageGrid = ({ images }) => {
  const [selectedItem, setSelectedItem] = useState('1');
  const [displayedItems, setDisplayedItems] = useState([]);

  // const items = [
  //   { id: '1', image: './src/assets/hero-section/2690553.jpg' },
  //   { id: '2', image: 'https://a0.muscache.com/im/pictures/26484d7c-62de-445e-a8b3-609826a58167.jpg?im_w=1200' },
  //   { id: '3', image: './src/assets/hero-section/hotel-bg.jpg' },
  //   { id: '4', image: 'https://a0.muscache.com/im/pictures/miso/Hosting-828819965371292386/original/5cd5acce-6650-421f-9a0c-86b9b918b8e5.jpeg?im_w=1200' },
  //   { id: '5', image: 'https://a0.muscache.com/im/pictures/miso/Hosting-828819965371292386/original/2b7e1509-f97a-4858-a025-e1309f17d4b1.jpeg?im_w=1200' },
  //   { id: '6', image: 'https://a0.muscache.com/im/pictures/miso/Hosting-828819965371292386/original/6cba9fef-e672-423a-a29f-b695afb165fe.jpeg?im_w=1200' },
  // ];

  const handleItemClick = (itemId) => {
    if (itemId === selectedItem) {
      return;
    }
    setSelectedItem(itemId);
  };

  useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth < 768) {
        setDisplayedItems(images.slice(0, 3));
      } else {
        setDisplayedItems(images.slice(0, 6));
      }
    };

    handleResize();
    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [selectedItem]);

  return (
    <>
      <div
        className={`grid-images ${selectedItem ? 'item-images-enlarged' : ''}`}
      >
        {displayedItems.map((item) => (
          <div
            key={item.id}
            className={`item-images ${
              selectedItem === item.id ? 'selected-images' : ''
            }`}
            onClick={() => handleItemClick(item.id)}
          >
            <img src={item.image} />
          </div>
        ))}
      </div>
    </>
  );
};

export default ImageGrid;
