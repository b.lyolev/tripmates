import React from 'react';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';

function LocationMap({ hotelLocation }) {
  const position = hotelLocation || [0, 0];

  return (
    <div className='col-span-4 sm:col-span-2'>
      <label
        htmlFor='event-location'
        className='block text-xl font-semibold text-gray-700 mb-2 dark:text-white'
      >
        Hotel location
      </label>
      <MapContainer
        center={position}
        zoom={13}
        style={{ height: '350px', width: '100%', borderRadius: '13px' }}
      >
        <TileLayer
          url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
          //   attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        />
        <Marker position={position}>
          <Popup>
            {hotelLocation ? (
              <p>{hotelLocation}</p>
            ) : (
              <p>No location available for this hotel.</p>
            )}
          </Popup>
        </Marker>
      </MapContainer>
    </div>
  );
}

export default LocationMap;
