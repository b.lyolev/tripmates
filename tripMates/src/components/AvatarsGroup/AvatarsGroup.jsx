import React, { useEffect, useState } from 'react';
import { Avatar, AvatarGroup } from '@chakra-ui/react';
import { fetchUsersFromUIDs } from '../../services/contacts.service';

export default function AvatarsGroup({
  eventParticipants,
  shouldAdjustPosition = false,
}) {
  const [currentParticipants, setCurrentParticipants] = useState([]);

  useEffect(() => {
    if (!eventParticipants) return;
    const fetchFriends = async () => {
      const participants = await fetchUsersFromUIDs(
        eventParticipants.split(',')
      );
      setCurrentParticipants(participants);
    };

    fetchFriends();
  }, [eventParticipants]);

  return (
    <>
      <AvatarGroup size='sm' max={shouldAdjustPosition ? 1 : 3}>
        {currentParticipants.map((attendee, i) => (
          <Avatar
            key={i}
            name={`${attendee.firstName} ${attendee.lastName}`}
            src={attendee.profileImage}
            size='sm'
            borderColor='gray-200'
          />
        ))}
      </AvatarGroup>
    </>
  );
}
