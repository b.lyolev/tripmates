import React from 'react';
import { Avatar } from '@chakra-ui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faCheck,
  faClose,
  faListCheck,
  faPhone,
} from '@fortawesome/free-solid-svg-icons';
import { useDispatch, useSelector } from 'react-redux';
import {
  acceptFriendRequest,
  addFriendRequest,
  rejectFriendRequest,
  removeFriend,
  removeFriendRequest,
} from '../../services/contacts.service';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function FriendCard({ user, isFriendSuggestions }) {
  const userData = useSelector((state) => state.auth.userData);

  const { userRequests, userSentRequests } = useSelector((state) => state.auth);

  const userFriends = useSelector((state) => state.auth.userFriends);

  const handleSendRequest = async () => {
    try {
      await addFriendRequest(userData.handle, user.handle);
      toast.success(`Friend request sent to ${user.firstName} successfully!`, { autoClose: 2000 });
    } catch (error) {
      toast.error('Failed to send friend request. Please try again.');
    }
  };

  const handleRemoveRequest = async () => {
    try {
      await removeFriendRequest(userData.handle, user.handle);
      toast.success(`Friend request removed for ${user.firstName}!`, { autoClose: 2000 });
    } catch (error) {
      toast.error('Failed to remove friend request. Please try again.');
    }
  };

  const handleAcceptRequest = async () => {
    try {
      await acceptFriendRequest(userData.handle, user.handle);
      toast.success(`You are now friends with ${user.firstName}!`, { autoClose: 2000 });
    } catch (error) {
      toast.error('Failed to accept friend request. Please try again.');
    }
  };

  const handleRejectRequest = async () => {
    try {
      await rejectFriendRequest(userData.handle, user.handle);
      toast.success(`Friend request from ${user.firstName} rejected.`, { autoClose: 2000 });
    } catch (error) {
      toast.error('Failed to reject friend request. Please try again.');
    }
  };

  const handleRemoveFriend = async () => {
    try {
      await removeFriend(userData.handle, user.handle);
      toast.success(`You have removed ${user.firstName} from your friends.`, { autoClose: 2000 });
    } catch (error) {
      toast.error('Failed to remove friend. Please try again.');
    }
  };

  const hasSentRequest = userSentRequests?.includes(user.handle);

  const hasAddedMe = userRequests?.includes(user.handle);

  const isAlreadyFriend = userFriends?.includes(user.handle);

  return (
    <div className='flex flex-col border rounded-2xl items-center border-gray-200 dark:border-gray-600  p-3 dark:bg-[#202020] bg-white shadow-md'>
      <Avatar
        size={'2xl'}
        className=' ring-2 ring-blue-600 dark:ring-[#c2df2b]'
        name={`${user.firstName} ${user.lastName}`}
        src={user.profileImage}
      />

      <div className='flex flex-col text-center dark:text-white text-black mt-3 gap-1 font-medium'>
        <span className=' text-xl'>
          {`${user.firstName} ${user.lastName}`}{' '}
        </span>
        <span className=' text-base  font-normal'>
          <FontAwesomeIcon icon={faPhone} className='mr-2' />
          {user.phoneNumber}
        </span>
      </div>
      <div className='flex flex-col mt-3 gap-2 w-full font-medium '>
        {hasAddedMe && !hasSentRequest && (
          <div className='flex justify-between items-center gap-3'>
            <button
              onClick={handleAcceptRequest}
              className='bg-blue-600 hover:bg-blue-800 dark:bg-[#c1df2b] hover:dark:bg-[#afc250] rounded-2xl py-2 px-4 w-full text-white  dark:text-black '
              
            >
              <FontAwesomeIcon icon={faCheck} />
            </button>
            <button
              onClick={handleRejectRequest}
              className='bg-gray-300 hover:bg-gray-400 dark:bg-gray-500  dark:hover:bg-gray-600 dark:text-white text-black rounded-2xl w-full  py-2 px-4'
            >
              <FontAwesomeIcon icon={faClose} />
            </button>
          </div>
        )}

        {!hasAddedMe && !hasSentRequest && !isAlreadyFriend ? (
          <button
            onClick={handleSendRequest}
            className='bg-blue-600 hover:bg-blue-800 dark:bg-[#c1df2b] hover:dark:bg-[#afc250] rounded-2xl py-2 text-white  dark:text-black'
          >
            Add
            {/* <ToastContainer position='bottom-right' autoClose={2000} /> */}
          </button>
        ) : null}

        {hasSentRequest && (
          <button
            onClick={handleRemoveRequest}
            className='bg-gray-300 hover:bg-gray-400 dark:bg-gray-500  dark:hover:bg-gray-600 dark:text-white text-black rounded-2xl  py-2'
          >
            Request sent
          </button>
        )}

        {isFriendSuggestions && (
          <button
            disabled={hasSentRequest}
            onClick={handleRemoveFriend}
            className='bg-gray-300 hover:bg-gray-400 dark:bg-gray-500  dark:hover:bg-gray-600 dark:text-white text-black rounded-2xl  py-2'
          >
            Remove
          </button>
        )}
      </div>
    </div>
  );
}
