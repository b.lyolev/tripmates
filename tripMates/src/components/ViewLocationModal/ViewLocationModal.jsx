import React from 'react';
import { Fragment, useRef, useState, useEffect } from 'react';
import { Dialog, Transition } from '@headlessui/react';
import { useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faClose,
  faLocationArrow,
  faLocationCrosshairs,
} from '@fortawesome/free-solid-svg-icons';
import 'react-datepicker/dist/react-datepicker.css';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import './location.css';
import { Icon } from 'leaflet';
import { useSelector } from 'react-redux';
import MapLocation from './MapLocation';
import LocationPicker from '../LocationPicker/LocationPicker';

export default function ViewLocationModal({ open, setOpen, geoLocation }) {
  const { userData } = useSelector((state) => state.auth);

  const [isLocationPickerFullScreen, setLocationPickerFullScreen] =
    useState(false);

  const [selectedLocation, setSelectedLocation] = useState(null);
  const [isLocationPicked, setLocationPicked] = useState(false);

  const [validationMessages, setValidationMessages] = useState({
    title: '',
    description: '',
  });

  const handleLocationPickerSave = (selectedLocation) => {
    setSelectedLocation(selectedLocation);
    setLocationPicked(true);
    setLocationPickerFullScreen(false); // Close the map here
  };

  const participants = [
    { name: 'Dimitar Ivanov', code: 'dimitar ivanov' },
    { name: 'Ivan Dimitrov', code: 'ivan dimitrov' },
    { name: 'Petar Petrov', code: 'petar petrov' },
    { name: 'Georgi Georgiev', code: 'georgi georgiev' },
    { name: 'Stefan Stefanov', code: 'stefan stefanov' },
    { name: 'Marin Ivanov', code: 'marin ivanov' },
    { name: 'Georgi Ivanov', code: 'georgi ivanov' },
    { name: 'Bozhidar Georgiev', code: 'bozhidar georgiev' },
    { name: 'Iliyan Dimitrov', code: 'iliyan dimitrov' },
  ];

  const [error, setError] = useState(null);

  const cancelButtonRef = useRef(null);

  const closeMapModal = () => {
    setOpen(false); // This function will close the modal
  };

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as='div'
        className='fixed z-50 inset-0 overflow-y-auto '
        initialFocus={cancelButtonRef}
        onClose={setOpen}
      >
        <div className='flex items-center justify-center min-h-screen  sm:px-4 sm:pb-20 text-center sm:block '>
          <Transition.Child
            as={Fragment}
            enter='ease-out duration-300'
            enterFrom='opacity-0'
            enterTo='opacity-100'
            leave='ease-in duration-200'
            leaveFrom='opacity-100'
            leaveTo='opacity-0'
          >
            <Dialog.Overlay className='fixed inset-0 bg-slate-600 bg-opacity-50 backdrop-blur-[2px] transition-opacity' />
          </Transition.Child>

          {/* This element is to trick the browser into centering the modal contents. */}
          <span
            className='hidden sm:inline-block sm:align-middle sm:h-screen '
            aria-hidden='true'
          >
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter='ease-out duration-300'
            enterFrom='opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95'
            enterTo='opacity-100 translate-y-0 sm:scale-100'
            leave='ease-in duration-200'
            leaveFrom='opacity-100 translate-y-0 sm:scale-100'
            leaveTo='opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95'
          >
            <div className='relative inline-block align-bottom dark:bg-[#171717] bg-white sm:rounded-2xl  text-left overflow-hidden   shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-3xl sm:w-full sm:p-6 max-sm:w-full  max-sm:p-0 max-sm:m-0 sm:ring  ring-blue-500 dark:ring-[#c2df2b] '>
              <div className='items-center justify-center '>
                <div className=' min-h-full flex flex-col justify-center  sm:px-6 lg:px-8 '>
                  <div className='h-[100vh] sm:h-[700px] w-full  text-black dark:text-white flex justify-center items-center'>
                    <MapLocation
                      location={geoLocation}
                      onClose={closeMapModal}
                    />
                  </div>
                </div>
              </div>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition.Root>
  );
}
