import { useState, useRef, useEffect } from 'react';
import {
  MapContainer,
  Marker,
  Popup,
  TileLayer,
  useMapEvent,
  useMap,
} from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
// import SearchBar from './SearchBar';
import { Icon } from 'leaflet';
import './location.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faClose, faRemove } from '@fortawesome/free-solid-svg-icons';

export default function LocationPicker({ location, onClose }) {
  const [center, setCenter] = useState(null);
  const [zoomLevel, setZoomLevel] = useState(17); // Current zoom level
  const [currentLocation, setCurrentLocation] = useState(center);

  const [isLocating, setIsLocating] = useState(true);

  const mapRef = useRef(null);
  const animateRef = useRef(false);

  const handleButtonClick = () => {
    // Call the onClose function to close the modal
    onClose();
  };
  useEffect(() => {
    if (location) {
      setCenter(location);
      setCurrentLocation(location);
    }
  }, []);

  useEffect(() => {
    if (mapRef.current && currentLocation) {
      mapRef.current.setView(currentLocation, mapRef.current.getZoom());
    }
  }, [currentLocation]);

  animateRef.current = true;

  return location ? (
    <div
      className='map-overlay'
      style={{
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
      }}
    >
      <MapContainer
        attributionControl={false}
        key={`${center?.lat}-${center?.lng}`}
        center={center}
        zoom={zoomLevel}
        scrollWheelZoom={true}
        style={{ height: '100%', width: '100%' }}
        animate={true}
        ref={mapRef}
      >
        <TileLayer
          attribution='&copy; <a href="#">TripMates</a>'
          url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
        />

        <div className='leaflet-top leaflet-center-close'>
          <div className='searchContainer m-4 flex flex-row items-center'></div>
        </div>

        <div className='leaflet-center-close leaflet-top  '>
          <div className='full-screen-buttons flex max-sm:flex-col gap-2'>
            <button
              onClick={handleButtonClick}
              className='absolute bottom-5 right-3 text-2xl rounded-md bg-blue-500  dark:bg-[#c2df2b] px-2 text-white dark:text-black'
            >
              <FontAwesomeIcon icon={faClose} />
            </button>
          </div>
        </div>

        <Marker draggable={false} position={currentLocation}>
          <Popup>
            {`Latitude: ${currentLocation?.lat?.toFixed(
              6
            )}, Longitude: ${currentLocation?.lng?.toFixed(6)}`}
          </Popup>
        </Marker>
      </MapContainer>
    </div>
  ) : (
    <p className='w-full text-center text-3xl font-bold '>No location found!</p>
  );
}
