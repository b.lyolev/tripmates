import {
  faArrowDown,
  faArrowUp,
  faLock,
  faLockOpen,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect, useState } from 'react';

export default function EventSortBar({ onSearch, onSort, onShowAll }) {
  const [searchQuery, setSearchQuery] = useState('');
  const [sortBy, setSortBy] = useState('date');
  const [sortOrder, setSortOrder] = useState('asc');
  const [eventType, setEventType] = useState('public');

  const handleSearch = () => {
    onSearch(searchQuery);
  };

  const handleSort = (value) => {
    if (sortBy === value) {
      setSortOrder(sortOrder === 'asc' ? 'desc' : 'asc');
    } else {
      setSortOrder('asc');
    }

    setSortBy(value);
    onSort(value, sortOrder);
  };

  const toggleEventType = () => {
    const newEventType = eventType === 'public' ? 'private' : 'public';
    setEventType(newEventType);
    onSort(sortBy, sortOrder, newEventType);
  };

  const renderSortIcon = (option) => {
    if (sortBy === option) {
      return sortOrder === 'asc' ? (
        <FontAwesomeIcon icon={faArrowUp} />
      ) : (
        <FontAwesomeIcon icon={faArrowDown} />
      );
    }
    return null;
  };

  const handleShowAll = () => {
    setEventType('all');
    onShowAll();
  };
  return (
    <div className='flex flex-col justify-between mb-4 sm:flex-row gap-4 '>
      <div className='flex items-center space-x-4 w-full'>
        <input
          type='text'
          placeholder='Search events'
          value={searchQuery}
          onChange={(e) => setSearchQuery(e.target.value)}
          className='border w-full border-gray-300 px-3 py-2 rounded dark:bg-[#252525] dark:text-white dark:border-gray-700 dark:focus:ring-gray-400 dark:focus:border-gray-400'
        />
        <button
          onClick={handleSearch}
          className='bg-blue-500 text-white px-3 py-[8px] rounded dark:bg-[#c2df2b] dark:text-black'
        >
          Search
        </button>
      </div>
      <div className='flex space-x-3 w-full sm:justify-end justify-center items-center'>
        <button
          onClick={() => handleSort('date')}
          className='rounded-xl border-2  py-2 px-3 text-sm font-medium shadow-sm hover:bg-gray-200  dark:hover:bg-gray-800 focus:outline-none  focus:ring-2 focus:ring-blue-500 dark:focus:ring-[#9dad48]  border-blue-500 dark:border-[#c2df2b] dark:text-white text-black '
        >
          <span className=' mr-2'>Sort by Date</span>
          <span>{renderSortIcon('date')}</span>
        </button>
        <button
          onClick={toggleEventType}
          className={`${
            eventType === 'public'
              ? 'border-blue-500 dark:border-[#c2df2b] dark:text-white text-black'
              : 'border-blue-500 dark:border-[#c2df2b] dark:text-white text-black'
          } rounded-xl border-2 py-2 px-3 text-sm font-medium  shadow-sm hover:bg-gray-200  dark:hover:bg-gray-800 focus:outline-none focus:ring-2 focus:ring-blue-500 dark:focus:ring-[#9dad48]`}
        >
          {eventType === 'public' ? (
            <FontAwesomeIcon icon={faLockOpen} className='text-gray-600' />
          ) : (
            <FontAwesomeIcon icon={faLock} className='text-red-500' />
          )}
          <span className='max-md:hidden  ml-2'>
            {eventType === 'public' ? 'Public' : 'Private'}
          </span>
        </button>
        <button
          onClick={handleShowAll}
          className={`${
            eventType === 'all'
              ? ' border-blue-500 dark:border-[#c2df2b] dark:text-white text-black'
              : '  border-blue-500 dark:border-[#c2df2b] dark:text-white text-black'
          } rounded-xl border-2  py-2 px-3 text-sm font-medium shadow-sm hover:bg-gray-200  dark:hover:bg-gray-800 focus:outline-none focus:ring-2 focus:ring-blue-500 dark:focus:ring-[#9dad48]  `}
        >
          Show All
        </button>
      </div>
    </div>
  );
}
