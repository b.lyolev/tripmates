import React, { useState } from 'react';
import FriendCard from '../FriendCard/FriendCard';
import { useSelector } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faArrowUp } from '@fortawesome/free-solid-svg-icons';

export default function Friends() {
  const allUsers = useSelector((state) => state.allUsers.users);
  const [searchQuery, setSearchQuery] = useState('');
  const [showAllFriends, setShowAllFriends] = useState(false);
  const userData = useSelector((state) => state.auth.userData);

  const userFriends = useSelector((state) => state.auth.userFriends);

  // Function to filter users based on search query
  const filteredUsers = allUsers.filter((user) => {
    const lowerCaseQuery = searchQuery.toLowerCase();
    const fullName = `${user?.firstName} ${user?.lastName}`.toLowerCase();
    const email = user?.email?.toLowerCase();
    const phoneNumber = user?.phoneNumber?.toLowerCase();
    const username = user?.handle?.toLowerCase();

    return (
      fullName.includes(lowerCaseQuery) ||
      (lowerCaseQuery.startsWith('@') &&
        email.includes(lowerCaseQuery.slice(1))) ||
      (lowerCaseQuery.startsWith('#') &&
        username.includes(lowerCaseQuery.slice(1))) ||
      (phoneNumber.includes(lowerCaseQuery) && /^\d{10}$/.test(lowerCaseQuery)) // Assuming phone number is 10 digits
    );
  });

  // Filter out your friends and yourself
  const users = filteredUsers.filter(
    (user) =>
      user.handle !== userData?.handle && !userFriends.includes(user.handle)
  );

  const visibleUsers = showAllFriends ? users : users.slice(0, 5);

  return (
    <div>
      <div className='flex flex-col sm:flex-row w-full mb-3  sm:items-end text-center '>
        <h1 className='flex-shrink-0 text-3xl font-medium dark:text-white  text-black  max-sm:mb-2 mr-3'>
          Friend Suggestions
        </h1>
        <div className='relative flex items-center w-full justify-end'>
          <input
            type='text'
            placeholder='Search for friends #username, @email or phone'
            className=' border rounded-md w-full max-w-lg  h-10 pr-10  mt-1 block border-gray-300 shadow-sm py-2 px-3 focus:outline-none  sm:text-lg  dark:border-gray-700 dark:bg-[#252525] dark:text-white dark:focus:ring-gray-400 dark:focus:border-gray-400'
            value={searchQuery}
            onChange={(e) => setSearchQuery(e.target.value)}
          />
          <FontAwesomeIcon
            icon={faSearch}
            className='text-2xl text-gray-400 absolute right-3 top-2/4 transform -translate-y-2/4 cursor-pointer'
          />
        </div>
      </div>

      {filteredUsers.length === 0 ? (
        <div className='flex justify-center items-center text-center'>
          <div className='flex justify-center items-center text-2xl font-bold text-black dark:text-gray-200 h-56'>
            Ooops, no results found.🙁
          </div>
        </div>
      ) : (
        <>
          <ul
            role='list'
            className='grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 py-2'
          >
            {visibleUsers.map((user, i) => (
              <FriendCard key={user.uid} user={user} />
            ))}
          </ul>
          {!showAllFriends ? (
            <div className='flex justify-center mt-2'>
              <button
                onClick={() => setShowAllFriends(true)}
                className='text-blue-500 hover:underline cursor-pointer'
              >
                Show All Suggestions
              </button>
            </div>
          ) : (
            <div className='flex justify-center mt-2'>
              <button
                onClick={() => setShowAllFriends(false)}
                className='text-blue-500 hover:underline cursor-pointer'
              >
                <FontAwesomeIcon icon={faArrowUp} className='mr-1' /> Shrink
              </button>
            </div>
          )}
        </>
      )}
    </div>
  );
}
