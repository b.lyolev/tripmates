import { useEffect, useState } from 'react';
import { RadioGroup } from '@headlessui/react';
import { colors } from '../../common/colors';

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export default function ColorPalette({ onSelectColor, defaultColor }) {
  const [selectedColor, setSelectedColor] = useState(defaultColor);

  useEffect(() => {
    setSelectedColor(defaultColor);
  }, [defaultColor]);

  const handleColorChange = (color) => {
    setSelectedColor(color);
    onSelectColor(color);
  };
  return (
    <RadioGroup value={selectedColor} onChange={handleColorChange}>
      <RadioGroup.Label className='block text-sm font-medium text-gray-700 dark:text-white '>
        Choose event color
      </RadioGroup.Label>
      <div className='mt-4 flex items-center space-x-3 justify-center rounded-lg   '>
        {colors.map((color, index) => (
          <RadioGroup.Option
            key={index}
            value={color}
            className={({ active, checked }) =>
              classNames(
                color.selectedColor,
                color.ringColor,
                color.name === defaultColor?.name &&
                  selectedColor.name === defaultColor?.name
                  ? 'ring '
                  : '',
                active && checked ? 'ring ' : '',
                !active && checked ? 'ring-2' : '',
                '-m-0.5 relative p-0.5 rounded-full flex items-center justify-center cursor-pointer focus:outline-none '
              )
            }
          >
            <RadioGroup.Label as='p' className='sr-only '>
              {color.name}
            </RadioGroup.Label>
            <span
              aria-hidden='true'
              className={classNames(
                color.bgColor,
                color.hoverColor,
                'h-6 w-6 border border-black border-opacity-10 rounded-full '
              )}
            />
          </RadioGroup.Option>
        ))}
      </div>
    </RadioGroup>
  );
}
