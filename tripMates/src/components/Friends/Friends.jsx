import React, { useEffect, useState } from 'react';
import FriendCard from '../FriendCard/FriendCard';
import { useSelector } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowUp } from '@fortawesome/free-solid-svg-icons';
import { fetchUsersFromUIDs } from '../../services/contacts.service';

export default function FriendSuggestions() {
  const [showAllFriends, setShowAllFriends] = useState(false);

  const allUsers = useSelector((state) => state.allUsers.users);

  const userFriends = useSelector((state) => state.auth.userFriends);

  const [currentUserFriends, setCurrentUserFriends] = useState([]);

  useEffect(() => {
    if (!userFriends) return;
    const fetchFriends = async () => {
      const friends = await fetchUsersFromUIDs(userFriends);
      setCurrentUserFriends(friends);
    };

    fetchFriends();
  }, [userFriends]);

  const visibleUsers = showAllFriends
    ? currentUserFriends
    : currentUserFriends.slice(0, 5);

  // console.log(currentUserFriends);
  return (
    <div>
      <h1 className='text-3xl font-medium dark:text-white px-2 text-black mb-2'>
        My Friends
      </h1>

      {userFriends?.length === 0 ? (
        <div className='flex justify-center items-center text-center'>
          <div className='flex justify-center items-center text-2xl font-bold text-black dark:text-gray-200 h-56'>
            You don't have any friends yet.
          </div>
        </div>
      ) : (
        <ul
          role='list'
          className='grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 py-2'
        >
          {showAllFriends
            ? visibleUsers.map((user, i) => (
                <FriendCard
                  key={user.uid}
                  user={user}
                  isFriendSuggestions={true}
                />
              ))
            : visibleUsers
                ?.slice(0, 5)
                .map((user, i) => (
                  <FriendCard
                    key={user.uid}
                    user={user}
                    isFriendSuggestions={true}
                  />
                ))}
        </ul>
      )}

      {userFriends?.length !== 0 &&
        (!showAllFriends ? (
          <div className='flex justify-center mt-2'>
            <button
              onClick={() => setShowAllFriends(true)}
              className='text-blue-500 hover:underline cursor-pointer'
            >
              Show All Friends
            </button>
          </div>
        ) : (
          <div className='flex justify-center mt-2'>
            <button
              onClick={() => setShowAllFriends(false)}
              className='text-blue-500 hover:underline cursor-pointer'
            >
              <FontAwesomeIcon icon={faArrowUp} className='mr-1' /> Shrink
            </button>
          </div>
        ))}
    </div>
  );
}
