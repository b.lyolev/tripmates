import React, { useState } from 'react';
import Autosuggest from 'react-autosuggest';
import { SearchCircleIcon } from '@heroicons/react/outline';
import { hotels } from '../../common/hotels';
import './SearchHotels.css';
import { classNames } from '../../utils/helpers';
import { useSelector } from 'react-redux';
import LoginModal from '../LoginModal/LoginModal';

export default function SearchHotels({ handleSearch }) {
  const [value, setValue] = useState('');
  const [suggestions, setSuggestions] = useState([]);
  const [suggestionsVisible, setSuggestionsVisible] = useState(false);
  const user = useSelector((state) => state.auth.user);

  const [openLoginModal, setOpenLoginModal] = useState(false);

  const handleOpenLoginModal = () => {
    setOpenLoginModal(true);
  };

  const getSuggestions = (inputValue) => {
    const inputValueLower = inputValue.toLowerCase();
    return hotels.filter((hotel) =>
      hotel.name.toLowerCase().includes(inputValueLower)
    );
  };

  const onSuggestionsFetchRequested = ({ value }) => {
    const suggestions = getSuggestions(value);
    setSuggestions(suggestions);
    setSuggestionsVisible(true);
  };

  const onSuggestionsClearRequested = () => {
    setSuggestions([]);
    setSuggestionsVisible(false);
  };

  const getSuggestionValue = (suggestion) => suggestion.name;

  const renderSuggestion = (suggestion) => (
    <div>
      <a href={`/hotel-page/${suggestion.id}`}>{suggestion.name}</a>
    </div>
  );

  const inputProps = {
    placeholder: 'Search hotels',
    value,
    onChange: (_, { newValue }) => {
      setValue(newValue);
      // handleSearch(newValue);
    },
    onKeyDown: (e) => {
      if (e.key === 'Enter') {
        handleSearch(value);
        setSuggestionsVisible(false);
        if (!user) {
          handleOpenLoginModal();
        }
      }
    },
    className:
      'lg:w-[650px] md-custom:w-[620px] md:w-[470px] sm-custom:w-[400px] xm:w-[350px] xmm:w-[250px] w-[180px] text-xl h-16 ml-4 px-4 py-2 rounded-lg dark:bg-[#202020] text-gray-800 dark:text-white focus:outline-none',
  };

  const handleSearchClick = () => {
    handleSearch(value);
  };

  // const containerClassName = `flex items-center w-full h-28 bg-white dark:bg-[#202020] justify-between ${
  //   suggestionsVisible ? 'rounded-t' : 'rounded-full'
  // } p-4 max-sm:h-24`;

  //shadow-[0_0_10px_5px_#707070]  dark:shadow-[0_0_10px_5px_#cccccc]
  return (
    <div
      className={classNames(
        'flex items-center w-full h-28 bg-white dark:bg-[#202020] justify-between p-4 max-sm:h-24 border-4 border-blue-500 dark:border-[#c2df2b]',
        suggestionsVisible && suggestions.length !== 0
          ? 'rounded-t-[55px]'
          : 'rounded-full'
      )}
    >
      <Autosuggest
        suggestions={suggestions}
        onSuggestionsFetchRequested={onSuggestionsFetchRequested}
        onSuggestionsClearRequested={onSuggestionsClearRequested}
        getSuggestionValue={getSuggestionValue}
        renderSuggestion={renderSuggestion}
        inputProps={inputProps}
        className=''
      />
      {user ? (
        <button
          type='button'
          className='inline-flex  justify-center items-center  text-base font-medium   dark:text-black max-sm:h-12'
          onClick={handleSearchClick}
        >
          <SearchCircleIcon
            className=' h-20 text-blue-600 max-sm:h-14 dark:text-[#c2df2b] '
            aria-hidden='true'
          />
        </button>
      ) : (
        <>
          <button
            type='button'
            className='inline-flex  justify-center items-center  text-base font-medium   dark:text-black max-sm:h-12'
            onClick={handleOpenLoginModal}
          >
            <SearchCircleIcon
              className=' h-20 text-blue-600 max-sm:h-14 dark:text-[#c2df2b] '
              aria-hidden='true'
            />
          </button>
          <LoginModal open={openLoginModal} setOpen={setOpenLoginModal} />
        </>
      )}
    </div>
  );
}
