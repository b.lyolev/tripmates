import React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import { FreeMode, Pagination } from 'swiper/modules';
import 'swiper/swiper-bundle.css';
import { useSelector } from 'react-redux';
import EventCard from '../EventCard/EventCard';
import { useDispatch } from 'react-redux';
import { eventImage } from '../../common/constants/constants';
import { useEffect, useState } from 'react';
import './PublicEventsSwiper.css';

export default function PublicEventsSwiper() {
  const events = useSelector((state) => state.events.events);
  const { user } = useSelector((state) => state.auth);
  const [slidesPerView, setSlidesPerView] = useState(4); // Initial value

  useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth >= 1024) {
        setSlidesPerView(4);
      } else if (window.innerWidth >= 768) {
        setSlidesPerView(3);
      } else if (window.innerWidth >= 640) {
        setSlidesPerView(2);
      } else {
        setSlidesPerView(1);
      }
    };

    handleResize();

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  if (user) {
    return null;
  }

  return (
    <div className='swiper-container'>
      <div className='py-3'>
        <h1 className='text-3xl font-bold tracking-tight text-gray-900 dark:text-white mx-auto'>
          Recent public events
        </h1>
      </div>
      <section
        aria-labelledby='filter-heading'
        className='border-t border-gray-200 py-4'
      />
      <Swiper
        slidesPerView={slidesPerView}
        spaceBetween={30}
        freeMode={true}
        pagination={{
          clickable: true,
        }}
        modules={[FreeMode, Pagination]}
        className='mySwiper h-[380px]'
      >
        {events
          ?.filter((event) => event.eventType === 'public')
          .sort((a, b) => new Date(a.startDate) - new Date(b.startDate))
          .map((event) => (
            <SwiperSlide key={event.uid}>
              <EventCard
                authorUID={event.authorUID}
                eventUID={event.uid}
                author={event.authorHandle}
                title={event.title}
                description={event.description}
                startDate={event.startDate}
                endDate={event.endDate}
                eventType={event.eventType}
                eventRecurrence={event.eventRecurrence}
                eventRepeat={event.eventRepeat}
                color={event.color}
                eventParticipants={event.eventParticipants}
                img={event.isHotel ? event.images[0].image : eventImage}
                isHotel={event.isHotel}
                eventLocation={event.eventLocation}
                link={event.link}
              />
            </SwiperSlide>
          ))}
      </Swiper>
    </div>
  );
}
