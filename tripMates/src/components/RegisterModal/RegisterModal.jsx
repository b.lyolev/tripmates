/* This example requires Tailwind CSS v2.0+ */
import { Fragment, useRef, useState } from 'react';
import { Dialog, Transition } from '@headlessui/react';

import { useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faClose, faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { createUserHandle, getUserByHandle } from '../../services/user.service';
import { registerUser } from '../../services/auth.service';
import { Password } from 'primereact/password';
import {
  isFormInvalid,
  validateEmail,
  validateFirstName,
  validateHandle,
  validateLastName,
  validatePassword,
  validatePhoneNumber,
} from '../../utils/validations';
import { useDispatch } from 'react-redux';
import { setUser } from '../../reducers/authActions';
import Logo from '../Logo/Logo';

export default function RegisterModal({ open, setOpen }) {
  const dispatch = useDispatch();
  const [passwordConfirmation, setPasswordConfirmation] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const [showPasswordConfirmation, setShowPasswordConfirmation] =
    useState(false);
  const [passwordHasContent, setPasswordHasContent] = useState(false);
  const [passwordConfirmationHasContent, setPasswordConfirmationHasContent] =
    useState(false);

  const [form, setForm] = useState({
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    handle: '',
    phoneNumber: '',
  });

  const [validationMessages, setValidationMessages] = useState({
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    handle: '',
    phoneNumber: '',
  });

  const updatePasswordConfirmation = (e) => {
    const value = e.target.value;
    setPasswordConfirmation(value);
    setPasswordConfirmationHasContent(!!value);
  };

  const [error, setError] = useState(null);

  const navigate = useNavigate();

  const updateForm = (prop) => (e) => {
    const value = e.target.value;

    setForm({
      ...form,
      [prop]: value,
    });

    const newValidationMessages = { ...validationMessages };

    switch (prop) {
      case 'firstName':
        newValidationMessages.firstName = validateFirstName(value);
        break;
      case 'lastName':
        newValidationMessages.lastName = validateLastName(value);
        break;
      case 'email':
        newValidationMessages.email = validateEmail(value);
        break;
      case 'password':
        newValidationMessages.password = validatePassword(value);
        setPasswordHasContent(!!value);
        break;
      case 'handle':
        newValidationMessages.handle = validateHandle(value);
        break;
      case 'phoneNumber':
        newValidationMessages.phoneNumber = validatePhoneNumber(value);
        break;
      default:
        break;
    }

    setValidationMessages(newValidationMessages);
  };

  const onRegister = () => {
    if (
      validationMessages.firstName ||
      validationMessages.lastName ||
      validationMessages.email ||
      validationMessages.password ||
      validationMessages.handle ||
      validationMessages.phoneNumber
    ) {
      return;
    }
    if (form.password !== passwordConfirmation) {
      setError('Password and confirmation do not match.');
      return;
    }
    getUserByHandle(form.handle)
      .then((snapshot) => {
        if (snapshot.exists()) {
          throw new Error(`Handle @${form.handle} has already been taken!`);
        }

        return registerUser(form.email, form.password);
      })
      .then((credential) => {
        return createUserHandle(
          form.handle,
          credential.user.uid,
          form.firstName,
          form.lastName,
          credential.user.email,
          form.phoneNumber
        ).then(() => {
          dispatch(setUser({ user: credential.user }));
        });
      })
      .then(() => {
        navigate('/');
      })
      .catch((error) => setError(error.message));
  };

  const cancelButtonRef = useRef(null);

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as='div'
        className='fixed z-50 inset-0 overflow-y-auto '
        initialFocus={cancelButtonRef}
        onClose={setOpen}
      >
        <div className='flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0'>
          <Transition.Child
            as={Fragment}
            enter='ease-out duration-300'
            enterFrom='opacity-0'
            enterTo='opacity-100'
            leave='ease-in duration-200'
            leaveFrom='opacity-100'
            leaveTo='opacity-0'
          >
            <Dialog.Overlay className='fixed inset-0 bg-slate-600 bg-opacity-50 backdrop-blur-[2px] transition-opacity' />
          </Transition.Child>

          {/* This element is to trick the browser into centering the modal contents. */}
          <span
            className='hidden sm:inline-block sm:align-middle sm:h-screen'
            aria-hidden='true'
          >
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter='ease-out duration-300'
            enterFrom='opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95'
            enterTo='opacity-100 translate-y-0 sm:scale-100'
            leave='ease-in duration-200'
            leaveFrom='opacity-100 translate-y-0 sm:scale-100'
            leaveTo='opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95'
          >
            <div className='relative inline-block align-bottom bg-white dark:bg-[#202020] rounded-2xl border-4 border-blue-600 dark:border-[#c2df2b] px-4 pt-5 pb-4 text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-2xl sm:w-full sm:p-3 max-sm:w-full max-sm:p-0 max-sm:m-0'>
              <div>
                <button
                  onClick={() => setOpen(false)}
                  className='absolute top-3 right-3 text-2xl rounded-md bg-blue-100 dark:bg-[#f2f5cc] px-2 text-gray-600 hover:text-gray-800'
                >
                  <FontAwesomeIcon icon={faClose} />
                </button>
                <div className='items-center justify-center'>
                  <div className=' min-h-full flex flex-col justify-center py-10 sm:px-6 lg:px-8 '>
                    <div className='flex flex-col justify-center sm:mx-auto sm:w-full sm:max-w-md items-center text-center'>
                      <Logo />
                      <h2 className='mt-6 text-center text-3xl font-extrabold text-gray-900 dark:text-white'>
                        Create Account
                      </h2>
                    </div>

                    <div className='mt-8 sm:mx-auto sm:w-full sm:max-w-md'>
                      <div className='bg-white dark:bg-[#202020] py-8 px-4 md:shadow lg:shadow sm:rounded-lg'>
                        <div className='space-y-5'>
                          <div className='flex flex-row  gap-3 max-md:flex-col'>
                            <div className='col-span-6 sm:col-span-3'>
                              <label
                                htmlFor='first-name'
                                className='block text-sm font-medium text-gray-700 dark:text-white'
                              >
                                First name
                              </label>
                              <input
                                type='text'
                                name='first-name'
                                id='first-name'
                                autoComplete='given-name'
                                className='mt-1 appearance-none block w-full px-5 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm'
                                value={form.firstName}
                                onChange={updateForm('firstName')}
                              />
                              {validationMessages.firstName && (
                                <p className='text-red-500 text-xs mt-1'>
                                  {validationMessages.firstName}
                                </p>
                              )}
                            </div>

                            <div className='col-span-6 sm:col-span-3'>
                              <label
                                htmlFor='last-name'
                                className='block text-sm font-medium text-gray-700 dark:text-white'
                              >
                                Last name
                              </label>
                              <input
                                type='text'
                                name='last-name'
                                id='last-name'
                                autoComplete='family-name'
                                className='mt-1 appearance-none block w-full px-5 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm'
                                value={form.lastName}
                                onChange={updateForm('lastName')}
                              />
                              {validationMessages.lastName && (
                                <p className='text-red-500 text-xs mt-1'>
                                  {validationMessages.lastName}
                                </p>
                              )}
                            </div>
                          </div>

                          <div>
                            <label
                              htmlFor='email'
                              className='block text-sm font-medium text-gray-700 dark:text-white'
                            >
                              Email address
                            </label>
                            <div className='mt-1 '>
                              <input
                                id='email'
                                name='email'
                                type='email'
                                autoComplete='email'
                                required
                                className='appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm'
                                value={form.email}
                                onChange={updateForm('email')}
                              />
                              {validationMessages.email && (
                                <p className='text-red-500 text-xs mt-1'>
                                  {validationMessages.email}
                                </p>
                              )}
                            </div>
                          </div>

                          <div>
                            <label
                              htmlFor='handle'
                              className='block text-sm font-medium text-gray-700 dark:text-white'
                            >
                              Username
                            </label>
                            <div className='mt-1'>
                              <input
                                id='handle'
                                name='handle'
                                type='text'
                                autoComplete='handle'
                                required
                                className='appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm'
                                value={form.handle}
                                onChange={updateForm('handle')}
                              />
                              {validationMessages.handle && (
                                <p className='text-red-500 text-xs mt-1'>
                                  {validationMessages.handle}
                                </p>
                              )}
                            </div>
                          </div>
                          <div>
                            <label
                              htmlFor='phoneNumber'
                              className='block text-sm font-medium text-gray-700 dark:text-white'
                            >
                              Phone Number
                            </label>
                            <div className='mt-1'>
                              <input
                                id='phoneNumber'
                                name='phoneNumber'
                                type='text'
                                autoComplete='phoneNumber'
                                required
                                className='appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm'
                                value={form.phoneNumber}
                                onChange={updateForm('phoneNumber')}
                              />
                              {validationMessages.phoneNumber && (
                                <p className='text-red-500 text-xs mt-1'>
                                  {validationMessages.phoneNumber}
                                </p>
                              )}
                            </div>
                          </div>
                          <div>
                            <label
                              htmlFor='password'
                              className='block text-sm font-medium text-gray-700 dark:text-white'
                            >
                              Password
                            </label>
                            <div className='mt-1 relative'>
                              <input
                                id='password'
                                name='password'
                                type={showPassword ? 'text' : 'password'}
                                autoComplete='current-password'
                                required
                                className='appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm pr-8'
                                value={form.password}
                                onChange={updateForm('password')}
                              />
                              {passwordHasContent && (
                                <button
                                  type='button'
                                  onClick={() => setShowPassword(!showPassword)}
                                  className='absolute top-2 right-2 text-1xl text-gray-600 hover:text-gray-800'
                                >
                                  {showPassword ? (
                                    <FontAwesomeIcon icon={faEyeSlash} />
                                  ) : (
                                    <FontAwesomeIcon icon={faEye} />
                                  )}
                                </button>
                              )}
                              {validationMessages.password && (
                                <p className='text-red-500 text-xs mt-1'>
                                  {validationMessages.password}
                                </p>
                              )}
                            </div>
                          </div>

                          <div>
                            <label
                              htmlFor='password-confirmation'
                              className='block text-sm font-medium text-gray-700 dark:text-white'
                            >
                              Password Confirmation
                            </label>
                            <div className='mt-1 relative'>
                              <input
                                id='password-confirmation'
                                name='password-confirmation'
                                type={
                                  showPasswordConfirmation ? 'text' : 'password'
                                }
                                autoComplete='new-password'
                                required
                                className='appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm pr-8'
                                value={passwordConfirmation}
                                onChange={updatePasswordConfirmation}
                              />
                              {passwordConfirmationHasContent && (
                                <button
                                  type='button'
                                  onClick={() =>
                                    setShowPasswordConfirmation(
                                      !showPasswordConfirmation
                                    )
                                  }
                                  className='absolute top-2 right-2 text-1xl text-gray-600 hover:text-gray-800'
                                >
                                  {showPasswordConfirmation ? (
                                    <FontAwesomeIcon icon={faEyeSlash} />
                                  ) : (
                                    <FontAwesomeIcon icon={faEye} />
                                  )}
                                </button>
                              )}
                            </div>
                          </div>

                          <div className='flex items-center justify-between'></div>

                          <div>
                            <button
                              onClick={onRegister}
                              type='submit'
                              disabled={isFormInvalid(validationMessages, form)}
                              className={`w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white  focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 ${
                                isFormInvalid(validationMessages, form)
                                  ? 'bg-blue-300 dark:bg-[#d5e96d] cursor-not-allowed hover:bg-blue-300'
                                  : 'bg-blue-600 dark:bg-[#c2df2b] hover:bg-blue-700 dark:hover:bg-[#afdf2b]'
                              }`}
                            >
                              Sign up
                            </button>
                          </div>
                        </div>
                        {error && (
                          <div className='text-red-500 text-sm mt-2 text-center'>
                            {error}
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition.Root>
  );
}
