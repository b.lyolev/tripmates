import React from 'react';
import { Event } from '../Event/Event';

function EventList({
  events,
  currentWeekStartDate,
  currentWeekEndDate,
  currentDate,
}) {
  return (
    <>
      {events.map((event) => (
        <Event
          key={event.date + event.startDateTime + event.endDateTime}
          startDate={event.startDate}
          endDate={event.endDate}
          startDateTime={event.startDateTime}
          endDateTime={event.endDateTime}
          title={event.title}
          color={event.color}
          currentWeekStartDate={currentWeekStartDate}
          currentWeekEndDate={currentWeekEndDate}
          currentDate={currentDate}
        />
      ))}
    </>
  );
}

export default EventList;
