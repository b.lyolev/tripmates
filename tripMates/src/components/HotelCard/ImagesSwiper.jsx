import { Swiper, SwiperSlide } from 'swiper/react';

import {
  Navigation,
  Pagination,
  Mousewheel,
  Scrollbar,
  A11y,
} from 'swiper/modules';

import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';
export default function ImagesSwiper({ images }) {
  return (
    <Swiper
      style={{
        '--swiper-navigation-size': '20px',
        '--swiper-pagination-color': '#fff',
        '--swiper-navigation-color': '#fff',
      }}
      slidesPerView={1}
      spaceBetween={30}
      loop={true}
      pagination={{
        clickable: true,
      }}
      navigation={true}
      modules={[Pagination, Navigation]}
      className='mySwiper'
    >
      {images.map((imageObj, i) => (
        <SwiperSlide key={i}>
          <img
            className='w-full mx-auto rounded-xl h-48 object-center select-none '
            src={imageObj.image}
            alt=''
          />
        </SwiperSlide>
      ))}
    </Swiper>
  );
}
