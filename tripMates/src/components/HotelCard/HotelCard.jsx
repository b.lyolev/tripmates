import React from 'react';
import ImagesSwiper from './ImagesSwiper';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { useState } from 'react';
import LoginModal from '../LoginModal/LoginModal';

export default function HotelCard({
  images,
  name,
  location,
  price,
  rating,
  id,
}) {
  const user = useSelector((state) => state.auth.user);

  const [openLoginModal, setOpenLoginModal] = useState(false);

  const handleOpenLoginModal = () => {
    setOpenLoginModal(true);
  };

  return (
    <div className='shadow-md rounded-xl bg-white dark:bg-[#252525] dark:border border-[#353535]'>
      <ImagesSwiper images={images} />
      <div className='px-3 py-3 flex flex-col '>
        <div>
          <div className='font-bold text-xl flex justify-between items-start gap-1 h-24 dark:text-white'>
            <div>
              {user ? (
                <Link to={`/hotel-page/${id}`}>{name}</Link>
              ) : (
                <p onClick={handleOpenLoginModal}>{name}</p>
              )}
              <p className='text-gray-500  font-medium text-base  dark:text-gray-300'>
                {location}
              </p>
            </div>
            <div className='flex items-center mt-1 text-gray-700 text-base  dark:text-gray-300'>
              {rating}
              <FontAwesomeIcon icon={faStar} className='ml-1 text-yellow-500' />
            </div>
          </div>
        </div>
        <div className='flex flex-row justify-between items-end '>
          <div className='text-black text-base font-bold dark:text-white'>
            {price}${' '}
            <span className='font-medium text-gray-800 dark:text-gray-300'>
              for night
            </span>
          </div>
          {user ? (
            <Link
              to={`/hotel-page/${id}`}
              className='text-white font-bold rounded-xl bg-blue-500 hover:bg-blue-700 py-2 px-4  dark:bg-[#c2df2b] dark:hover:bg-[#adc050] dark:text-black'
            >
              Book now
            </Link>
          ) : (
            <>
              <button
                className='text-white font-bold rounded-xl bg-blue-500 hover:bg-blue-700 py-2 px-4  dark:bg-[#c2df2b] dark:hover:bg-[#adc050] dark:text-black'
                onClick={handleOpenLoginModal}
              >
                Book now
              </button>
              <LoginModal open={openLoginModal} setOpen={setOpenLoginModal} />
            </>
          )}
        </div>
      </div>
    </div>
  );
}
