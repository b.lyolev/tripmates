import { faMoon, faSun } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useEffect, useState } from 'react';
import { motion } from 'framer-motion';

const DarkModeToggle = () => {
  const [darkMode, setDarkMode] = useState(false);

  useEffect(() => {
    const savedDarkMode = localStorage.getItem('darkMode');
    if (savedDarkMode) {
      setDarkMode(savedDarkMode === 'true');
      document.documentElement.classList.toggle(
        'dark',
        savedDarkMode === 'true'
      );
    }
  }, []);

  const toggleDarkMode = () => {
    setDarkMode(!darkMode);
    document.documentElement.classList.toggle('dark');
    localStorage.setItem('darkMode', !darkMode);
  };

  return (
    <div className='dark:text-white'>
      <motion.button
        className='flex items-center justify-center dark:bg-[#171717] rounded-full p-2 h-10 w-10  bg-slate-100 hover:bg-slate-100 py-2  text-gray-400  focus:relative  dark:hover:bg-gray-800   transition-colors duration-300'
        onClick={toggleDarkMode}
      >
        <motion.div
          initial={{ rotate: 0 }}
          whileHover={{ rotate: !darkMode ? 0 : 360 }} // Rotate on hover
          className='text-blue-500 dark:text-yellow-400'
        >
          <FontAwesomeIcon icon={darkMode ? faSun : faMoon} />
        </motion.div>
      </motion.button>
    </div>
  );
};

export default DarkModeToggle;
