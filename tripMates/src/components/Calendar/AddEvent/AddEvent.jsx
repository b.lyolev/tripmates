import React from 'react';
import { Fragment, useRef, useState, useEffect } from 'react';
import { Dialog, Transition } from '@headlessui/react';
import { useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faClose,
  faLocationArrow,
  faLocationCrosshairs,
} from '@fortawesome/free-solid-svg-icons';
import ReactDatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

import {
  isFormInvalid,
  validateTitle,
  validateDescription,
  isEventValid,
} from '../../../utils/validations';
import { QuestionMarkCircleIcon } from '@heroicons/react/solid';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import { MultiSelect } from 'primereact/multiselect';
import moment from 'moment';
import {
  addEventToDatabase,
  createEvent,
} from '../../../services/event.service';
import ColorPalette from '../../ColorPalette/ColorPalette';
import LocationPicker from '../../LocationPicker/LocationPicker';
import Logo from '../../Logo/Logo';
import {
  addDays,
  endOfDay,
  getDate,
  setHours,
  setMinutes,
  subDays,
} from 'date-fns';
import { useSelector } from 'react-redux';
import {
  Avatar,
  AvatarGroup,
  Checkbox,
  CheckboxGroup,
  RadioGroup,
  Slider,
  SliderFilledTrack,
  SliderThumb,
  SliderTrack,
  Tooltip,
} from '@chakra-ui/react';
import { is } from 'date-fns/locale';
import { colors } from '../../../common/colors';
import { calculateMaxTime, calculateMinTime } from '../../../utils/helpers';
import { fetchUsersFromUIDs } from '../../../services/contacts.service';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function AddEvent({ open, setOpen }) {
  const { userData } = useSelector((state) => state.auth);

  const [isLocationPickerFullScreen, setLocationPickerFullScreen] =
    useState(false);

  const [selectedLocation, setSelectedLocation] = useState(null);
  const [isLocationPicked, setLocationPicked] = useState(false);
  const [selectedColor, setSelectedColor] = useState(null);
  const [selectedParticipants, setSelectedParticipants] = useState('');
  const [currentDateTime, setCurrentDateTime] = useState(
    new Date().toISOString().split('T')[0]
  );

  const [selectedRecurrence, setSelectedRecurrence] = useState('');

  const [eventRepeat, setEventRepeat] = useState(1);

  const handleColorSelect = (color) => {
    setSelectedColor(color);
  };

  const [form, setForm] = useState({
    title: '',
    description: '',
    startDate: '',
    endDate: '',
    eventType: '',
    eventRecurrence: '',
    color: '',
  });

  const [validationMessages, setValidationMessages] = useState({
    title: '',
    description: '',
  });

  const userFriends = useSelector((state) => state.auth.userFriends);

  const [currentUserFriends, setCurrentUserFriends] = useState([]);

  useEffect(() => {
    if (!userFriends) return;
    const fetchFriends = async () => {
      const friends = await fetchUsersFromUIDs(userFriends);
      setCurrentUserFriends(friends);
    };

    fetchFriends();
  }, [userFriends]);

  const participantsOptions = currentUserFriends.map((friend) => ({
    name: `${friend.firstName} ${friend.lastName}`,
    handle: friend.handle,
  }));

  const [currentParticipants, setCurrentParticipants] = useState([]);

  useEffect(() => {
    if (!selectedParticipants) return;
    const fetchFriends = async () => {
      const participants = await fetchUsersFromUIDs(
        selectedParticipants.map((person) => person.handle)
      );
      setCurrentParticipants(participants);
    };

    fetchFriends();
  }, [selectedParticipants]);

  // console.log('Selected participants:', currentParticipants);
  const [error, setError] = useState(null);

  const navigate = useNavigate();

  const updateForm = (prop) => (e) => {
    const value = e.target.value;

    setForm({
      ...form,
      [prop]: value,
    });

    const newValidationMessages = { ...validationMessages };

    switch (prop) {
      case 'title':
        newValidationMessages.title = validateTitle(value);
        break;
      case 'description':
        newValidationMessages.description = validateDescription(value);
        break;
      default:
        break;
    }

    setValidationMessages(newValidationMessages);
  };

  const onCreateEvent = async () => {
    if (
      validationMessages.title ||
      validationMessages.description ||
      new Date(form.startDate) >= new Date(form.endDate)
    ) {
      // Display a warning message when the validation fails
      setError('Start date must be earlier than the end date.');
      return;
    }

    // Clear the error message if validation succeeds
    setError(null);
    createEvent(
      userData.uid,
      userData.handle,
      new Date(form.startDate).getTime(),
      new Date(form.endDate).getTime(),
      form.title,
      form.description,
      form.eventType || 'public',
      selectedParticipants.length > 0
        ? userData.handle +
            ',' +
            selectedParticipants.map((person) => person.handle).join(',')
        : userData.handle,
      selectedRecurrence || 'none',
      eventRepeat,
      selectedLocation,
      selectedColor || colors[0],
      false,
      null,
      null
    )
      .then(() => {
        toast.success('Event created successfully!', { autoClose: 2000 });
        setForm({
          title: '',
          description: '',
          startDate: '',
          endDate: '',
          eventType: '',
          eventParticipants: '',
          color: '',
        });
        setSelectedRecurrence('');
        setEventRepeat(1);
        setLocationPicked(false);
        setSelectedParticipants([]); // Reset selected participants array
        setSelectedLocation(null); // Reset selected location
      })
      .catch((e) =>
        toast.error('Error creating event: ' + e.message, { autoClose: 2000 })
      );
  };

  const cancelButtonRef = useRef(null);

  const handleLocationPickerSave = (selectedLocation) => {
    setSelectedLocation(selectedLocation);
    setLocationPicked(true);
    setLocationPickerFullScreen(false); // Close the map here
  };

  // console.log('Selected location:', selectedLocation);

  const isEventLongerThanOneDay = () => {
    const startDate = new Date(form.startDate);
    const endDate = new Date(form.endDate);

    // Compare the year, month, and day parts of the dates
    return (
      startDate.getFullYear() !== endDate.getFullYear() ||
      startDate.getMonth() !== endDate.getMonth() ||
      startDate.getDate() !== endDate.getDate()
    );
  };

  const handleRecurrenceChange = (value) => {
    const isMultiDayEvent = isEventLongerThanOneDay();

    if (value === 'daily' && isMultiDayEvent) {
      return; // Do not allow "Daily" recurrence for multi-day events
    }

    setSelectedRecurrence(value === selectedRecurrence ? '' : value);
    setEventRepeat(1); // Reset eventRepeat when changing recurrence option
  };

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as='div'
        className='fixed z-50 inset-0 overflow-y-auto '
        initialFocus={cancelButtonRef}
        onClose={setOpen}
      >
        <div className='flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block'>
          <Transition.Child
            as={Fragment}
            enter='ease-out duration-300'
            enterFrom='opacity-0'
            enterTo='opacity-100'
            leave='ease-in duration-200'
            leaveFrom='opacity-100'
            leaveTo='opacity-0'
          >
            <Dialog.Overlay className='fixed inset-0 bg-slate-600 bg-opacity-50 backdrop-blur-[2px] transition-opacity' />
          </Transition.Child>

          {/* This element is to trick the browser into centering the modal contents. */}
          <span
            className='hidden sm:inline-block sm:align-middle sm:h-screen'
            aria-hidden='true'
          >
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter='ease-out duration-300'
            enterFrom='opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95'
            enterTo='opacity-100 translate-y-0 sm:scale-100'
            leave='ease-in duration-200'
            leaveFrom='opacity-100 translate-y-0 sm:scale-100'
            leaveTo='opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95'
          >
            <div className='relative inline-block align-bottom dark:bg-[#171717] bg-white rounded-2xl  text-left overflow-hidden   shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-3xl sm:w-full sm:p-6 max-sm:w-full  max-sm:p-0 max-sm:m-0 ring  ring-blue-500 dark:ring-[#c2df2b] '>
              <div className=''>
                <button
                  onClick={() => setOpen(false)}
                  className='absolute top-3 right-3 text-2xl rounded-md bg-blue-500  dark:bg-[#c2df2b] px-2 text-white dark:text-black'
                >
                  <FontAwesomeIcon icon={faClose} />
                </button>
                <div className='items-center justify-center '>
                  <div className=' min-h-full flex flex-col justify-center  sm:px-6 lg:px-8 '>
                    <div className='space-y-6 sm:px-6 lg:px-0 lg:col-span-9 '>
                      <section>
                        <div>
                          <div className=' sm:rounded-md sm:overflow-hidden  '>
                            <div className=' py-3 px-4  '>
                              <div className='flex flex-row items-center justify-center sm:mx-auto sm:w-full sm:max-w-md max-sm:mt-3 '>
                                <Logo />
                                <h2 className=' ml-2 text-center text-3xl font-extrabold text-blue-500 dark:text-[#c2df2b] '>
                                  Book Event
                                </h2>
                              </div>

                              <div className='mt-6 grid grid-cols-4 gap-6'>
                                <div className='col-span-4 sm:col-span-2'>
                                  <label
                                    htmlFor='event-title'
                                    className='block text-sm font-medium text-gray-700 dark:text-white'
                                  >
                                    Event Title
                                  </label>
                                  <input
                                    type='text'
                                    name='title'
                                    id='title'
                                    autoComplete='off'
                                    maxLength={30}
                                    minLength={3}
                                    placeholder='Enter event title'
                                    className='mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-gray-900 focus:border-gray-900 sm:text-sm  dark:border-gray-700 dark:bg-[#252525] dark:text-white dark:focus:ring-gray-400 dark:focus:border-gray-400'
                                    value={form.title}
                                    onChange={updateForm('title')}
                                  />
                                  {validationMessages.title && (
                                    <p className='text-red-500 text-xs mt-1'>
                                      {validationMessages.title}
                                    </p>
                                  )}
                                </div>

                                <div className='col-span-4 sm:col-span-2'>
                                  <label
                                    htmlFor='event-type'
                                    className='block text-sm font-medium text-gray-700 dark:text-white'
                                  >
                                    Event Type
                                  </label>
                                  <select
                                    id='event-type'
                                    name='event-type'
                                    className='mt-1 block w-full bg-white border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-gray-900 focus:border-gray-900 sm:text-sm  dark:border-gray-700 dark:bg-[#252525] dark:text-white dark:focus:ring-gray-400 dark:focus:border-gray-400'
                                    value={form.eventType}
                                    onChange={updateForm('eventType')}
                                  >
                                    <option value=''>Select event type</option>
                                    <option value='public'>Public</option>
                                    <option value='private'>Private</option>
                                  </select>
                                </div>

                                <div className='col-span-4 sm:col-span-2'>
                                  <label
                                    htmlFor='start-date'
                                    className='block text-sm font-medium text-gray-700 dark:text-white'
                                  >
                                    Start Date and Time
                                  </label>

                                  <ReactDatePicker
                                    placeholderText='Pick start date and time'
                                    wrapperClassName='w-full'
                                    selected={form.startDate}
                                    onChange={(date) =>
                                      updateForm('startDate')({
                                        target: { value: date },
                                      })
                                    }
                                    showTimeSelect
                                    dateFormat='dd.MM.yyyy HH:mm'
                                    timeFormat='HH:mm'
                                    minDate={new Date()}
                                    className='mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-gray-900 focus:border-gray-900 sm:text-sm  dark:border-gray-700 dark:bg-[#252525] dark:text-white dark:focus:ring-gray-400 dark:focus:border-gray-400'
                                  />
                                </div>

                                <div className='col-span-4 sm:col-span-2 '>
                                  <label
                                    htmlFor='end-date'
                                    className='block text-sm font-medium text-gray-700 dark:text-white'
                                  >
                                    End Date and Time
                                  </label>

                                  <ReactDatePicker
                                    minTime={calculateMinTime(
                                      form.startDate,
                                      form.endDate
                                    )}
                                    maxTime={
                                      calculateMinTime(
                                        form.startDate,
                                        form.endDate
                                      )
                                        ? calculateMaxTime(form.startDate)
                                        : null
                                    }
                                    wrapperClassName='w-full'
                                    selected={form.endDate}
                                    onChange={(date) =>
                                      updateForm('endDate')({
                                        target: { value: date },
                                      })
                                    }
                                    showTimeSelect
                                    dateFormat='dd.MM.yyyy HH:mm'
                                    timeFormat='HH:mm'
                                    placeholderText='Pick end date and time'
                                    minDate={form?.startDate || new Date()} // Minimum end date based on start date
                                    className='mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-gray-900 focus:border-gray-900 sm:text-sm  dark:border-gray-700 dark:bg-[#252525] dark:text-white dark:focus:ring-gray-400 dark:focus:border-gray-400'
                                    customInput={<input />} // Use custom input element
                                  />
                                </div>

                                <div className='col-span-4'>
                                  <label
                                    htmlFor='event-description'
                                    className='block text-sm font-medium text-gray-700 dark:text-white'
                                  >
                                    Event Description (up to 500 characters)
                                  </label>
                                  <textarea
                                    id='event-description'
                                    name='event-description'
                                    rows='3'
                                    maxLength={500}
                                    placeholder='Enter event description'
                                    className='mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-gray-900 focus:border-gray-900 sm:text-sm  dark:border-gray-700 dark:bg-[#252525] dark:text-white dark:focus:ring-gray-400 dark:focus:border-gray-400'
                                    value={form.description}
                                    onChange={updateForm('description')}
                                  ></textarea>
                                  {validationMessages.description && (
                                    <p className='text-red-500 text-xs mt-1'>
                                      {validationMessages.description}
                                    </p>
                                  )}
                                </div>

                                <div
                                  className={`col-span-4 ${
                                    selectedParticipants.length > 0 &&
                                    'sm:col-span-2'
                                  }`}
                                >
                                  <div className='card flex justify-content-center flex-col'>
                                    <label
                                      htmlFor='event-description'
                                      className='block text-sm font-medium text-gray-700 dark:text-white mb-1'
                                    >
                                      Event Participants
                                    </label>
                                    <MultiSelect
                                      value={selectedParticipants}
                                      onChange={(e) =>
                                        setSelectedParticipants(e.value)
                                      }
                                      options={participantsOptions}
                                      optionLabel='name'
                                      filter
                                      placeholder='Select Participants'
                                      maxSelectedLabels={4}
                                      display='chip'
                                      className='w-full md:w-20rem h-12  items-center hover:border-black  dark:border-gray-700 dark:bg-[#252525] dark:text-white dark:focus:ring-gray-400 dark:focus:border-gray-400'
                                    />
                                  </div>
                                </div>

                                {selectedParticipants.length > 0 && (
                                  <div className='col-span-4 sm:col-span-2 text-gray-700 dark:text-white '>
                                    <label
                                      htmlFor='event-recurrence'
                                      className='block text-sm font-medium text-gray-700 dark:text-white mb-1'
                                    >
                                      Selected Participants
                                    </label>
                                    <AvatarGroup size='md' max={4} gap={6}>
                                      {currentParticipants.map(
                                        (attendee, i) => (
                                          <Tooltip
                                            key={i}
                                            label={`${attendee.firstName} ${attendee.lastName}`}
                                            fontSize='md'
                                          >
                                            <Avatar
                                              key={i}
                                              name={`${attendee.firstName} ${attendee.lastName}`}
                                              src={attendee.profileImage}
                                              size='md'
                                              borderColor='gray-200'
                                            />
                                          </Tooltip>
                                        )
                                      )}
                                    </AvatarGroup>
                                  </div>
                                )}

                                <div className='col-span-4 sm:col-span-2 text-gray-700 dark:text-white '>
                                  <label
                                    htmlFor='event-recurrence'
                                    className='block text-sm font-medium text-gray-700 dark:text-white'
                                  >
                                    Event Recurrence
                                  </label>

                                  <div className='flex gap-3 mt-1 items-center'>
                                    <input
                                      id='daily'
                                      type='checkbox'
                                      className='accent-blue-500 dark:accent-[#c2df2b] w-4 h-4'
                                      checked={selectedRecurrence === 'daily'}
                                      disabled={isEventLongerThanOneDay()}
                                      onChange={() =>
                                        handleRecurrenceChange('daily')
                                      }
                                    />
                                    <label htmlFor='daily'>Daily</label>

                                    <input
                                      id='weekly'
                                      type='checkbox'
                                      className='accent-blue-500 dark:accent-[#c2df2b] w-4 h-4'
                                      checked={selectedRecurrence === 'weekly'}
                                      onChange={() =>
                                        handleRecurrenceChange('weekly')
                                      }
                                    />
                                    <label htmlFor='weekly'>Weekly</label>

                                    <input
                                      id='monthly'
                                      type='checkbox'
                                      className='accent-blue-500 dark:accent-[#c2df2b] w-4 h-4'
                                      checked={selectedRecurrence === 'monthly'}
                                      onChange={() =>
                                        handleRecurrenceChange('monthly')
                                      }
                                    />
                                    <label htmlFor='monthly'>Monthly</label>
                                  </div>
                                </div>

                                <div className='col-span-4 sm:col-span-2 text-gray-700 dark:text-white '>
                                  {selectedRecurrence && (
                                    <div className='mt-1'>
                                      <label
                                        htmlFor='event-interval'
                                        className='block text-sm font-medium text-black dark:text-white'
                                      >
                                        Repeat{' '}
                                        <span>
                                          {eventRepeat}{' '}
                                          {selectedRecurrence === 'daily'
                                            ? 'days'
                                            : selectedRecurrence === 'weekly'
                                            ? 'weeks'
                                            : 'months'}
                                        </span>
                                      </label>
                                      <Slider
                                        value={eventRepeat}
                                        onChange={setEventRepeat}
                                        min={1}
                                        max={30} // Adjust the maximum value as needed
                                      >
                                        <SliderTrack bg={'#E2E8F0'}>
                                          <SliderFilledTrack bg={'#4A5568'} />
                                        </SliderTrack>
                                        <SliderThumb />
                                      </Slider>
                                    </div>
                                  )}
                                </div>
                                <div className='col-span-4 sm:col-span-2 text-left flex flex-col'>
                                  <label
                                    htmlFor='event-location'
                                    className='block text-sm font-medium text-gray-700 dark:text-white mb-2'
                                  >
                                    Event Location
                                  </label>

                                  <button
                                    className='block  py-1 w-full text-center text-sm border-4 rounded-md shadow-sm font-bold text-black dark:text-white focus:outline-none focus:ring-2 focus:ring-blue-500 dark:focus:ring-[#def765] hover:bg-blue-300 dark:border-[#c2df2b] border-blue-500 dark:hover:bg-[#8f9e41]'
                                    onClick={() => {
                                      setLocationPicked(false);
                                      setSelectedLocation(null);

                                      setLocationPickerFullScreen(true);
                                    }}
                                  >
                                    {isLocationPicked ? (
                                      <>
                                        My Location
                                        <span className='ml-2 text-lg items-center'>
                                          <FontAwesomeIcon
                                            icon={faLocationCrosshairs}
                                          />
                                        </span>
                                      </>
                                    ) : (
                                      <>
                                        Pick Location
                                        <span className='ml-2 text-lg items-center'>
                                          <FontAwesomeIcon
                                            icon={faLocationArrow}
                                          />
                                        </span>
                                      </>
                                    )}
                                  </button>
                                  {isLocationPickerFullScreen && (
                                    <div className='fixed inset-0 z-50 bg-white'>
                                      <LocationPicker
                                        onClose={() =>
                                          setLocationPickerFullScreen(false)
                                        }
                                        onSave={handleLocationPickerSave}
                                      />
                                    </div>
                                  )}
                                </div>

                                <div className=' col-span-4 sm:col-span-2 text-center '>
                                  <ColorPalette
                                    onSelectColor={handleColorSelect}
                                  />
                                </div>
                              </div>
                            </div>
                            <div className='flex justify-center py-3 '>
                              <button
                                onClick={onCreateEvent}
                                type='submit'
                                disabled={isEventValid(
                                  validationMessages,
                                  form
                                )}
                                className={` flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-lg font-medium text-white dark:text-black  focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500  dark:focus:ring-[#9dad48] ${
                                  isEventValid(validationMessages, form)
                                    ? 'bg-blue-300 cursor-not-allowed dark:bg-[#7d884e]'
                                    : 'bg-blue-600 hover:bg-blue-700 dark:bg-[#c2df2b] dark:hover:bg-[#cee071]'
                                }`}
                              >
                                Create Event
                              </button>
                            </div>

                            {error && (
                              <div className='text-red-500 text-sm mt-2 text-center'>
                                {error}
                              </div>
                            )}
                          </div>
                        </div>
                      </section>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition.Root>
  );
}
