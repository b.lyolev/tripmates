import { Fragment, useEffect, useRef, useState } from 'react';
import {
  ChevronDownIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
  DotsHorizontalIcon,
} from '@heroicons/react/solid';
import { Menu, Transition } from '@headlessui/react';
import DayView from './DayView';
import CalendarHeader from '../CalendarHeader/CalendarHeader';
import { useDisclosure } from '@chakra-ui/react';
import { useDispatch, useSelector } from 'react-redux';
import { setSelectedDate } from '../../../reducers/selectedDateActions';
import { is } from 'date-fns/locale';
import { setCurrentWeek } from '../../../reducers/currentWeekActions';

export default function DayViewWithMonth() {
  const dispatch = useDispatch();

  const selectedDate = useSelector((state) => state.selectedDate);
  const currentWeek = useSelector((state) => state.currentWeek);

  const [currentMonth, setCurrentMonth] = useState(new Date());

  const [currentMonthYear, setCurrentMonthYear] = useState(
    new Date().toLocaleString('en-US', {
      month: 'long',
      year: 'numeric',
    })
  );

  //   const handlePreviousWeek = () => {
  //     setCurrentWeek((prevWeek) => {
  //       const newWeek = new Date(prevWeek);
  //       newWeek.setDate(prevWeek.getDate() - 7);

  //       const monthYear = newWeek.toLocaleString('en-US', {
  //         month: 'long',
  //         year: 'numeric',
  //       });
  //       setCurrentMonthYear(monthYear);

  //       return newWeek;
  //     });
  //   };

  //   const handleNextWeek = () => {
  //     setCurrentWeek((prevWeek) => {
  //       const newWeek = new Date(prevWeek);
  //       newWeek.setDate(prevWeek.getDate() + 7);

  //       const monthYear = newWeek.toLocaleString('en-US', {
  //         month: 'long',
  //         year: 'numeric',
  //       });
  //       setCurrentMonthYear(monthYear);

  //       return newWeek;
  //     });
  //   };

  //   const handleThisWeek = () => {
  //     setCurrentWeek(new Date());

  //     const monthYear = new Date().toLocaleString('en-US', {
  //       month: 'long',
  //       year: 'numeric',
  //     });
  //     setCurrentMonthYear(monthYear);
  //   };

  function getDaysInMonth(date, currentDate) {
    const year = date.getFullYear();
    const month = date.getMonth();
    const firstDay = new Date(year, month, 1);
    const lastDay = new Date(year, month + 1, 0);
    const timezoneOffset = date.getTimezoneOffset();
    const startDay = new Date(year, month, 1);
    startDay.setDate(-2);
    startDay.setMinutes(startDay.getMinutes() + timezoneOffset);
    const endDay = new Date(year, month, 1);
    endDay.setDate(39);
    endDay.setMinutes(endDay.getMinutes() + timezoneOffset);

    const days = [];

    for (let day = startDay; day <= endDay; day.setDate(day.getDate() + 1)) {
      const isCurrentMonth =
        day >= firstDay &&
        (day <= lastDay || day.getDate() === lastDay.getDate());

      currentDate.setHours(0, 0, 0, 0);
      currentDate.setDate(currentDate.getDate() + 1);
      currentDate.setHours(currentDate.getHours() - 3);

      const isToday = day.getTime() === currentDate.getTime(); // Compare with currentDate
      const isSelected = selectedDate === day.getTime();

      days.push({
        date: day.getTime(),
        isCurrentMonth,
        isToday,
        isSelected,
      });
    }

    return days;
  }

  const handleDateClick = (date) => {
    dispatch(setSelectedDate(new Date(date).getTime()));
  };

  const goToPreviousMonth = () => {
    const previousMonth = new Date(currentMonth);
    previousMonth.setMonth(previousMonth.getMonth() - 1);

    setCurrentMonth(previousMonth);
  };

  const goToNextMonth = () => {
    const nextMonth = new Date(currentMonth);
    nextMonth.setMonth(nextMonth.getMonth() + 1);
    setCurrentMonth(nextMonth);
  };

  const days = getDaysInMonth(currentMonth, new Date());

  const handlePreviousDay = () => {
    const newSelectedDate = new Date(selectedDate);
    newSelectedDate.setDate(newSelectedDate.getDate() - 1);

    // Check if the selected date is 4 days before the current date
    const currentDate = new Date();
    currentDate.setDate(currentDate.getDate() - 4);

    console.log(new Date(selectedDate));
    console.log(newSelectedDate);

    // Check if the selected date is a Sunday (first day of the week)
    if (newSelectedDate.getDay() < 0 || newSelectedDate.getDay() === 6) {
      // Subtract 7 days from the current week
      const newCurrentWeek = new Date(currentWeek);
      newCurrentWeek.setDate(newCurrentWeek.getDate() - 6);
      dispatch(setCurrentWeek(newCurrentWeek.getTime()));

      // Set the selected date to the last day of the previous week
      newSelectedDate.setDate(newSelectedDate.getDate());
    }

    // Update currentMonthYear when the month changes
    if (newSelectedDate.getMonth() !== currentMonth.getMonth()) {
      setCurrentMonthYear(
        newSelectedDate.toLocaleString('en-US', {
          month: 'long',
          year: 'numeric',
        })
      );
    }

    dispatch(setSelectedDate(newSelectedDate.getTime()));
  };

  const handleNextDay = () => {
    const newSelectedDate = new Date(selectedDate);
    newSelectedDate.setDate(newSelectedDate.getDate() + 1);

    // Check if the selected date is 4 days after the current date
    const currentDate = new Date();
    currentDate.setDate(currentDate.getDate() + 4);

    // Check if the selected date is a Saturday (last day of the week)
    if (newSelectedDate.getDay() === 7 || newSelectedDate.getDay() === 0) {
      // Add 7 days to the current week
      const newCurrentWeek = new Date(currentWeek);
      newCurrentWeek.setDate(newCurrentWeek.getDate() + 7);
      dispatch(setCurrentWeek(newCurrentWeek.getTime()));

      // Set the selected date to the first day of the next week
      newSelectedDate.setDate(newSelectedDate.getDate());
    }

    // Update currentMonthYear when the month changes
    if (newSelectedDate.getMonth() !== currentMonth.getMonth()) {
      setCurrentMonthYear(
        newSelectedDate.toLocaleString('en-US', {
          month: 'long',
          year: 'numeric',
        })
      );
    }

    dispatch(setSelectedDate(newSelectedDate.getTime()));
  };

  const handleToday = () => {
    const today = new Date();
    dispatch(setSelectedDate(today.getTime()));
  };

  return (
    <div className='flex justify-center items-center  mt-6 my-10'>
      <div className='flex  flex-col  h-[800px] w-full max-w-7xl md:mx-10 sm:border-8   border-gray-200  dark:border-gray-700 rounded-2xl'>
        <CalendarHeader
          currentMonthYear={currentMonthYear}
          handlePrevious={handlePreviousDay}
          handleToday={handleToday}
          handleNext={handleNextDay}
        />
        <div className='flex flex-auto overflow-hidden bg-white dark:bg-[#202020]'>
          <DayView />

          <div className='hidden w-1/2 max-w-md flex-none border-l border-gray-100   py-10 px-8 md:block'>
            <div className='flex items-center text-center text-gray-900 dark:text-white '>
              <button
                type='button'
                className='-m-1.5 flex flex-none items-center justify-center p-1.5  text-gray-400 hover:text-gray-500'
                onClick={goToPreviousMonth}
              >
                <span className='sr-only'>Previous month</span>
                <ChevronLeftIcon className='h-5 w-5' aria-hidden='true' />
              </button>
              <div className='flex-auto font-semibold'>
                {currentMonth.toLocaleString('default', { month: 'long' })}{' '}
                {currentMonth.getFullYear()}
              </div>
              <button
                type='button'
                className='-m-1.5 flex flex-none items-center justify-center p-1.5 text-gray-400  hover:text-gray-500'
                onClick={goToNextMonth}
              >
                <span className='sr-only'>Next month</span>
                <ChevronRightIcon className='h-5 w-5' aria-hidden='true' />
              </button>
            </div>
            <div className='mt-6 grid grid-cols-7 text-center text-xs leading-6 dark:text-gray-200  text-gray-500'>
              <div>M</div>
              <div>T</div>
              <div>W</div>
              <div>T</div>
              <div>F</div>
              <div>S</div>
              <div>S</div>
            </div>
            <div className='isolate mt-2 grid grid-cols-7 gap-px rounded-lg bg-gray-200 dark:bg-gray-600 text-sm shadow ring-1 ring-gray-200 dark:ring-gray-700'>
              {days.map((day) => (
                <button
                  key={day.date}
                  type='button'
                  className={`py-1.5 hover:bg-gray-100 dark:hover:bg-[#171717] focus:z-10 ${
                    day.isCurrentMonth
                      ? 'bg-white dark:bg-[#212121]'
                      : 'bg-gray-50 dark:bg-[#191919]'
                  } ${(day.isSelected || day.isToday) && 'font-semibold'} ${
                    day.isSelected && 'text-white dark:text-black'
                  } ${
                    !day.isSelected &&
                    day.isCurrentMonth &&
                    !day.isToday &&
                    'text-gray-900 dark:text-gray-100'
                  } ${
                    !day.isSelected &&
                    !day.isCurrentMonth &&
                    !day.isToday &&
                    'text-gray-400 '
                  } ${
                    day.isToday &&
                    !day.isSelected &&
                    'text-black dark:text-white  '
                  } ${day.date === selectedDate ? 'border-blue-600' : ''}`}
                  onClick={() => handleDateClick(day.date)}
                >
                  <time
                    dateTime={day.date}
                    className={`mx-auto flex h-7 w-7 items-center justify-center rounded-full ${
                      day.isSelected && day.isToday
                        ? 'bg-blue-600 dark:bg-[#c2df2b]'
                        : day.isSelected
                        ? 'bg-blue-400 dark:bg-[#e8f7a2]'
                        : ''
                    }
                   ${
                     day.isToday &&
                     !day.isSelected &&
                     'border border-blue-500 dark:border-[#c2df2b]'
                   }
                   `}
                  >
                    {new Date(day.date).getDate()}{' '}
                  </time>
                </button>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
