import { Fragment, useEffect, useState } from 'react';
import {
  ChevronDownIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
  ClockIcon,
} from '@heroicons/react/solid';
import { Menu, Transition } from '@headlessui/react';
import {
  addMonths,
  subMonths,
  startOfMonth,
  isSameMonth,
  isToday,
} from 'date-fns';
import { da } from 'date-fns/locale';
import { useSelector } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHotel } from '@fortawesome/free-solid-svg-icons';
import CalendarHeader from '../CalendarHeader/CalendarHeader';
import EditEvent from '../../EditEvent/EditEvent';
import { initializeAnalytics } from 'firebase/analytics';

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

const MonthView = () => {
  const [currentDate, setCurrentDate] = useState(new Date());
  const [monthStartDate, setMonthStartDate] = useState(new Date());
  const userData = useSelector((state) => state.auth.userData);
  const allEvents = useSelector((state) => state.events.events);

  const events = allEvents?.filter((event) => {
    // Check if the author handle matches the user handle or if the user handle is in eventParticipants
    return (
      event.authorHandle === userData?.handle ||
      event.eventParticipants.split(',').includes(userData?.handle) ||
      event.eventType === 'public' ||
      userData?.isAdmin
    );
  });

  const transformedEvents = {};

  events?.forEach((event) => {
    const startDate = new Date(event.startDate).toISOString().split('T')[0];
    const endDate = new Date(event.endDate).toISOString().split('T')[0];

    // Create a new event object
    const newEvent = {
      id: event.uid,
      name: event.title,
      time: new Date(event.startDate).toLocaleTimeString('en-US', {
        hour: '2-digit',
        minute: '2-digit',
      }),
      datetime: new Date(event.startDate).toISOString(),
      href: '#',
    };

    if (!transformedEvents[startDate]) {
      transformedEvents[startDate] = [];
    }
    transformedEvents[startDate].push(newEvent);
  });

  function isToday(date) {
    const today = new Date();
    return (
      date.getUTCDate() === today.getUTCDate() &&
      date.getUTCMonth() === today.getUTCMonth() &&
      date.getUTCFullYear() === today.getUTCFullYear()
    );
  }

  function generateCalendar(startDate, originalEvents) {
    if (!originalEvents) {
      return [];
    }
    const calendar = [];
    const currentYear = startDate.getUTCFullYear();
    const currentMonth = startDate.getUTCMonth();

    const firstDate = new Date(Date.UTC(currentYear, currentMonth, 1));
    firstDate.setUTCDate(firstDate.getUTCDate() - 4);

    // Create a map to hold the events by date
    const eventsByDate = {};

    const sortedEvents = [...originalEvents].sort((a, b) => {
      const startDateA = new Date(a.startDate);
      const endDateA = new Date(a.endDate);
      const durationA = (endDateA - startDateA) / (1000 * 60); // duration in minutes

      const startDateB = new Date(b.startDate);
      const endDateB = new Date(b.endDate);
      const durationB = (endDateB - startDateB) / (1000 * 60); // duration in minutes

      return durationB - durationA; // sort in descending order
    });

    let longEventCounter = 1;
    sortedEvents?.forEach((event) => {
      const startDate = new Date(event.startDate);
      const endDate = new Date(event.endDate);

      const diffInDays = Math.floor(
        (endDate - startDate) / (1000 * 60 * 60 * 24)
      );

      if (diffInDays > 0) {
        longEventCounter++;

        // Multi-day event
        for (let i = 0; i <= diffInDays; i++) {
          const newDate = new Date(startDate);
          // console.log(newDate);

          newDate.setDate(startDate.getDate() + i);
          newDate.setHours(newDate.getHours() + 3);

          const eventDateStr = newDate.toISOString().slice(0, 10);

          const newEvent = {
            id: event.uid + '-' + i,
            authorUID: event.authorUID,
            uid: event.uid,
            authorHandle: event.authorHandle,
            title: event.title,
            description: event.description,
            eventType: event.eventType,
            eventRecurrence: event.eventRecurrence,
            eventRepeat: event.eventRepeat,
            eventParticipants: event.eventParticipants,
            eventLocation: event.eventLocation,
            startDate: event.startDate,
            endDate: event.endDate,

            isHotel: event.isHotel,
            name: event.title,
            color: event.color,
            time: newDate.toLocaleTimeString('en-US', {
              hour: '2-digit',
              minute: '2-digit',
            }),
            datetime: newDate.toISOString(),
            href: '#',
            longEvent: longEventCounter,
            isMultiDayEvent: true,
            isStart: i === 0,
            isEnd: i === diffInDays,
            startTimeString: startDate.toLocaleTimeString('en-US', {
              hour: '2-digit',
              minute: '2-digit',
            }),
            endTimeString: endDate.toLocaleTimeString('en-US', {
              hour: '2-digit',
              minute: '2-digit',
            }),
            startDateString: startDate.toLocaleDateString('en-US', {
              day: 'numeric',
              month: 'numeric',
              year: 'numeric',
            }),
            endDateString: endDate.toLocaleDateString('en-US', {
              day: 'numeric',
              month: 'numeric',
              year: 'numeric',
            }),
          };

          newEvent.overlapGroup = longEventCounter;

          if (!eventsByDate[eventDateStr]) {
            eventsByDate[eventDateStr] = [];
          }
          eventsByDate[eventDateStr].push(newEvent);
        }
      } else {
        // Single-day event
        const eventDateStr = startDate.toISOString().slice(0, 10);
        const newEvent = {
          id: event.uid,
          uid: event.authorUID,
          authorHandle: event.authorHandle,
          title: event.title,
          description: event.description,
          eventType: event.eventType,
          eventRecurrence: event.eventRecurrence,
          eventRepeat: event.eventRepeat,
          eventParticipants: event.eventParticipants,
          eventLocation: event.eventLocation,
          startDate: event.startDate,
          endDate: event.endDate,

          eventUID: event.uid,
          isHotel: event.isHotel,
          name: event.title,
          color: event.color,
          longEvent: 8,
          startTime: startDate.toLocaleTimeString('en-US', {
            hour: '2-digit',
            minute: '2-digit',
          }),
          endTimeString: endDate.toLocaleTimeString('en-US', {
            hour: '2-digit',
            minute: '2-digit',
          }),
          datetime: startDate.toISOString(),
          startDateString: startDate.toLocaleDateString('en-US', {
            day: 'numeric',
            month: 'numeric',
            year: 'numeric',
          }),
          endDateString: endDate.toLocaleDateString('en-US', {
            day: 'numeric',
            month: 'numeric',
            year: 'numeric',
          }),
          href: '#',
        };

        newEvent.overlapGroup = 0;

        if (!eventsByDate[eventDateStr]) {
          eventsByDate[eventDateStr] = [];
        }
        eventsByDate[eventDateStr].push(newEvent);
      }
    });

    for (let i = 0; i < 35; i++) {
      const dayDate = new Date(firstDate);
      dayDate.setUTCDate(firstDate.getUTCDate() + i);

      const day = {
        date: dayDate.toISOString().slice(0, 10),
        events: eventsByDate[dayDate.toISOString().slice(0, 10)] || [],
      };

      const dayMonth = dayDate.getUTCMonth();
      const dayYear = dayDate.getUTCFullYear();

      if (dayMonth === currentMonth && dayYear === currentYear) {
        if (isToday(dayDate)) {
          day.isToday = true;
        }
        day.isCurrentMonth = true;
      }

      calendar.push(day);
    }

    return calendar;
  }

  useEffect(() => {
    const updatedDays = generateCalendar(monthStartDate, events);
    setDays(updatedDays);
    setSelectedDay(null);
  }, [monthStartDate, allEvents]);

  const [days, setDays] = useState(generateCalendar(monthStartDate, events));

  const handleNextMonth = () => {
    const nextMonth = addMonths(monthStartDate, 1);
    setMonthStartDate(nextMonth);
    setDays(generateCalendar(nextMonth, events));
  };

  const handlePreviousMonth = () => {
    const prevMonth = subMonths(monthStartDate, 1);
    setMonthStartDate(prevMonth);
    setDays(generateCalendar(prevMonth, events));
  };

  const handleToday = () => {
    const today = new Date();
    setMonthStartDate(today);
    setDays(generateCalendar(today, events));
  };

  const selectedMonth = monthStartDate.getMonth();

  const [selectedDay, setSelectedDay] = useState(null);

  const currentMonthYear = new Intl.DateTimeFormat('en-US', {
    year: 'numeric',
    month: 'long',
  }).format(monthStartDate);

  const [openEditModal, setOpenEditModal] = useState(false);

  const [currentEvent, setCurrentEvent] = useState(null);

  const handleOpenEditModal = (event) => {
    setCurrentEvent(event);
    setOpenEditModal(true);
  };

  return (
    <div className='flex  justify-center items-center  mt-6 my-10'>
      <div className='flex h-full flex-col w-full max-w-7xl md:mx-10 sm:border-8 border-gray-200  dark:border-gray-700 rounded-2xl'>
        <CalendarHeader
          currentMonthYear={currentMonthYear}
          handlePrevious={handlePreviousMonth}
          handleNext={handleNextMonth}
          handleToday={handleToday}
        />

        <div className='shadow ring-1 ring-black ring-opacity-5 lg:flex lg:flex-auto lg:flex-col'>
          <div className='grid grid-cols-7 gap-px border-b  bg-gray-200 dark:bg-[#505050] text-center  font-semibold  text-gray-700 lg:flex-none divide-gray-100  border-gray-400  dark:border-gray-200 text-sm leading-6  '>
            <div className='bg-white py-3 dark:bg-[#252525] dark:text-white dark:divide-[#707070]'>
              M<span className='sr-only sm:not-sr-only'>on</span>
            </div>
            <div className='bg-white py-3 dark:bg-[#252525] dark:text-white dark:divide-[#707070] '>
              T<span className='sr-only sm:not-sr-only'>ue</span>
            </div>
            <div className='bg-white py-3 dark:bg-[#252525] dark:text-white dark:divide-[#707070]'>
              W<span className='sr-only sm:not-sr-only'>ed</span>
            </div>
            <div className='bg-white py-3 dark:bg-[#252525] dark:text-white dark:divide-[#707070]'>
              T<span className='sr-only sm:not-sr-only'>hu</span>
            </div>
            <div className='bg-white py-3 dark:bg-[#252525] dark:text-white dark:divide-[#707070]'>
              F<span className='sr-only sm:not-sr-only'>ri</span>
            </div>
            <div className='bg-white py-3 dark:bg-[#252525] dark:text-white dark:divide-[#707070]'>
              S<span className='sr-only sm:not-sr-only'>at</span>
            </div>
            <div className='bg-white py-3 dark:bg-[#252525] dark:text-white dark:divide-[#707070]'>
              S<span className='sr-only sm:not-sr-only'>un</span>
            </div>
          </div>
          <div className='flex bg-gray-200 dark:bg-gray-500 text-xs leading-6 text-gray-700 lg:flex-auto'>
            <div className='hidden w-full lg:grid lg:grid-cols-7 lg:grid-rows-5 lg:gap-px'>
              {days.map((day) => (
                <div
                  key={day.date}
                  className={classNames(
                    day.isCurrentMonth
                      ? 'bg-white '
                      : 'bg-gray-50 text-gray-500 ',
                    'relative py-1 h-32 bg-gray-50  dark:bg-[#171717] dark:text-white'
                  )}
                >
                  <time
                    dateTime={day.date}
                    className={
                      day.isToday
                        ? 'flex h-6 w-6 items-center justify-center rounded-full bg-blue-600 dark:bg-[#c2df2b] font-bold dark:text-black  text-white m-1'
                        : ' flex h-6 w-6 items-center justify-center rounded-full m-1'
                    }
                  >
                    {day.date?.split('-')?.pop()?.replace(/^0/, '')}
                  </time>
                  {day.events.length > 0 && (
                    <ol className='mt-1'>
                      {day.events.slice(0, 2).map((event, index, arr) => {
                        return (
                          <li key={event.id}>
                            <div
                              onClick={() => handleOpenEditModal(event)}
                              className={classNames(
                                event.isMultiDayEvent &&
                                  event.isStart &&
                                  'rounded-l-md ml-2 ',
                                event.isMultiDayEvent &&
                                  event.isEnd &&
                                  'rounded-r-md mr-2',
                                !event.isMultiDayEvent && 'rounded-md mx-2',
                                event.color.bgColor,
                                'group flex  px-1  mb-1 cursor-pointer'
                              )}
                            >
                              <p className='flex-auto truncate font-medium text-gray-900 group-hover:text-gray-700'>
                                {event.isMultiDayEvent && event.isStart && (
                                  <span className='mr-2'>
                                    {event.isHotel && (
                                      <FontAwesomeIcon
                                        className='mr-1 '
                                        icon={faHotel}
                                      />
                                    )}

                                    {event.name}
                                  </span>
                                )}
                                {event.isMultiDayEvent &&
                                  !event.isStart &&
                                  !event.isEnd && (
                                    <span className='mr-2'></span>
                                  )}

                                {/* {event.isMultiDayEvent && event.isEnd && (
                              <span className='mr-2'>End</span>
                            )} */}
                                {!event.isMultiDayEvent && (
                                  <span className=''>{event.name}</span>
                                )}
                              </p>
                              <time
                                dateTime={event.datetime}
                                className='ml-3 flex-none text-gray-500 xl:block'
                              >
                                {/* {console.log(event)}
                            {event.time} */}
                                {event.isMultiDayEvent && event.isStart && (
                                  <span className='mr-2'>
                                    from {event.startTimeString}
                                  </span>
                                )}

                                {event.isMultiDayEvent && event.isEnd && (
                                  <span className='mr-2'>
                                    to {event.endTimeString}
                                  </span>
                                )}
                                {!event.isMultiDayEvent && (
                                  <span className=''>
                                    {event.startTimeString}
                                  </span>
                                )}
                              </time>
                            </div>
                          </li>
                        );
                      })}
                      {day.events.length > 2 && (
                        <li className='ml-3 text-gray-500'>
                          + {day.events.length - 2} more
                        </li>
                      )}
                    </ol>
                  )}
                </div>
              ))}
            </div>

            <div className='isolate grid w-full grid-cols-7 grid-rows-5 gap-px lg:hidden'>
              {days.map((day) => (
                <button
                  key={day.date}
                  className={classNames(
                    day.isCurrentMonth
                      ? 'bg-white dark:bg-[#212121]'
                      : 'bg-gray-50 dark:bg-[#191919]',
                    (day.isSelected || day.isToday) && 'font-semibold ',
                    day.isSelected && 'text-white',
                    !day.isSelected &&
                      day.isToday &&
                      'text-blue-600 dark:text-[#c2df2b]',
                    !day.isSelected &&
                      day.isCurrentMonth &&
                      !day.isToday &&
                      'text-gray-900 dark:text-gray-300',
                    !day.isSelected &&
                      !day.isCurrentMonth &&
                      !day.isToday &&
                      'text-gray-500 dark:text-gray-400',
                    selectedDay?.date === day.date &&
                      ' shadow-inner shadow-black',

                    'flex h-14 flex-col py-2 px-3 hover:bg-gray-100 focus:z-10'
                  )}
                  onClick={() => setSelectedDay(day)}
                >
                  {/* {console.log(day.isSelected)} */}
                  <time
                    dateTime={day.date}
                    className={classNames(
                      day.isSelected &&
                        'flex h-6 w-6 items-center justify-center rounded-full',
                      day.isSelected && day.isToday && 'bg-blue-600',
                      day.isSelected && !day.isToday && 'bg-gray-900',
                      'ml-auto'
                    )}
                  >
                    {day.date?.split('-')?.pop()?.replace(/^0/, '')}
                  </time>
                  <span className='sr-only'>{day.events.length} events</span>
                  {day.events.length > 0 && (
                    <span className='-mx-0.5 mt-auto flex flex-wrap-reverse'>
                      {day.events.map((event) => (
                        <span
                          key={event.id}
                          // className='mx-0.5 mb-1 h-1.5 w-1.5 rounded-full bg-gray-400'
                          className={classNames(
                            event.isMultiDayEvent
                              ? `mx-0.5 mb-1 h-1.5 w-1.5 rounded-full ${event.color.bgColor}`
                              : `mx-0.5 mb-1 h-1.5 w-1.5 rounded-full bg-gray-400`
                          )}
                        />
                      ))}
                    </span>
                  )}
                </button>
              ))}
            </div>
          </div>
        </div>
        {selectedDay?.events?.length && selectedDay.events.length > 0 && (
          <div className='py-10 px-4 sm:px-6'>
            <ol className='divide-y divide-gray-100 dark:divide-gray-700 overflow-hidden rounded-lg bg-white dark:bg-[#252525]  text-sm shadow ring-1 ring-black ring-opacity-5 dark:border dark:border-gray-700'>
              {selectedDay?.events.map((event) => (
                <li
                  key={event.id}
                  className='group flex p-4 pr-6 focus-within:bg-gray-50 hover:bg-gray-50 dark:hover:bg-[#303030]'
                >
                  <div className='flex-auto'>
                    <p className='font-semibold text-gray-900 dark:text-gray-100'>
                      {event.name}
                    </p>
                    <time
                      dateTime={event.datetime}
                      className='mt-2 flex items-center text-gray-700 dark:text-gray-400'
                    >
                      <ClockIcon
                        className='mr-2 h-5 w-5 text-gray-400 dark:text-gray-200'
                        aria-hidden='true'
                      />
                      {new Date(event.startDate).toLocaleTimeString('en-US', {
                        hour: '2-digit',
                        minute: '2-digit',
                      })}{' '}
                    </time>
                  </div>
                  <button
                    onClick={() => handleOpenEditModal(event)}
                    className='ml-6 flex-none self-center rounded-md border border-gray-300 py-2 px-3 font-semibold dark:text-gray-700 opacity-0 shadow-sm hover:bg-gray-50 focus:opacity-100 group-hover:opacity-100 bg-blue-600 text-white dark:bg-[#c2df2b]'
                  >
                    {event.author === userData?.handle ? 'Edit' : 'View'}
                    <span className='sr-only'>, {event.name}</span>
                  </button>
                </li>
              ))}
            </ol>
          </div>
        )}
        {currentEvent && (
          <EditEvent
            open={openEditModal}
            setOpen={setOpenEditModal}
            initialEvent={currentEvent}
          />
        )}
      </div>
    </div>
  );
};

export default MonthView;
