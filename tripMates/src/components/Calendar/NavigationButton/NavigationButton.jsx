import React from 'react';
import { ChevronLeftIcon, ChevronRightIcon } from '@heroicons/react/solid';

function NavigationButton({ onClick, children }) {
  return (
    <button type='button' onClick={onClick}>
      {children}
    </button>
  );
}

export function LeftNavigationButton({ onClick }) {
  return (
    <NavigationButton onClick={onClick}>
      <div className='flex items-center justify-center rounded-l-md border border-r-0 border-gray-300 bg-white py-2 pl-3 pr-4 text-gray-400 hover:text-gray-500 focus:relative md:w-9 md:px-2 md:hover:bg-gray-50'>
        <span className='sr-only'>Previous month</span>
        <ChevronLeftIcon className='h-5 w-5' aria-hidden='true' />
      </div>
    </NavigationButton>
  );
}

export function TodayButton({ onClick }) {
  return (
    <button
      type='button'
      className='hidden border-t border-b border-gray-300 bg-white px-3.5 text-sm font-medium text-gray-700 hover:bg-gray-50 hover:text-gray-900 focus:relative md:block'
      onClick={onClick}
    >
      Today
    </button>
  );
}

export function RightNavigationButton({ onClick }) {
  return (
    <NavigationButton onClick={onClick}>
      <div className='flex items-center justify-center rounded-r-md border border-l-0 border-gray-300 bg-white py-2 pl-4 pr-3 text-gray-400 hover:text-gray-500 focus:relative md:w-9 md:px-2 md:hover:bg-gray-50'>
        <span className='sr-only'>Next month</span>
        <ChevronRightIcon className='h-5 w-5' aria-hidden='true' />
      </div>
    </NavigationButton>
  );
}
