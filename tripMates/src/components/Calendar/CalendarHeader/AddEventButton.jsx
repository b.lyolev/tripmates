import React from 'react';
import { useState } from 'react';
import AddEvent from '../AddEvent/AddEvent';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMoon, faPlus, faSun } from '@fortawesome/free-solid-svg-icons';

function AddEventButton() {
  const [openAddEventModal, setOpenAddEventModal] = useState(false);

  const handleOpenAddEventModal = () => {
    setOpenAddEventModal(true);
  };

  return (
    <>
      <button
        onClick={handleOpenAddEventModal}
        type='button'
        className='max-sm:hidden ml-6 rounded-xl border border-transparent bg-blue-600 dark:bg-[#c2df2b] py-2 px-4 text-sm font-medium text-white dark:text-black shadow-sm hover:bg-blue-700  dark:hover:bg-[#ddee87] focus:outline-none focus:ring-2 focus:ring-blue-500 dark:focus:ring-[#9dad48] focus:ring-offset-2'
      >
        Add event
      </button>
      <button
        className='md:hidden flex items-center justify-center dark:bg-[#c2df2b] rounded-full h-8 w-8  bg-blue-500 text-gray-400    focus:relative  dark:md:hover:bg-[#323232]  dark:text-[#e0e0e0]'
        onClick={handleOpenAddEventModal}
      >
        <FontAwesomeIcon
          icon={faPlus}
          className=' dark:text-black text-white'
        />
      </button>

      <AddEvent open={openAddEventModal} setOpen={setOpenAddEventModal} />
    </>
  );
}

export default AddEventButton;
