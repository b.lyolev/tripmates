import React from 'react';
import { Link } from 'react-router-dom';
import { Menu, Transition } from '@headlessui/react';
import { ChevronDownIcon } from '@heroicons/react/solid';
import { Fragment } from 'react';
import { classNames } from '../../../utils/helpers';
import { useDispatch, useSelector } from 'react-redux';
import { setSelectedView } from '../../../reducers/viewActions';

const viewOptions = [
  { label: 'Day view', value: 'Day View', to: '/calendar/day-view' },
  { label: 'Week view', value: 'Week View', to: '/calendar/week-view' },
  { label: 'Month view', value: 'Month View', to: '/calendar/month-view' },
];

export default function SelectView() {
  const selectedView = useSelector((state) => state.currentView.selectedView);
  const dispatch = useDispatch();

  const handleSelectView = (view) => {
    dispatch(setSelectedView(view)); // Dispatch the action to update the selected view
  };

  return (
    <Menu as='div' className='relative'>
      <Menu.Button
        type='button'
        className='flex items-center rounded-xl border border-gray-300 bg-white dark:bg-[#171717] dark:text-[#e0e0e0] py-2 pl-3 pr-2 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50'
      >
        {selectedView}
        <ChevronDownIcon
          className='ml-2 h-5 w-5 text-gray-400 dark:text-[#e0e0e0]'
          aria-hidden='true'
        />
      </Menu.Button>

      <Transition
        as={Fragment}
        enter='transition ease-out duration-100'
        enterFrom='transform opacity-0 scale-95'
        enterTo='transform opacity-100 scale-100'
        leave='transition ease-in duration-75'
        leaveFrom='transform opacity-100 scale-100'
        leaveTo='transform opacity-0 scale-95'
      >
        <Menu.Items className='absolute right-0 mt-3 w-36 origin-top-right overflow-hidden rounded-xl bg-white shadow-xl ring-1 ring-black ring-opacity-5  focus:outline-none dark:bg-[#303030] '>
          <div className=''>
            {viewOptions.map((option) => (
              <Menu.Item key={option.value}>
                {({ active }) => (
                  <Link
                    to={option.to}
                    onClick={() => handleSelectView(option.value)}
                    className={classNames(
                      active
                        ? 'bg-gray-100 text-gray-900 dark:text-white dark:bg-[#404040] shadow-inner'
                        : 'text-gray-700 dark:text-white',
                      'block px-4 py-2 text-sm '
                    )}
                  >
                    {option.label}
                  </Link>
                )}
              </Menu.Item>
            ))}
          </div>
        </Menu.Items>
      </Transition>
    </Menu>
  );
}
