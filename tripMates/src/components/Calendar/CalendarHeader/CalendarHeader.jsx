import React from 'react';
import { classNames } from '../../../utils/helpers';
import { Fragment } from 'react';
import { Menu, Transition } from '@headlessui/react';
import { ChevronDownIcon } from '@heroicons/react/solid';
import { DotsHorizontalIcon } from '@heroicons/react/outline';
import { ChevronLeftIcon, ChevronRightIcon } from '@heroicons/react/solid';
import { Link } from 'react-router-dom';
import AddEventButton from './AddEventButton';
import CurrentMonth from './CurrentMonth';
import NavigationButtons from './NavigationButtons';
import { Select } from '@chakra-ui/react';
import SelectView from './SelectView';

export default function CalendarHeader({
  currentMonthYear,
  handlePrevious,
  handleToday,
  handleNext,
}) {
  return (
    <div>
      <header className='relative z-40 flex flex-none items-center justify-between border-b border-gray-200 py-4 px-6 dark:bg-[#171717] dark:border-[#afafaf] sm:rounded-t-lg'>
        <CurrentMonth currentMonthYear={currentMonthYear} />
        <div className='flex items-center '>
          <div className='sm:hidden mr-4 '>
            <AddEventButton></AddEventButton>
          </div>
          <div className='flex items-center sm:rounded-xl shadow-sm md:items-stretch '>
            <NavigationButtons
              handlePreviousWeek={handlePrevious}
              handleThisWeek={handleToday}
              handleNextWeek={handleNext}
            />
          </div>

          <div className='hidden md:ml-4 md:flex md:items-center'>
            <SelectView />
            <div className='ml-6 h-6 w-px bg-gray-300' />

            <AddEventButton />
          </div>
          <Menu as='div' className='relative ml-6 md:hidden'>
            <Menu.Button className='-mx-2 flex items-center rounded-full border border-transparent p-2 text-gray-400 hover:text-gray-500'>
              <span className='sr-only'>Open menu</span>
              <DotsHorizontalIcon className='h-5 w-5' aria-hidden='true' />
            </Menu.Button>

            <Transition
              as={Fragment}
              enter='transition ease-out duration-100'
              enterFrom='transform opacity-0 scale-95'
              enterTo='transform opacity-100 scale-100'
              leave='transition ease-in duration-75'
              leaveFrom='transform opacity-100 scale-100'
              leaveTo='transform opacity-0 scale-95'
            >
              <Menu.Items className='absolute right-0 mt-3 w-36 origin-top-right divide-y divide-gray-100 overflow-hidden rounded-xl bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none dark:bg-[#303030] '>
                <div>
                  <Menu.Item>
                    {({ active }) => (
                      <a
                        onClick={handleToday}
                        href='#'
                        className={classNames(
                          active
                            ? 'bg-gray-100 text-gray-900 dark:text-white  dark:bg-[#404040] shadow-inner'
                            : 'text-gray-700 dark:text-white ',
                          'block px-4 py-2 text-sm'
                        )}
                      >
                        Go to today
                      </a>
                    )}
                  </Menu.Item>
                </div>
                <div>
                  <Menu.Item>
                    {({ active }) => (
                      <Link
                        to='/calendar/day-view'
                        className={classNames(
                          active
                            ? 'bg-gray-100 text-gray-900 dark:text-white  dark:bg-[#404040] shadow-inner'
                            : 'text-gray-700 dark:text-white ',
                          'block px-4 py-2 text-sm'
                        )}
                      >
                        Day view
                      </Link>
                    )}
                  </Menu.Item>
                  <Menu.Item>
                    {({ active }) => (
                      <Link
                        to='/calendar/week-view'
                        className={classNames(
                          active
                            ? 'bg-gray-100 text-gray-900 dark:text-white  dark:bg-[#404040] shadow-inner'
                            : 'text-gray-700 dark:text-white ',
                          'block px-4 py-2 text-sm'
                        )}
                      >
                        Week view
                      </Link>
                    )}
                  </Menu.Item>
                  <Menu.Item>
                    {({ active }) => (
                      <Link
                        to='/calendar/month-view'
                        className={classNames(
                          active
                            ? 'bg-gray-100 text-gray-900 dark:text-white  dark:bg-[#404040] shadow-inner'
                            : 'text-gray-700 dark:text-white ',
                          'block px-4 py-2 text-sm'
                        )}
                      >
                        Month view
                      </Link>
                    )}
                  </Menu.Item>
                </div>
              </Menu.Items>
            </Transition>
          </Menu>
        </div>
      </header>
    </div>
  );
}
