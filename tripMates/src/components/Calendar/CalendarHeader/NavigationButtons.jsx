import React from 'react';
import { ChevronLeftIcon, ChevronRightIcon } from '@heroicons/react/solid';

export default function NavigationButtons({
  handlePreviousWeek,
  handleThisWeek,
  handleNextWeek,
}) {
  return (
    <>
      <button
        type='button'
        className='flex items-center justify-center rounded-l-xl border border-r-0 border-gray-300 bg-white py-2 pl-3 pr-4 text-gray-400 hover:text-gray-500 focus:relative md:w-9 md:px-2 md:hover:bg-gray-50 dark:bg-[#171717]  dark:text-[#e0e0e0] dark:md:hover:bg-[#323232] '
        onClick={handlePreviousWeek}
      >
        <span className='sr-only'>Previous month</span>
        <ChevronLeftIcon className='h-5 w-5' aria-hidden='true' />
      </button>
      <button
        type='button'
        className='hidden border-t border-b border-gray-300 bg-white dark:bg-[#171717]  dark:text-[#e0e0e0] px-3.5 text-sm font-medium text-gray-700 hover:bg-gray-50 hover:text-gray-900  md:block'
        onClick={handleThisWeek}
      >
        Today
      </button>
      <span className='relative -mx-px h-5 w-px bg-gray-300 md:hidden' />
      <button
        type='button'
        className='flex items-center justify-center rounded-r-xl border border-l-0 border-gray-300 bg-white py-2 pl-4 pr-3 text-gray-400 hover:text-gray-500  md:w-9 md:px-2 md:hover:bg-gray-50 dark:md:hover:bg-[#323232] dark:bg-[#171717]  dark:text-[#e0e0e0]'
        onClick={handleNextWeek}
      >
        <span className='sr-only'>Next month</span>
        <ChevronRightIcon className='h-5 w-5' aria-hidden='true' />
      </button>
    </>
  );
}
