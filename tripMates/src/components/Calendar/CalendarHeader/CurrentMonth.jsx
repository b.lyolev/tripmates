import React from 'react';

export default function CurrentMonth({ currentMonthYear }) {
  return (
    <h1 className='text-lg font-semibold text-gray-900 dark:text-[#ffffff] '>
      {currentMonthYear}

      <span className='sm:hidden'></span>
    </h1>
  );
}
