// import React, { useState } from 'react';
// import { ChevronDownIcon } from '@heroicons/react/solid';
// import { Menu, Transition } from '@headlessui/react';
// import { classNames } from '../../../utils/helpers';
// import { Fragment } from 'react';

// function ViewOptions() {
//   const viewOptions = [
//     { label: 'Week view', value: 'week' },
//     { label: 'Month view', value: 'month' },
//     { label: 'Year view', value: 'year' },
//   ];

//   const [selectedView, setSelectedView] = useState('Week view'); // Initialize the selected view as 'week'

//   const handleViewOptionClick = (value) => {
//     setSelectedView(value); // Update the selected view when an option is clicked
//   };

//   return (
//     <Menu as='div' className='relative'>
//       <Menu.Button
//         type='button'
//         className='flex items-center rounded-md border border-gray-300 bg-white py-2 pl-3 pr-2 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50'
//       >
//         {viewOptions.find((option) => option.value === selectedView).label}
//         <ChevronDownIcon
//           className='ml-2 h-5 w-5 text-gray-400'
//           aria-hidden='true'
//         />
//       </Menu.Button>

//       <Transition
//         as={Fragment}
//         enter='transition ease-out duration-100'
//         enterFrom='transform opacity-0 scale-95'
//         enterTo='transform opacity-100 scale-100'
//         leave='transition ease-in duration-75'
//         leaveFrom='transform opacity-100 scale-100'
//         leaveTo='transform opacity-0 scale-95'
//         className='absolute z-10'
//       >
//         <Menu.Items className='absolute right-0 mt-3 w-36 origin-top-right overflow-hidden rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none'>
//           <div className='py-1'>
//             {viewOptions.map((option) => (
//               <Menu.Item key={option.value}>
//                 {({ active }) => (
//                   <a
//                     href='#'
//                     className={classNames(
//                       active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
//                       'block px-4 py-2 text-sm',
//                       option.value === selectedView ? 'font-semibold' : '' // Apply font-semibold to the selected option
//                     )}
//                     onClick={() => handleViewOptionClick(option.value)}
//                   >
//                     {option.label}
//                   </a>
//                 )}
//               </Menu.Item>
//             ))}
//           </div>
//         </Menu.Items>
//       </Transition>
//     </Menu>
//   );
// }

// export default ViewOptions;
