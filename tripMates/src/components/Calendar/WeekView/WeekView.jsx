/* This example requires Tailwind CSS v2.0+ */
import React, { Fragment, useEffect, useRef, useState } from 'react';
import {
  ChevronDownIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
  DotsHorizontalIcon,
} from '@heroicons/react/solid';
import { Menu, Transition } from '@headlessui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faEye,
  faHotel,
  faMoon,
  faSun,
} from '@fortawesome/free-solid-svg-icons';
import AddEventButton from '../CalendarHeader/AddEventButton';
import { Link } from 'react-router-dom';
import LoadingSpinner from '../../LoadingSpinner/LoadingSpinner';
import { useSelector } from 'react-redux';
import { oc } from 'date-fns/locale';
import { Avatar, AvatarGroup } from '@chakra-ui/react';
import AvatarsGroup from '../../AvatarsGroup/AvatarsGroup';
import CalendarHeader from '../CalendarHeader/CalendarHeader';
import { fetchUsersFromUIDs } from '../../../services/contacts.service';
import EditEvent from '../../EditEvent/EditEvent';

const colVariants = {
  1: 'sm:col-start-1',
  2: 'sm:col-start-2',
  3: 'sm:col-start-3',
  4: 'sm:col-start-4',
  5: 'sm:col-start-5',
  6: 'sm:col-start-6',
};

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export default function WeekView() {
  const [currentWeek, setCurrentWeek] = useState(new Date());
  const [groupedEvents, setGroupedEvents] = useState([]);
  const [multiDayEvents, setMultiDayEvents] = useState([]);
  const [clickedDate, setClickedDate] = useState(new Date());
  const [filteredEvents, setFilteredEvents] = useState([]);
  const [isMobile, setIsMobile] = useState(window.innerWidth <= 640);
  const [currentTime, setCurrentTime] = useState(new Date());

  const allEvents = useSelector((state) => state.events.events);
  const userData = useSelector((state) => state.auth.userData);

  const events = allEvents?.filter((event) => {
    return (
      event.authorHandle === userData?.handle ||
      event.eventParticipants.split(',').includes(userData?.handle) ||
      event.eventType === 'public' ||
      userData?.isAdmin
    );
  });

  useEffect(() => {
    const intervalId = setInterval(() => {
      setCurrentTime(new Date());
    }, 20000); // Update every second
    return () => {
      clearInterval(intervalId);
    };
  }, []);

  const hours = currentTime.getHours().toString().padStart(2, '0');
  const minutes = currentTime.getMinutes().toString().padStart(2, '0');

  const formattedTime = `${hours}:${minutes}`;
  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth <= 640);
    };
    // setClickedDate();

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  const [currentMonthYear, setCurrentMonthYear] = useState(
    new Date().toLocaleString('en-US', {
      month: 'long',
      year: 'numeric',
    })
  );

  const container = useRef(null);
  const containerNav = useRef(null);
  const containerOffset = useRef(null);
  const longEventsRef = useRef(null);
  const [longEventsHeight, setLongEventsHeight] = useState();

  useEffect(() => {
    if (longEventsRef.current) {
      const newHeight = longEventsRef.current.clientHeight;
      if (newHeight !== longEventsHeight) {
        setLongEventsHeight(newHeight);
      }
    }
  }, [longEventsHeight, multiDayEvents]);

  // useEffect(() => {
  //   // Set the container scroll position based on the current time.
  //   const currentMinute = new Date().getHours() * 49;
  //   container.current.scrollTop =
  //     ((container.current.scrollHeight -
  //       containerNav.current.offsetHeight -
  //       containerOffset.current.offsetHeight) *
  //       currentMinute) /
  //     1440;
  // }, []);

  function calculateGridColumn(eventDate) {
    const date = new Date(eventDate);
    return date.getDay() + 1;
  }

  const handlePreviousWeek = () => {
    setCurrentWeek((prevWeek) => {
      const newWeek = new Date(prevWeek);
      newWeek.setDate(prevWeek.getDate() - 7);

      const monthYear = newWeek.toLocaleString('en-US', {
        month: 'long',
        year: 'numeric',
      });
      setCurrentMonthYear(monthYear);

      return newWeek;
    });
  };

  const handleNextWeek = () => {
    setCurrentWeek((prevWeek) => {
      const newWeek = new Date(prevWeek);
      newWeek.setDate(prevWeek.getDate() + 7);

      const monthYear = newWeek.toLocaleString('en-US', {
        month: 'long',
        year: 'numeric',
      });
      setCurrentMonthYear(monthYear);

      return newWeek;
    });
  };

  const handleThisWeek = () => {
    setCurrentWeek(new Date());

    const monthYear = new Date().toLocaleString('en-US', {
      month: 'long',
      year: 'numeric',
    });
    setCurrentMonthYear(monthYear);
  };

  const startOfWeek = new Date(currentWeek);
  startOfWeek.setDate(currentWeek.getDate() - currentWeek.getDay());

  const endOfWeek = new Date(startOfWeek);
  endOfWeek.setDate(endOfWeek.getDate() + 6);

  const isWithinCurrentWeek =
    currentTime >= startOfWeek && currentTime <= endOfWeek;

  const dates = [];

  for (let i = 0; i < 7; i++) {
    const date = new Date(startOfWeek);
    date.setDate(startOfWeek.getDate() + i);
    dates.push(date);
  }

  function isToday(date) {
    const today = new Date();
    return (
      date.getDate() === today.getDate() &&
      date.getMonth() === today.getMonth() &&
      date.getFullYear() === today.getFullYear()
    );
  }

  function calculateEventSpan(startTime, endTime) {
    const start = new Date(startTime);
    const end = new Date(endTime);
    const diffInMinutes = (end - start) / (1000 * 60);

    const rows = Math.floor(diffInMinutes / 5);
    return rows;
  }

  // Format event time based on start and end times
  // function formatEventTime(startTime, endTime) {
  //   const start = new Date(startTime);
  //   const end = new Date(endTime);
  //   const startTimeFormatted = start.toLocaleTimeString('en-US', {
  //     hour: 'numeric',
  //     minute: 'numeric',
  //   });

  //   const endTimeFormatted = end.toLocaleTimeString('en-US', {
  //     hour: 'numeric',
  //     minute: 'numeric',
  //   });

  //   return `${startTimeFormatted} - ${endTimeFormatted}`;
  // }

  const groupEvents = (events) => {
    const updatedGroupedEvents = [];
    // console.log(events);
    events?.forEach((event) => {
      let addedToGroup = false;

      for (const group of updatedGroupedEvents) {
        const firstEventInGroup = group[0];

        const isOverlapping =
          event.startDate < firstEventInGroup.endDate &&
          event.endDate > firstEventInGroup.startDate;

        if (isOverlapping) {
          group.push(event);

          addedToGroup = true;
          break;
        }
      }

      if (!addedToGroup) {
        updatedGroupedEvents.push([event]);
      }
    });
    // console.log(updatedGroupedEvents);
    return updatedGroupedEvents;
  };

  useEffect(() => {
    // console.log('Updating multi-day events...');

    const startOfWeek = new Date(currentWeek);
    startOfWeek.setDate(currentWeek.getDate() - currentWeek.getDay());
    startOfWeek.setHours(0, 0, 0, 0);

    const endOfWeek = new Date(startOfWeek);
    endOfWeek.setDate(endOfWeek.getDate() + 6);

    // Filter events to include only those that intersect with the current week
    const filteredEvents = events?.filter((event) => {
      const eventStartWeek = new Date(event.startDate);

      eventStartWeek.setDate(
        eventStartWeek.getDate() - eventStartWeek.getDay()
      );

      const eventEndWeek = new Date(event.endDate);
      eventEndWeek.setDate(eventEndWeek.getDate() - eventEndWeek.getDay());

      return (
        (eventStartWeek >= startOfWeek && eventStartWeek <= endOfWeek) ||
        (eventEndWeek >= startOfWeek && eventEndWeek <= endOfWeek) ||
        (eventStartWeek <= startOfWeek && eventEndWeek >= startOfWeek)
      );
    });

    const newMultiDayEvents = [];

    filteredEvents?.forEach((event) => {
      // console.log(event.start, event.end);
      const eventStartWeek = new Date(event.startDate);
      eventStartWeek.setDate(
        eventStartWeek.getDate() - eventStartWeek.getDay()
      );

      const eventEndWeek = new Date(event.endDate);
      eventEndWeek.setDate(eventEndWeek.getDate() - eventEndWeek.getDay());

      if (eventStartWeek >= startOfWeek && eventStartWeek <= endOfWeek) {
        if (eventEndWeek > endOfWeek) {
          // The event's end is outside the current week, split it into multiple parts
          let currentStartDate = event.startDate;
          let currentEndDate = new Date(endOfWeek);

          currentStartDate = new Date(currentEndDate);
          currentStartDate.setDate(currentStartDate.getDate() + 1); // Move to the next day

          currentEndDate.setDate(currentEndDate.getDate() + 1);
          currentEndDate.setHours(23, 59, 59, 999);

          newMultiDayEvents.push(event);
          currentStartDate.setDate(currentEndDate.getDate() + 1); // Move to the next day
        } else if (
          eventStartWeek >= startOfWeek &&
          eventStartWeek <= endOfWeek &&
          getMultiDayEventDuration(event) >= 1
        ) {
          newMultiDayEvents.push(event);
        }
      } else if (eventStartWeek < startOfWeek && eventEndWeek >= startOfWeek) {
        // Event starts before the current week but ends within it
        const newEvent = {
          ...event,
          startDate: new Date(startOfWeek),
          text: 'part',
        };
        newMultiDayEvents.push(newEvent);
      }
    });

    newMultiDayEvents.sort((a, b) => {
      const startComparison = a.start - b.start;
      if (startComparison === 0) {
        const endComparison = a.end - b.end;
        if (endComparison === 0) {
          return a.title.localeCompare(b.title);
        }
        return endComparison;
      }
      return startComparison;
    });

    setMultiDayEvents(newMultiDayEvents);
  }, [allEvents, currentWeek]);

  useEffect(() => {
    // console.log('Updating grouped events...');

    const startOfWeek = new Date(currentWeek);
    startOfWeek.setDate(currentWeek.getDate() - currentWeek.getDay());
    startOfWeek.setHours(0, 0, 0, 0);

    const endOfWeek = new Date(startOfWeek);
    endOfWeek.setDate(endOfWeek.getDate() + 7);

    const updatedGroupedEvents = groupEvents(
      events?.filter((event) => {
        const eventDurationInDays = getMultiDayEventDuration(event);

        const start = new Date(event.startDate);
        const end = new Date(event.endDate);
        const isSameDay = start.toDateString() === end.toDateString();

        if (eventDurationInDays > 0 && !isSameDay) {
          return false; // Exclude this event from grouped events
        }

        return start >= startOfWeek && end <= endOfWeek;
      })
    );
    // console.log(updatedGroupedEvents);
    setGroupedEvents(updatedGroupedEvents);
  }, [allEvents, currentWeek, multiDayEvents]);

  function getMultiDayEventDuration(event) {
    const start = new Date(event.startDate); // Convert start date string to Date object
    const end = new Date(event.endDate); // Convert end date string to Date object

    const startDate = new Date(
      start.getFullYear(),
      start.getMonth(),
      start.getDate(),
      0,
      0,
      0
    );
    const endDate = new Date(
      end.getFullYear(),
      end.getMonth(),
      end.getDate(),
      0,
      0,
      0
    );

    if (startDate.getTime() !== endDate.getTime()) {
      const timeDifference = Math.abs(endDate.getTime() - startDate.getTime());
      const daysDifference = Math.ceil(timeDifference / (1000 * 60 * 60 * 24));
      return daysDifference;
    }

    return 0; // Not a multi-day event
  }

  function calculateGridRow(eventTime) {
    const date = new Date(eventTime);
    const eventHour = date.getHours();
    const eventMinute = date.getMinutes();
    const totalMinutes = eventHour * 60 + eventMinute;
    const relativePosition = totalMinutes / 1440; // 1440 minutes in a day
    const totalGridRows = 288; // Total number of grid rows
    return Math.floor(relativePosition * totalGridRows + 2);
  }
  function filterEventsByDate(date, events) {
    const filteredEvents = events?.filter((event) => {
      const eventStartDate = new Date(event.startDate);
      const eventEndDate = new Date(event.endDate);

      // Set the time portion of eventStartDate and eventEndDate to 00:00:00
      eventStartDate.setHours(0, 0, 0, 0);
      eventEndDate.setHours(0, 0, 0, 0);

      // Calculate the duration of the event in milliseconds
      const eventDuration = eventEndDate - eventStartDate;

      // Check if the event starts on the given date, ends on or after it,
      // and has a duration less than or equal to 24 hours (86400000 milliseconds)
      return (
        eventStartDate <= date &&
        date <= eventEndDate &&
        eventDuration <= 86400000
      );
    });

    // console.log(filteredEvents);
    return filteredEvents;
  }

  const handleDateClick = (clickedDate) => {
    const startOfDay = new Date(clickedDate);
    startOfDay.setHours(0, 0, 0, 0);

    // console.log(startOfDay);

    const eventsForClickedDate = filterEventsByDate(startOfDay, events);

    const groupedEventsForClickedDate = groupEvents(eventsForClickedDate);

    // setSelectedDate(startOfDay);
    setClickedDate(clickedDate || new Date());
    setFilteredEvents(groupedEventsForClickedDate);
    // console.log(groupedEventsForClickedDate);
  };

  useEffect(() => {
    // Calculate the start of the clicked day
    const startOfDay = new Date(clickedDate);
    startOfDay.setHours(0, 0, 0, 0);

    // Filter events for the clicked day
    const eventsForClickedDate = filterEventsByDate(startOfDay, events);
    const groupedEventsForClickedDate = groupEvents(eventsForClickedDate);

    setFilteredEvents(groupedEventsForClickedDate);
  }, [clickedDate, allEvents]);

  //startDate.toLocaleTimeString('en-US', {
  //   hour: '2-digit',
  //   minute: '2-digit',
  // }),

  const convertTime = (time) => {
    const date = new Date(time);
    return date.toLocaleTimeString('en-GB', {
      hour: '2-digit',
      minute: '2-digit',
      hour12: false,
    });
  };

  const [openEditModal, setOpenEditModal] = useState(false);

  const [currentEvent, setCurrentEvent] = useState(null);

  const handleOpenEditModal = (event) => {
    setCurrentEvent(event);
    setOpenEditModal(true);
  };
  return (
    <>
      <div className='flex  justify-center items-center mb-10'>
        <div className='flex h-[800px] flex-col w-full max-w-7xl md:mx-10 sm:border-8   border-gray-200  dark:border-gray-700 rounded-2xl'>
          <CalendarHeader
            currentMonthYear={currentMonthYear}
            handlePrevious={handlePreviousWeek}
            handleToday={handleThisWeek}
            handleNext={handleNextWeek}
          />
          <div
            ref={container}
            className='flex flex-auto flex-col  bg-gray-50  dark:bg-[#171717] overflow-auto'
          >
            <div
              style={{ width: '165%' }}
              className='flex max-w-full flex-none flex-col sm:max-w-none md:max-w-full '
            >
              <div
                ref={containerNav}
                className='sticky top-0 z-30 flex-none bg-white dark:bg-[#252525] shadow dark:shadow-gray-500 dark:shadow-xs ring-1 ring-black ring-opacity-5 dark:border-b'
              >
                <div className='ml-14 grid grid-cols-7 text-sm leading-6 text-gray-500 sm:hidden dark:text-white '>
                  {dates.map((date, index) => (
                    <button
                      key={index}
                      type='button'
                      className={`flex flex-col items-center pt-2 pb-3  
                    
                   ${
                     clickedDate && date.getTime() === clickedDate.getTime()
                       ? 'bg-blue-100 dark:bg-[#c2df2b] rounded-lg text-black font-medium'
                       : ''
                   }`}
                      onClick={() => handleDateClick(date)}
                    >
                      {date.toLocaleDateString('en-US', { weekday: 'short' })}{' '}
                      <span
                        className={`mt-1 flex h-8 w-8 items-center justify-center font-semibold  ${
                          isToday(date)
                            ? 'rounded-full bg-blue-600 dark:bg-[#c4db50] text-white dark:text-black shadow-inner shadow-gray-500'
                            : 'text-gray-900'
                        }
                    ${
                      clickedDate && date.getTime() === clickedDate.getTime()
                        ? 'dark:text-black font-semibold'
                        : 'dark:text-white'
                    }
                    `}
                      >
                        {date.getDate()}
                      </span>
                    </button>
                  ))}
                </div>

                <div className='-mr-px hidden grid-cols-7 divide-x w-full divide-gray-100  border-gray-100 text-sm leading-6 text-gray-500 sm:grid dark:bg-[#252525] dark:text-white dark:divide-[#707070] '>
                  <div className='col-end-1 w-14 ' />

                  {dates.map((date, index) => (
                    <div
                      key={index}
                      className={`flex items-center justify-center py-3  ${
                        isToday(date)
                          ? 'bg-blue-500 dark:bg-[#c2df2b] dark:text-black  text-white font-bold'
                          : '' // Add a special style for today's date
                      }`}
                    >
                      <span>
                        {date.toLocaleDateString('en-US', { weekday: 'short' })}{' '}
                        <span
                          className={`items-center justify-center font-semibold `}
                        >
                          {date.getDate()}
                        </span>
                      </span>
                    </div>
                  ))}
                </div>
              </div>
              <div className='eventContainer flex flex-auto'>
                <div className='sticky left-0 z-10 w-14 flex-none bg-white ring-1 ring-gray-100  dark:bg-[#252525] ' />
                <div className='grid flex-auto grid-cols-1 grid-rows-1'>
                  <div
                    className='col-start-1 col-end-2 row-start-1 grid divide-y divide-gray-100 dark:divide-[#707070]'
                    style={{
                      gridTemplateRows: 'repeat(48, minmax(3.5rem, 1fr))',
                    }}
                  >
                    <div
                      ref={containerOffset}
                      className={`row-end-1 sticky h-auto pt-[5px]`}
                    >
                      <div ref={longEventsRef} className='grid grid-cols-7 '>
                        {/* {console.log(multiDayEvents)} */}
                        {multiDayEvents
                          // Create a shallow copy of the array before sorting
                          .slice()
                          .sort((a, b) => {
                            return (
                              getMultiDayEventDuration(b) -
                              getMultiDayEventDuration(a)
                            );
                          }) // Sort by event duration
                          .map((event, index) => {
                            return (
                              <li
                                key={index}
                                className={`relative mt-px flex z-10 h-6 top-8 pb-[5px] mx-1 text-black font-medium overflow-hidden `}
                                style={{
                                  gridColumn: `${calculateGridColumn(
                                    event.startDate
                                  )} / span ${
                                    getMultiDayEventDuration(event) + 1
                                  }`,
                                  gridRow: `auto`,
                                  position: 'sticky',
                                }}
                              >
                                <div
                                  onClick={() => handleOpenEditModal(event)}
                                  className={classNames(
                                    event.color.bgColor,
                                    // event.color.hoverColor,
                                    ' rounded-md text-sm px-2 cursor-pointer'
                                  )}
                                  style={{
                                    // position: 'sticky',
                                    // top: 0,
                                    width: '100%',
                                    height: '100%',
                                  }}
                                >
                                  <p
                                    className={`overflow-hidden truncate text-sm`}
                                  >
                                    <span>
                                      {event.isHotel ? (
                                        <>
                                          <FontAwesomeIcon
                                            icon={faHotel}
                                            className='mr-1'
                                          />
                                          {/* <span>{`${
                                            event.title
                                          } from ${convertTime(
                                            event.startDate
                                          )} to ${convertTime(
                                            event.endDate
                                          )}`}</span> */}
                                          <span className='text-gray-800'>
                                            {event.title}
                                          </span>
                                          <span className='ml-1 text-gray-500 font-normal'>
                                            {' '}
                                            {`from ${convertTime(
                                              event.startDate
                                            )} to ${convertTime(
                                              event.endDate
                                            )}`}
                                          </span>
                                        </>
                                      ) : (
                                        <span>
                                          <span className='text-gray-800'>
                                            {event.title}
                                          </span>
                                          <span className='ml-1 text-gray-500 font-normal'>
                                            {' '}
                                            {`from ${convertTime(
                                              event.startDate
                                            )} to ${convertTime(
                                              event.endDate
                                            )}`}
                                          </span>
                                        </span>
                                      )}
                                    </span>
                                    {/* {' - '}
                                  {event.title} */}
                                  </p>
                                </div>
                              </li>
                            );
                          })}
                      </div>
                    </div>

                    {Array.from({ length: 24 }, (_, hour) => (
                      <React.Fragment key={hour}>
                        <div>
                          <div className='sticky left-0 z-20 mt-0 -ml-14 w-14 pr-2 text-right text-xs leading-5 text-gray-400 dark:text-white'>
                            {hour.toString().padStart(2, '0') + ':00'}
                          </div>
                        </div>
                        <div />
                      </React.Fragment>
                    ))}
                    <div />
                  </div>

                  {/* Vertical lines */}

                  <div className='col-start-1 col-end-2 row-start-1 hidden grid-cols-1 grid-rows-1 divide-x divide-gray-100 sm:grid sm:grid-cols-7 dark:divide-[#707070] '>
                    <div className='col-start-1 row-span-full' />
                    <div className='col-start-2 row-span-full' />
                    <div className='col-start-3 row-span-full' />
                    <div className='col-start-4 row-span-full' />
                    <div className='col-start-5 row-span-full' />
                    <div className='col-start-6 row-span-full' />
                    <div className='col-start-7 row-span-full' />
                    {/* <div className='col-start-8 row-span-full ' /> */}
                  </div>
                  {/* Events */}

                  <ol
                    className={`col-start-1 col-end-2 row-start-1 grid sm:grid-cols-7 grid-cols-1 `}
                    style={{
                      gridTemplateRows: `repeat(${288}, minmax(0, 1fr)) auto `,
                      marginTop: `${
                        longEventsHeight ? longEventsHeight + 3 : 4
                      }px`,

                      borderTop: `${
                        longEventsHeight ? '2px solid #e2e8f0' : 'none'
                      }`,
                    }}
                  >
                    {isWithinCurrentWeek && (
                      <div
                        className='col-start-1 col-end-2 row-start-1  text-[10px] text-red-500 text-right border-t border-dashed border-red-400'
                        style={{
                          gridRow: `${
                            calculateGridRow(currentTime) - 1
                          } / span 1`,
                          gridColumn: `1 / span ${calculateGridColumn(
                            currentTime
                          )}`,
                          height: '2px',
                          zIndex: 1,
                        }}
                      >
                        <p className=' pr-2'>{formattedTime}</p>
                      </div>
                    )}
                    {/* {console.log(filteredEvents)} */}
                    {isMobile
                      ? filteredEvents.map((group, groupIndex) => {
                          const numberOfOverlappingEvents = group.length;

                          return (
                            <React.Fragment key={groupIndex}>
                              {group.map((event, eventIndex) => {
                                const shouldAdjustPosition =
                                  numberOfOverlappingEvents > 1;

                                return (
                                  <li
                                    key={eventIndex}
                                    className={`relative flex  py-[5px] px-1 text-black font-medium overflow-hidden `}
                                    style={{
                                      gridColumn: `1 `,
                                      gridRow: `${
                                        calculateGridRow(event.startDate) - 1
                                      } / span ${calculateEventSpan(
                                        event.startDate,
                                        event.endDate
                                      )}`,
                                      left: shouldAdjustPosition
                                        ? `${
                                            (eventIndex * 100) /
                                            numberOfOverlappingEvents
                                          }%`
                                        : 'auto',
                                      width: shouldAdjustPosition
                                        ? `${100 / numberOfOverlappingEvents}%`
                                        : 'auto',
                                    }}
                                  >
                                    <div
                                      onClick={() => handleOpenEditModal(event)}
                                      className={classNames(
                                        event.color.bgColor,
                                        // event.color.hoverColor,
                                        event.color.border,
                                        'w-full rounded-r-2xl border-l-[4px] p-2 m-1 cursor-pointer'
                                      )}
                                      style={{
                                        overflow: shouldAdjustPosition
                                          ? 'hidden'
                                          : 'auto',
                                        width: '100%',
                                        display: 'flex',
                                        flexDirection: 'column',
                                        position: 'relative', // Add position relative to the container
                                      }}
                                    >
                                      <div className='font-semibold overflow-hidden truncate text-gray-800'>
                                        <p>{event.title}</p>
                                        <span>
                                          from {convertTime(event.startDate)} to{' '}
                                          {convertTime(event.endDate)}
                                        </span>
                                      </div>
                                      <div className='mt-auto absolute bottom-0 right-0 p-2'>
                                        <AvatarsGroup
                                          eventParticipants={
                                            event.eventParticipants
                                          }
                                          shouldAdjustPosition={true}
                                        />
                                      </div>
                                    </div>
                                  </li>
                                );
                              })}
                            </React.Fragment>
                          );
                        })
                      : groupedEvents?.map((group, groupIndex) => {
                          const numberOfOverlappingEvents = group.length;

                          return (
                            <React.Fragment key={groupIndex}>
                              {group.map((event, eventIndex) => {
                                const shouldAdjustPosition =
                                  numberOfOverlappingEvents > 1;

                                return (
                                  <li
                                    key={eventIndex}
                                    className={`relative mt-px flex text-black  font-medium  overflow-hidden`}
                                    style={{
                                      gridColumn: `${calculateGridColumn(
                                        event.startDate
                                      )} `,
                                      gridRow: `${
                                        calculateGridRow(event.startDate) - 1
                                      } / span ${calculateEventSpan(
                                        event.startDate,
                                        event.endDate
                                      )}`,
                                      left: shouldAdjustPosition
                                        ? `${
                                            (eventIndex * 100) /
                                            numberOfOverlappingEvents
                                          }%`
                                        : 'auto',
                                      width: shouldAdjustPosition
                                        ? `${100 / numberOfOverlappingEvents}%`
                                        : 'auto',
                                    }}
                                  >
                                    <div
                                      onClick={() => handleOpenEditModal(event)}
                                      className={classNames(
                                        event.color.bgColor,
                                        // event.color.hoverColor,
                                        event.color.border,
                                        'w-full rounded-r-2xl border-l-[4px] p-2 m-1 cursor-pointer'
                                      )}
                                      style={{
                                        overflow: shouldAdjustPosition
                                          ? 'hidden'
                                          : 'auto',
                                        display: 'flex',
                                        flexDirection: 'column',
                                        position: 'relative', // Add position relative to the container
                                      }}
                                    >
                                      <div className='font-semibold overflow-hidden truncate text-gray-800'>
                                        <p>{event.title}</p>
                                        <span>
                                          from {convertTime(event.startDate)} to{' '}
                                          {convertTime(event.endDate)}
                                        </span>
                                      </div>
                                      <div className='flex flex-1 flex-col items-end justify-end'>
                                        <div className='flex flex-row items-center'>
                                          <AvatarsGroup
                                            eventParticipants={
                                              event.eventParticipants
                                            }
                                            shouldAdjustPosition={
                                              shouldAdjustPosition
                                            }
                                          />
                                        </div>
                                      </div>
                                    </div>
                                  </li>
                                );
                              })}
                            </React.Fragment>
                          );
                        })}
                  </ol>

                  {currentEvent && (
                    <EditEvent
                      open={openEditModal}
                      setOpen={setOpenEditModal}
                      initialEvent={currentEvent}
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
