import { useEffect, useState } from 'react';
import { Avatar } from '@chakra-ui/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faUserAltSlash } from '@fortawesome/free-solid-svg-icons';
import { formatDate, formatTime } from '../../utils/helpers';
import { useSelector } from 'react-redux';
import {
  blockUser,
  getAllUsers,
  unblockUser,
} from '../../services/user.service';
import useAllUsersFetching from '../../hooks/useAllUsersFetch';

export default function UserProfiles() {
  const [allUsers, setAllUsers] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const [searchTerm, setSearchTerm] = useState('');
  const [sortOption, setSortOption] = useState('recentlyCreated');
  const [filteredUsers, setFilteredUsers] = useState([]);

  const user = useSelector((state) => state.auth.userData);

  useEffect(() => {
    {
      const fetchUsers = async () => {
        try {
          const users = await getAllUsers();
          console.log(users);

          setAllUsers(users);
        } catch (error) {
          console.error('Error fetching user data:', error);
        } finally {
          setIsLoading(false);
        }
      };

      fetchUsers();
    }
  }, [user, user?.isBlocked]);

  console.log(allUsers);

  useEffect(() => {
    // Filter and sort users initially when allUsers changes
    const updatedFilteredUsers = filterAndSortUsers(
      allUsers,
      searchTerm,
      sortOption
    );
    setFilteredUsers(updatedFilteredUsers);
  }, [allUsers, searchTerm, sortOption]);

  const handleSortOptionChange = (e) => {
    const newSortOption = e.target.value;
    setSortOption(newSortOption);
  };

  const handleSearchTermChange = (e) => {
    const newSearchTerm = e.target.value;
    setSearchTerm(newSearchTerm);
  };

  const filterAndSortUsers = (users, searchTerm, sortOption) => {
    let filteredUsers = users.filter(
      (user) =>
        user.firstName.toLowerCase().includes(searchTerm.toLowerCase()) ||
        user.lastName.toLowerCase().includes(searchTerm.toLowerCase())
    );

    switch (sortOption) {
      case 'mostEvents':
        filteredUsers.sort((a, b) => {
          const aCreatedEventsCount = Object.keys(a.createdEvents || {}).length;
          const bCreatedEventsCount = Object.keys(b.createdEvents || {}).length;
          return bCreatedEventsCount - aCreatedEventsCount;
        });
        break;

      case 'recentlyCreated':
        filteredUsers.sort(
          (a, b) =>
            new Date(b.createdOn.dateTime) - new Date(a.createdOn.dateTime)
        );
        break;

      case 'oldest':
        filteredUsers.sort(
          (a, b) =>
            new Date(a.createdOn.dateTime) - new Date(b.createdOn.dateTime)
        );
        break;

      default:
    }
    return filteredUsers;
  };

  const handleBlockUser = async (userHandle, isBlocked) => {
    try {
      if (isBlocked) {
        await unblockUser(userHandle);
      } else {
        await blockUser(userHandle);
      }

      // Update the user's isBlocked property in the state
      setAllUsers((prevUsers) =>
        prevUsers.map((user) =>
          user.handle === userHandle ? { ...user, isBlocked: !isBlocked } : user
        )
      );
    } catch (error) {
      console.error('Error handling user block/unblock:', error);
    }
  };

  return (
    <div className='p-4 bg-white dark:bg-[#202020] shadow-md rounded-2xl'>
      <h2 className='text-2xl font-semibold  dark:text-white px-2 pb-3'>
        All Users
      </h2>
      <div className='mb-4 flex justify-between items-center gap-3 max-sm:flex-col'>
        <input
          type='text'
          placeholder='Search users...'
          value={searchTerm}
          onChange={handleSearchTermChange}
          className='px-4 py-2 border border-gray-300 dark:text-white dark:bg-[#252525] dark:border-gray-600 rounded-md focus:outline-none  w-full'
        />
        <select
          value={sortOption}
          onChange={handleSortOptionChange}
          className='px-4 py-2 border border-gray-300 dark:text-gray-100 rounded-md min-w-[200px] dark:border-gray-600 dark:bg-[#252525] focus:outline-none max-sm:w-full'
        >
          <option value='none'>Sort By</option>
          <option value='mostPosts'>Most Events</option>
          <option value='recentlyCreated'>Recently Created</option>
          <option value='oldest'>Oldest</option>
        </select>
      </div>
      <div className='grid grid-cols-1 gap-5 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4'>
        {filteredUsers.map((user) => (
          <div
            key={user?.uid}
            className='bg-white dark:bg-[#181818] p-6 rounded-xl shadow-md flex-col flex justify-between border dark:border-[#202020]'
          >
            <div>
              <div className='flex justify-center '>
                <Avatar
                  size='2xl'
                  className='ring-2 ring-blue-600 dark:ring-[#c2df2b]'
                  name={user?.firstName + ' ' + user?.lastName}
                  src={user?.profileImage}
                />
              </div>
              <div className='mt-4'>
                <h3 className='text-xl font-semibold text-gray-900 text-center dark:text-white'>
                  {user?.firstName} {user?.lastName}{' '}
                  <span className='text-sm text-gray-600 dark:text-gray-300'>
                    ({user?.role || 'User'})
                  </span>
                </h3>
                <div className='mt-2'>
                  <p className='text-sm text-gray-700 dark:text-white'>
                    <span className='font-semibold'>Account Created on:</span>{' '}
                    {`${formatDate(user?.createdOn?.dateTime)} ${formatTime(
                      user?.createdOn?.dateTime
                    )}`}
                  </p>
                  <p className='text-sm text-gray-700 dark:text-white'>
                    <span className='font-semibold'>Email:</span> {user.email}
                  </p>
                  <p className='text-sm text-gray-700 dark:text-white'>
                    <span className='font-semibold'>About:</span>{' '}
                    {user.aboutMe || 'No description provided'}
                  </p>
                </div>
              </div>
            </div>
            <div className='mt-4 flex justify-center'>
              <button
                disabled={user.role?.toLowerCase() === 'admin'}
                onClick={() => handleBlockUser(user.handle, user.isBlocked)}
                className={`px-4 py-2 text-sm font-medium border ${
                  user.role?.toLowerCase() === 'admin'
                    ? 'bg-gray-200 text-gray-500 cursor-not-allowed'
                    : user.isBlocked
                    ? 'border-red-500 text-red-500 hover:bg-green-200 hover:text-green-500 hover:border-green-500'
                    : '  hover:border-red-500 hover:text-red-500 border-blue-500 dark:border-[#c2df2b] dark:text-white text-black'
                } rounded-xl border-2 py-2 px-3 text-sm font-medium  shadow-sm hover:bg-gray-200  dark:hover:bg-gray-800 focus:outline-none  `}
              >
                <FontAwesomeIcon
                  icon={user.isBlocked ? faUserAltSlash : faUser}
                  className='mr-1'
                />
                {user.isBlocked ? 'Unblock' : 'Block User'}
              </button>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
