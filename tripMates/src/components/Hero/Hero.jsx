import SearchHotels from '../SearchHotels/SearchHotels';

export default function Hero({ handleSearch }) {
  return (
    <main>
      <div>
        <div className='relative '>
          <div className='max-w-7xl mx-auto sm:px-6 lg:px-8 mt-6'>
            <div className='relative  shadow-lg sm:border-4 border-blue-500 dark:border-[#c2df2b] sm:rounded-2xl sm:overflow-hidden'>
              <div className='absolute inset-0'>
                <img
                  className='h-full w-full object-cover blur-[4px]'
                  src='./src/assets/hero-section/hotel-bg.jpg'
                  alt='People working on laptops'
                />
                <div className='absolute inset-0 bg-gray-500  mix-blend-multiply' />
              </div>
              <div className='relative px-4 py-16 sm:px-6 sm:py-24 lg:py-24 lg:px-8'>
                <h1 className='text-center text-4xl font-extrabold tracking-tight sm:text-5xl lg:text-6xl'>
                  <span className='block text-white '>
                    Unlock Unforgettable Getaways
                  </span>

                  <span className='block mt-2   text-transparent bg-clip-text bg-gradient-to-l from-blue-300 to-green-300 text-4xl '>
                    Explore, Plan, and Book Hotels with Friends!
                  </span>
                </h1>

                <p className='mt-3 max-w-lg mx-auto text-center text-2xl p-2 font-medium text-white sm:max-w-3xl   '>
                  Welcome to{' '}
                  <span className='font-bold  text-blue-200 '>TripMates</span>,
                  where turning your group travel dreams into reality has never
                  been easier. Imagine coordinating and booking your next
                  vacation with friends or family seamlessly, all within a
                  single, user-friendly app.
                </p>
              </div>
            </div>
          </div>

          <div
            key='search'
            className='absolute max-w-4xl w-full mx-auto px-8 text-center -bottom-16 right-0 left-0 lg:px-12 sm:px-16'
          >
            <SearchHotels handleSearch={handleSearch} />
          </div>
        </div>
      </div>
    </main>
  );
}
