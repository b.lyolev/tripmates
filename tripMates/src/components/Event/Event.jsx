import { isWithinInterval, startOfDay, endOfDay } from 'date-fns';
import { format } from 'date-fns';

export function Event({
  startDate,
  endDate,
  startDateTime,
  endDateTime,
  title,
  color,
  currentWeekStartDate,
  currentWeekEndDate,
}) {
  const [startHour, startMinute] = startDateTime.split(':').map(Number);
  const [endHour, endMinute] = endDateTime.split(':').map(Number);

  const eventStartDate = new Date(startDate);
  eventStartDate.setHours(startHour, startMinute);

  const eventEndDate = new Date(endDate);
  eventEndDate.setHours(endHour, endMinute);
  // console.log(eventEndDate);

  const isStartWithinCurrentWeek = isWithinInterval(eventStartDate, {
    start: startOfDay(currentWeekStartDate),
    end: endOfDay(currentWeekEndDate),
  });

  const isEndWithinCurrentWeek = isWithinInterval(eventEndDate, {
    start: startOfDay(currentWeekStartDate),
    end: endOfDay(currentWeekEndDate),
  });

  const isMiddleWeek =
    eventStartDate < startOfDay(currentWeekStartDate) &&
    eventEndDate > endOfDay(currentWeekEndDate);

  let colStart = 0;
  let colEnd = 0;
  if (isStartWithinCurrentWeek) {
    const dayIndex = eventStartDate.getDay();
    colStart = dayIndex + 1;
    colEnd =
      colStart +
      Math.ceil((eventEndDate - eventStartDate) / (24 * 60 * 60 * 1000));

    if (isEndWithinCurrentWeek) {
      colEnd -= colStart;
    }
  } else if (isEndWithinCurrentWeek || isMiddleWeek) {
    colStart = 1;
    colEnd = Math.min(
      Math.ceil((eventEndDate - currentWeekStartDate) / (24 * 60 * 60 * 1000)) +
        1,
      8
    );
  }

  if (!isStartWithinCurrentWeek && !isEndWithinCurrentWeek && !isMiddleWeek) {
    return null;
  }

  const startGridRow = startHour * 12 + Math.floor((startMinute / 5) * 1) + 1;
  const span =
    (endHour - startHour) * 12 +
    Math.floor(((endMinute - startMinute) / 5) * 1);

  return (
    <li
      className={`relative m-[0.5px] flex-1  flex sm:col-start-${colStart}`}
      style={{
        gridColumn: `${colStart} / span ${colEnd}`,
        gridRow: `${startGridRow} / span ${span}`,
      }}
    >
      <a
        href='#'
        className={`group flex flex-col overflow-y-auto w-full m-[1px] rounded-lg bg-${color}-50 p-2 text-xs leading-5 hover:bg-${color}-100`}
      >
        <p className={`order-1 font-semibold text-${color}-700`}>{title}</p>
        <p className={`text-${color}-500 group-hover:text-${color}-700`}>
          <time dateTime={`${startDate}T${startHour}:${startMinute}`}>
            {format(eventStartDate, 'MMMM d')}
            {eventStartDate.getDate() !== eventEndDate.getDate() &&
              ` - ${format(eventEndDate, 'MMMM d')}`}
          </time>
          <br />
          <time dateTime={`${endDate}T${endHour}:${endMinute}`}>
            {startHour < 10 ? '0' + startHour : startHour}:
            {startMinute < 10 ? '0' + startMinute : startMinute}{' '}
            {startHour >= 12 ? 'PM' : 'AM'} -{' '}
            {endHour < 10 ? '0' + endHour : endHour}:
            {endMinute < 10 ? '0' + endMinute : endMinute}{' '}
            {endHour >= 12 ? 'PM' : 'AM'}
          </time>
        </p>
      </a>
    </li>
  );
}
