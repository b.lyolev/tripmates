import React, { useEffect, useState } from 'react';
import { Fragment } from 'react';
import { Popover, Transition } from '@headlessui/react';
import { ChevronDownIcon } from '@heroicons/react/solid';
import { useDispatch, useSelector } from 'react-redux';

import { setSelectedFilters } from '../../reducers/filterReducers';

export default function Filters({ filters }) {
  const selectedFilters = useSelector((state) => state.filters);
  const dispatch = useDispatch();

  const handleFilterChange = (sectionId, optionValue, isSelected) => {
    dispatch(
      setSelectedFilters({
        sectionId,
        optionValue,
        isSelected,
      })
    );
  };

  // console.log(selectedFilters);
  return (
    <div>
      <Popover.Group className='sm:flex sm:items-baseline sm:space-x-8 space-x-3'>
        {filters.map((section) => (
          <Popover
            as='div'
            key={section.name}
            id='desktop-menu'
            className='relative z-10 inline-block text-left '
          >
            <div>
              <Popover.Button
                className={`group inline-flex items-center justify-center text-base font-medium   dark:text-white dark:hover:text-gray-300 hover:text-gray-900 focus:outline-none ${
                  Object.values(selectedFilters[section.id] || {}).some(
                    (isSelected) => isSelected
                  )
                    ? 'text-blue-600'
                    : 'text-gray-700'
                }`}
              >
                <span>{section.name}</span>
                {Object.values(selectedFilters[section.id] || {}).some(
                  (isSelected) => isSelected
                ) && (
                  <span className='ml-1.5 rounded py-0.5 px-1.5 bg-gray-200 text-xs font-semibold text-gray-700  tabular-nums'>
                    {
                      Object.values(selectedFilters[section.id] || {}).filter(
                        (isSelected) => isSelected
                      ).length
                    }
                  </span>
                )}
                <ChevronDownIcon
                  className='flex-shrink-0 -mr-1 ml-1 h-6 w-6 text-gray-400 group-hover:text-gray-500'
                  aria-hidden='true'
                />
              </Popover.Button>
            </div>

            <Transition
              as={Fragment}
              enter='transition ease-out duration-100'
              enterFrom='transform opacity-0 scale-95'
              enterTo='transform opacity-100 scale-100'
              leave='transition ease-in duration-75'
              leaveFrom='transform opacity-100 scale-100'
              leaveTo='transform opacity-0 scale-95'
            >
              <Popover.Panel className='origin-top-right absolute right-0 mt-2 p-4 rounded-xl bg-white shadow-xl ring-1 ring-black ring-opacity-5  focus:outline-none dark:bg-[#282828] '>
                <form className='space-y-4'>
                  {section.options.map((option, optionIdx) => (
                    <div
                      key={option.value + optionIdx}
                      className='flex items-center'
                    >
                      <input
                        id={`filter-${section.id}-${optionIdx}`}
                        name={`${section.id}[]`}
                        value={option.value}
                        type='checkbox'
                        className='h-4 w-4 border-gray-300 rounded text-blue-600 focus:ring-blue-500 dark:fill-[#c2df2b] dark:ring-[#c2df2b] '
                        onChange={(e) =>
                          handleFilterChange(
                            section.id,
                            option.value,
                            e.target.checked
                          )
                        }
                        checked={
                          selectedFilters[section.id]?.[option.value] || false
                        }
                      />

                      <label
                        htmlFor={`filter-${section.id}-${optionIdx}`}
                        className='ml-3 pr-6 text-sm font-medium text-gray-900 whitespace-nowrap dark:text-white'
                      >
                        {option.label}
                      </label>
                    </div>
                  ))}
                </form>
              </Popover.Panel>
            </Transition>
          </Popover>
        ))}
      </Popover.Group>
    </div>
  );
}
