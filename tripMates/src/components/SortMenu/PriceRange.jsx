import React, { useState } from 'react';
import { Fragment } from 'react';
import { Popover, Transition } from '@headlessui/react';
import { ChevronDownIcon } from '@heroicons/react/solid';
import RangeSlider from 'react-range-slider-input';
import 'react-range-slider-input/dist/style.css';
import { useDispatch, useSelector } from 'react-redux';
import { setPriceRange } from '../../reducers/filterReducers';
import './PriceRange.css';

export default function PriceRange() {
  const defaultPriceRange = [0, 5000];
  const priceRange = useSelector((state) => state.priceRange);

  const dispatch = useDispatch();

  const handlePriceChange = (newRange) => {
    dispatch(setPriceRange(newRange));
  };

  const isDefaultPriceRange =
    JSON.stringify(priceRange) === JSON.stringify(defaultPriceRange);

  const priceTextClass = isDefaultPriceRange
    ? 'text-gray-900 dark:text-white  '
    : 'text-blue-600 dark:text-white ';
  return (
    <div>
      <Popover.Group className='sm:flex sm:items-baseline sm:space-x-8 space-x-3 '>
        <Popover
          as='div'
          key='Price'
          id='desktop-menu'
          className='relative z-10 inline-block text-left '
        >
          <div>
            <Popover.Button className='group inline-flex items-center justify-center text-base font-medium text-gray-700 hover:text-gray-900 focus:outline-none'>
              <span className={priceTextClass}>Price</span>

              <ChevronDownIcon
                className='flex-shrink-0 -mr-1 ml-1 h-6 w-6 text-gray-400 group-hover:text-gray-500'
                aria-hidden='true'
              />
            </Popover.Button>
          </div>

          <Transition
            as={Fragment}
            enter='transition ease-out duration-100'
            enterFrom='transform opacity-0 scale-95'
            enterTo='transform opacity-100 scale-100'
            leave='transition ease-in duration-75'
            leaveFrom='transform opacity-100 scale-100'
            leaveTo='transform opacity-0 scale-95'
          >
            <Popover.Panel className='origin-top-right absolute right-0 mt-2 bg-white dark:bg-[#282828] rounded-md shadow-2xl p-4 ring-1 ring-black ring-opacity-5 focus:outline-none'>
              <form className='space-y-4 w-64'>
                <p>
                  <label
                    htmlFor='range-slider-red'
                    className='text-sm font-medium text-gray-900 dark:text-white flex justify-between px-5'
                  >
                    <span>Price Range</span>
                    <span>{`${priceRange[0]}$ - ${priceRange[1]}$`}</span>
                  </label>
                </p>
                <RangeSlider
                  min={0}
                  max={5000}
                  step={10}
                  value={priceRange}
                  onInput={handlePriceChange}
                />
              </form>
            </Popover.Panel>
          </Transition>
        </Popover>
      </Popover.Group>
    </div>
  );
}
