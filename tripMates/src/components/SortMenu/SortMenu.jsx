import { Fragment, useState } from 'react';
import {
  Dialog,
  Disclosure,
  Menu,
  Popover,
  Transition,
} from '@headlessui/react';
import { XIcon } from '@heroicons/react/outline';
import { ChevronDownIcon } from '@heroicons/react/solid';
import Sort from './Sort';
import Filters from './Filters';
import PriceRange from './PriceRange';
import { useSelector } from 'react-redux';

const sortOptions = [
  { name: 'Most Popular', href: '#' },
  { name: 'Best Rating', href: '#' },
  { name: 'Newest', href: '#' },
];

const filters = [
  {
    id: 'category',
    name: 'Category',
    options: [
      { value: 'hotels', label: 'Hotels' },
      { value: 'villas', label: 'Villas' },
      { value: 'cottages', label: 'Cottages' },
      { value: 'apartments', label: 'Apartments' },
      { value: 'resorts', label: 'Resorts' },
    ],
  },
];

export default function SortMenu() {
  return (
    <div>
      <div className=' mx-auto  text-left  lg:max-w-7xl px-2'>
        <div className='py-3'>
          <h1 className='text-3xl font-bold tracking-tight text-gray-900 dark:text-white'>
            Trending hotels
          </h1>
        </div>

        <section
          aria-labelledby='filter-heading'
          className='border-t border-gray-200 py-4'
        >
          <h2 id='filter-heading' className='sr-only'>
            Product filters
          </h2>

          <div className='flex items-center justify-between'>
            <Sort sortOptions={sortOptions} />
            <div className='flex gap-4'>
              <Filters filters={filters} />
              <PriceRange />
            </div>
          </div>
        </section>
      </div>
    </div>
  );
}
