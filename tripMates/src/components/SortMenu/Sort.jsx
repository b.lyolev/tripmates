import React from 'react';
import { Fragment } from 'react';
import { Menu, Transition } from '@headlessui/react';
import { ChevronDownIcon } from '@heroicons/react/solid';
import { classNames } from '../../utils/helpers';
import { useDispatch, useSelector } from 'react-redux';
import { setSelectedSortOption } from '../../reducers/filterReducers';

export default function Sort({ sortOptions }) {
  const dispatch = useDispatch();
  const selectedSortOption = useSelector((state) => state.sort);

  const handleSortOptionClick = (option) => {
    dispatch(setSelectedSortOption(option)); // Dispatch the action to update the selected sort option
  };
  return (
    <div>
      <Menu as='div' className='relative z-10 inline-block text-left'>
        <div>
          <Menu.Button className='group inline-flex justify-center rounded-xl text-base font-medium text-gray-700 dark:text-white dark:hover:text-gray-300 hover:text-gray-900'>
            Sort
            <ChevronDownIcon
              className='flex-shrink-0 -mr-1 ml-1 h-6 w-5 text-gray-400 group-hover:text-gray-500'
              aria-hidden='true'
            />
          </Menu.Button>
        </div>

        <Transition
          as={Fragment}
          enter='transition ease-out duration-100'
          enterFrom='transform opacity-0 scale-95'
          enterTo='transform opacity-100 scale-100'
          leave='transition ease-in duration-75'
          leaveFrom='transform opacity-100 scale-100'
          leaveTo='transform opacity-0 scale-95'
        >
          <Menu.Items className='origin-top-left absolute left-0 z-10 mt-2 w-40 overflow-hidden rounded-xl bg-white shadow-xl ring-1 ring-black ring-opacity-5  focus:outline-none dark:bg-[#282828] '>
            <div>
              {sortOptions.map((option, index) => (
                <Menu.Item key={index}>
                  {({ active }) => (
                    <a
                      className={classNames(
                        active ? 'bg-gray-100 dark:bg-[#353535] ' : '',
                        selectedSortOption === option.name
                          ? 'bg-gray-200 dark:bg-[#404040]'
                          : '', // Apply the selected option style conditionally
                        'block px-4 py-2 text-sm font-medium text-gray-900 cursor-pointer  dark:text-[#e0e0e0] shadow-inner'
                      )}
                      onClick={() => handleSortOptionClick(option.name)}
                    >
                      {option.name}
                    </a>
                  )}
                </Menu.Item>
              ))}
            </div>
          </Menu.Items>
        </Transition>
      </Menu>
    </div>
  );
}
