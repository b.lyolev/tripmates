import React, { useEffect, useState } from 'react';
import { Fragment } from 'react';
import { Popover, Transition } from '@headlessui/react';
import {
  AnnotationIcon,
  BellIcon,
  ChatAlt2Icon,
  InboxIcon,
  MenuIcon,
  QuestionMarkCircleIcon,
  XIcon,
} from '@heroicons/react/outline';
import { ChevronDownIcon } from '@heroicons/react/solid';
import { classNames } from '../../utils/helpers';
import LoginModal from '../LoginModal/LoginModal';
import RegisterModal from '../RegisterModal/RegisterModal';
import UserProfileDropdown from './UserProfile/UserProfile';
import { logoutUser } from '../../services/auth.service';
import { useNavigate } from 'react-router-dom';
import UserProfileMobile from './UserProfileMobile/UserProfileMobile';
import Notification from './Notification/Notification';
import { CalendarIcon } from 'lucide-react';
import { Link } from 'react-router-dom';
import Logo from '../Logo/Logo';
import DarkModeToggle from '../DarkModeToggle/DarkModeToggle';
import { useDispatch, useSelector } from 'react-redux';
import { setUser, setUserData } from '../../reducers/authActions';
import { motion, AnimatePresence } from 'framer-motion';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from '../../config/firebase-config';
import LoadingSpinner from '../LoadingSpinner/LoadingSpinner';
import { useRef } from 'react';

const navigation = [
  { name: 'Events', href: '/events' },
  { name: 'Calendar', href: '/calendar/week-view' },
  { name: 'Friends', href: '/friends' },
];
export default function NavBar() {
  const dispatch = useDispatch();

  const [userLoaded, loading] = useAuthState(auth);
  const user = useSelector((state) => state.auth.user);

  const userData = useSelector((state) => state.auth.userData);

  const [openLoginModal, setOpenLoginModal] = useState(false);
  const [openRegisterModal, setOpenRegisterModal] = useState(false);
  const popoverButtonRef = useRef(null);

  const handleOpenLoginModal = () => {
    setOpenLoginModal(true);
  };

  const handleOpenRegisterModal = () => {
    setOpenRegisterModal(true);
  };

  const handleNavigationClick= () => {
    popoverButtonRef.current.click();
  }

  const navigateTo = useNavigate();

  const onLogout = () => {
    logoutUser().then(() => {
      dispatch(setUser(null));
      dispatch(setUserData(null));
    });

    setOpenLoginModal(false);
    setOpenRegisterModal(false);

    navigateTo('/');
  };

  return (
    <motion.header>
      <Popover className='relative z-50'>
        <div className='flex justify-between items-center max-w-7xl mx-auto px-4 py-6 sm:px-6 lg:justify-start md:space-x-10 lg:px-8 '>
          <div className='flex justify-start  lg:w-0 lg:flex-1   '>
            <Link to={'/'} className='flex flex-row items-center'>
              <Logo />
              <div className='  text-2xl  ml-2  font-extrabold  sm:text-4xl'>
                <span className='font-bold text-blue-500 dark:text-[#c2df2b]  '>
                  Trip
                </span>
                <span className='font-bold text-blue-500 dark:text-[#c2df2b] '>
                  Mates
                </span>
              </div>
            </Link>
          </div>
          <div className='lg:hidden flex items-center gap-3'>
            <div className='inline-flex items-center  focus:outline-none'>
              {user && <Notification />}
              <DarkModeToggle />
            </div>
            <Popover.Button className='bg-gray rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray focus:outline-none focus:ring-2 focus:ring-inset focus:ring-blue-500 dark:focus:ring-[#c2df2b]'>
              <span className='sr-only'>Open menu</span>
              <MenuIcon className='h-6 w-6' aria-hidden='true' />
            </Popover.Button>
          </div>

          <Popover.Group as='nav' className='hidden lg:flex space-x-5'>
            {user &&
              navigation.map((item) => (
                <motion.div
                  className='-mt-2'
                  key={item.name}
                  whileHover={{
                    translateY: 5,

                    scale: 1.05,
                    transition: { duration: 0.2 },
                  }}
                >
                  <Link
                    key={item.name}
                    to={item.href}
                    className=' ml-6  border-b-4 border-blue-600 dark:border-[#c2df2b] py-2 px-4 text-base font-medium text-blue-600 dark:text-[#c2df2b] '
                  >
                    {item.name}
                  </Link>
                </motion.div>
              ))}
          </Popover.Group>

          <div className='hidden lg:flex items-center justify-end md:flex-1 lg:w-0'>
            {user !== null ? (
              <>
                <div className='flex justify-center items-center'>
                  <div className='mt-2'>
                    {' '}
                    <Notification />{' '}
                  </div>
                  <div className='flex gap-1 items-center border-2 rounded-full px-[5px] dark:bg-[#202020] bg-white'>
                    <DarkModeToggle />
                    <UserProfileDropdown
                      currentUser={userData}
                      onLogout={onLogout}
                    />
                  </div>
                </div>
              </>
            ) : (
              <>
                <div className='flex flex-row gap-8 border-2 border-gray-200 p-2 px-3 rounded-full items-center bg-white dark:bg-[#202020]'>
                  <DarkModeToggle />
                  <button
                    onClick={handleOpenLoginModal}
                    className='whitespace-nowrap text-base font-medium text-gray-500 hover:text-gray-900 dark:text-white dark:hover:text-gray-300'
                  >
                    Sign in
                  </button>
                  <LoginModal
                    open={openLoginModal}
                    setOpen={setOpenLoginModal}
                  />
                  <button
                    onClick={handleOpenRegisterModal}
                    className=' whitespace-nowrap inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-full shadow-sm text-base font-medium text-white bg-blue-600 hover:bg-blue-700 dark:text-black dark:bg-[#c2df2b] dark:hover:bg-[#b2d233]'
                  >
                    Register
                  </button>
                  <RegisterModal
                    open={openRegisterModal}
                    setOpen={setOpenRegisterModal}
                  />
                </div>
              </>
            )}
          </div>
        </div>

        <Transition
          as={Fragment}
          enter='duration-200 ease-out'
          enterFrom='opacity-0 scale-95'
          enterTo='opacity-100 scale-100'
          leave='duration-100 ease-in'
          leaveFrom='opacity-100 scale-100'
          leaveTo='opacity-0 scale-95'
        >
          <Popover.Panel
            focus
            className='absolute z-30 top-0 inset-x-0 p-2 transition transform origin-top-right lg:hidden'
          >
            <div className='rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 bg-white dark:bg-[#252525] divide-y-2 divide-gray-50'>
              <div className='pb-3 pt-3 px-5'>
                <div className='flex items-center justify-between'>
                  <div className='flex flex-row '>
                    <div className='h-8 w-auto ' />
                    <Logo />
                    <div className='  text-2xl  ml-2  font-extrabold  sm:text-4xl'>
                      <span className='font-bold text-blue-500 dark:text-[#c2df2b]  '>
                        Trip
                      </span>
                      <span className='font-bold text-blue-500 dark:text-[#c2df2b] '>
                        Mates
                      </span>
                    </div>
                  </div>
                  <div className='-mr-2'>
                    <Popover.Button ref={popoverButtonRef} className='bg-gray rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray focus:outline-none focus:ring-2 focus:ring-inset focus:ring-blue-500 dark:focus:ring-[#c2df2b]'>
                      <span className='sr-only'>Close menu</span>
                      <XIcon className='h-6 w-6' aria-hidden='true' />
                    </Popover.Button>
                  </div>
                </div>
              </div>
              <div className='py-6 px-5 '>
                <div className='grid gap-4 mt-4 '>
                  {user &&
                    navigation.map((item) => (
                      <Link
                        key={item.name}
                        to={item.href}
                        onClick={handleNavigationClick}
                        className='text-base font-medium text-gray-900 dark:text-white hover:text-gray-700'
                      >
                        {item.name}
                      </Link>
                    ))}
                </div>

                {user ? (
                  <UserProfileMobile
                    currentUser={userData}
                    onLogout={onLogout}
                  />
                ) : (
                  <div className='mt-1'>
                    <button
                      onClick={handleOpenRegisterModal}
                      className='w-full flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white dark:text-black bg-blue-600 dark:bg-[#c2df2b] hover:bg-blue-700 dark:hover:bg-[#c2df2b]'
                    >
                      Register
                    </button>
                    <p className='mt-3 text-center text-base font-medium text-gray-500 dark:text-gray-400 '>
                      Existing customer?{' '}
                      <button
                        onClick={handleOpenLoginModal}
                        className='text-gray-900 dark:text-white'
                      >
                        Sign in
                      </button>
                    </p>
                  </div>
                )}
              </div>
            </div>
          </Popover.Panel>
        </Transition>
      </Popover>
    </motion.header>
  );
}
