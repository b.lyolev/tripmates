import { CheckIcon, XIcon } from '@heroicons/react/solid';
import { motion, useAnimation } from 'framer-motion';
import { useState, useEffect } from 'react';
import { classNames } from '../../../utils/helpers';
import { set } from 'firebase/database';

export default function NotificationCard({
  notification,
  handleAcceptRequest,
  handleRejectRequest,
}) {
  const controls = useAnimation();
  const [swipe, setSwipe] = useState({ direction: null, icon: null });

  const [color, setColor] = useState('dark:bg-black bg-white');

  const handleDrag = (_, info) => {
    if (info.offset.x > 100) {
      setColor('bg-green-500');
    } else if (info.offset.x < -100) {
      setColor('bg-red-500');
    } else {
      setColor('dark:bg-black bg-white');
    }
  };

  const handleDragEnd = (event, info) => {
    const offset = info.offset.x;

    if (offset < -200) {
      controls.start({ x: -300, opacity: 1 });
      handleRejectRequest(notification); // call reject function here
    } else if (offset > 200) {
      controls.start({ x: 300, opacity: 1 });
      handleAcceptRequest(notification); // call accept function here
    } else {
      controls.start({ x: 0, opacity: 1 });
    }
  };
  return (
    <motion.div
      drag='x'
      onDrag={handleDrag}
      onDragEnd={handleDragEnd}
      animate={controls}
      whileTap={{ cursor: 'grabbing' }}
      whileHover={{ cursor: 'grab' }}
      transition={{ type: 'spring', stiffness: 300, damping: 30 }}
      className={classNames(
        'flex flex-col border dark:text-white text-black rounded-2xl items-center border-gray-200 dark:border-gray-600 py-3 shadow-md transition-colors duration-300 ease-in-out font-medium',
        color
      )}
    >
      <div className='flex flex-row justify-between items-center gap-3 '>
        {notification.firstName} {notification.lastName} sent you a friend
        request
        <div className='flex justify-center space-x-2 items-center'>
          <button
            className='text-green-500 hover:text-green-700'
            onClick={() => handleAcceptRequest(notification)}
          >
            <CheckIcon className='w-5 h-5' />
          </button>
          <button
            className='text-red-500 hover:text-red-700'
            onClick={() => handleRejectRequest(notification)}
          >
            <XIcon className='w-5 h-5' />
          </button>
        </div>
      </div>
    </motion.div>
  );
}
