import { Fragment } from 'react';
import { BellIcon, CheckIcon, XIcon } from '@heroicons/react/outline';
import { useSelector } from 'react-redux';
import { Menu, Transition } from '@headlessui/react';
import {
  acceptFriendRequest,
  rejectFriendRequest,
  fetchUsersFromUIDs,
} from '../../../services/contacts.service';
import { useEffect, useState } from 'react';
import { classNames } from '../../../utils/helpers';
import NotificationCard from './NotificationCard';

export default function Notification() {
  const { userData, userRequests } = useSelector((state) => state.auth);
  const [notifications, setNotifications] = useState([]);

  useEffect(() => {
    if (!userRequests) return;
    const fetchFriends = async () => {
      const requests = await fetchUsersFromUIDs(userRequests);
      setNotifications(requests);
    };

    fetchFriends();
  }, [userRequests]);

  const handleAcceptRequest = async (user) => {
    await acceptFriendRequest(userData.handle, user.handle);
  };

  const handleRejectRequest = async (user) => {
    await rejectFriendRequest(userData.handle, user.handle);
  };

  return (
    <Menu as='div' className='relative'>
      <Menu.Button
        type='button'
        className=' mr-3 flex items-center   dark:text-[#e0e0e0] py-2 pl-3 pr-2 text-sm font-medium text-gray-700 hover:bg-text-blue-500 dark:hover:text-[#c1df2b]'
      >
        {notifications?.length > 0 && (
          <span className='absolute -top-1 right-2 h-5 w-5 rounded-full bg-red-500 flex justify-center items-center text-white'>
            {notifications?.length}
          </span>
        )}
        <BellIcon className='h-7 w-7' aria-hidden='true' />
      </Menu.Button>

      <Transition
        as={Fragment}
        enter='transition ease-out duration-100'
        enterFrom='transform opacity-0 scale-95'
        enterTo='transform opacity-100 scale-100'
        leave='transition ease-in duration-75'
        leaveFrom='transform opacity-100 scale-100'
        leaveTo='transform opacity-0 scale-95'
      >
        <Menu.Items className='absolute right-0 mt-2 p-2 w-96  origin-top-center overflow-hidden rounded-xl bg-white shadow-xl ring-1 ring-black ring-opacity-5 focus:outline-none dark:bg-[#303030]'>
          <div className=''>
            {notifications?.length === 0 ? (
              <div className='text-center p-2 text-gray-700 dark:text-white'>
                No new notifications
              </div>
            ) : (
              notifications?.map((notification) => (
                <Menu.Item key={notification.uid}>
                  <NotificationCard
                    key={notification.uid}
                    notification={notification}
                    handleAcceptRequest={handleAcceptRequest}
                    handleRejectRequest={handleRejectRequest}
                  />
                </Menu.Item>
              ))
            )}
          </div>
        </Menu.Items>
      </Transition>
    </Menu>
  );
}
