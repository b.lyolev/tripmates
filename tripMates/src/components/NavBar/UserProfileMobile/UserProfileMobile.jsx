import React, { Fragment } from 'react';
import { Disclosure, Menu, Transition } from '@headlessui/react';
import { Avatar } from '@chakra-ui/react';

export default function UserProfileMobile({ currentUser, onLogout }) {
  return (
    <div>
      <div className='flex items-center '>
        <div className='flex-shrink-0'>
          <Avatar
            size='md'
            className='ring-2 ring-blue-600 dark:ring-[#c2df2b] mt-5'
            name={
              currentUser?.firstName && currentUser?.lastName
                ? currentUser.firstName + ' ' + currentUser.lastName
                : ''
            }
            src={currentUser?.profileImage}
          />
        </div>

        <div className='NameOfUser mx-2 text-left px-1'>
          <span className='text-base font-semibold tracking-tight text-gray-800 dark:text-gray-400'>
            {currentUser?.handle}
          </span>
          <p className='text-sm font-semibold text-blue-400 dark:text-[#c2df2b]'>
            {currentUser?.role || 'User'}
          </p>
        </div>
      </div>
      <div className='mt-3 px-2 space-y-1'>
        <Disclosure>
          <Disclosure.Button
            as='a'
            href='/profile'
            className='block  py-1 text-base font-medium text-gray-800 dark:text-white hover:text-blue-600 dark:hover:text-[#c2df2b] '
          >
            Your Profile
          </Disclosure.Button>

          <Disclosure.Button
            as='a'
            href='#'
            onClick={onLogout}
            className='block py-1 rounded-md text-base font-medium text-gray-800 dark:text-white hover:text-blue-600 dark:hover:text-[#c2df2b]'
          >
            Sign out
          </Disclosure.Button>
        </Disclosure>
      </div>
    </div>
  );
}
