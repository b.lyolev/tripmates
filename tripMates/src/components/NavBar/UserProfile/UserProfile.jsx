import React, { Fragment } from 'react';
import { Menu, Transition } from '@headlessui/react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import { Avatar, AvatarBadge } from '@chakra-ui/react';
import { BellIcon } from '@heroicons/react/outline';
import DarkModeToggle from '../../DarkModeToggle/DarkModeToggle';

export default function UserProfileDropdown({ currentUser, onLogout }) {
  return (
    <>
      <Menu as='div' className='relative flex-shrink-0 '>
        <div>
          <Menu.Button className='items-center  py-2  px-2 rounded-full flex text-sm text-white  '>
            <span className='sr-only'>Open user menu</span>
            <div className='NameOfUser  text-right mr-3 '>
              <span className=' text-lg font-semibold tracking-tight text-gray-800 dark:text-white leading'>
                {currentUser?.handle}
              </span>
            </div>
            <Avatar
              size='sm'
              className='ring-2 ring-blue-600 dark:ring-[#c2df2b]'
              name={
                currentUser?.firstName && currentUser?.lastName
                  ? currentUser.firstName + ' ' + currentUser.lastName
                  : ''
              }
              src={currentUser?.profileImage}
            >
              <AvatarBadge boxSize='1em' bg='red.500' />
            </Avatar>
          </Menu.Button>
        </div>
        <Transition
          as={Fragment}
          enter='transition ease-out duration-100'
          enterFrom='transform opacity-0 scale-95'
          enterTo='transform opacity-100 scale-100'
          leave='transition ease-in duration-75'
          leaveFrom='transform opacity-100 scale-100'
          leaveTo='transform opacity-0 scale-95'
        >
          <Menu.Items className='origin-top-right absolute right-0 mt-2 w-48 shadow-lg py-1 bg-white dark:bg-[#171717] ring-1 ring-black ring-opacity-5 focus:outline-none rounded-xl'>
            <Menu.Item>
              {({ active }) => (
                <Link
                  to='/profile'
                  className={classNames(
                    'block px-4 py-2 text-base text-gray-700  hover:text-blue-600  dark:text-[#e0e0e0] dark:hover:text-[#c2df2b]'
                  )}
                >
                  Your Profile
                </Link>
              )}
            </Menu.Item>
            <Menu.Item>
              {({ active }) => (
                <a
                  href='#'
                  onClick={onLogout}
                  className={classNames(
                    'block px-4 py-2 text-base font-medium text-gray-700 hover:text-blue-600  dark:text-[#e0e0e0] dark:hover:text-[#c2df2b]'
                  )}
                >
                  Sign out
                </a>
              )}
            </Menu.Item>
          </Menu.Items>
        </Transition>
      </Menu>
    </>
  );
}
