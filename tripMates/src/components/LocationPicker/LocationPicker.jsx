import { useState, useRef, useEffect } from 'react';
import {
  MapContainer,
  Marker,
  Popup,
  TileLayer,
  useMapEvent,
  useMap,
} from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import SearchBar from './SearchBar';
import { Icon } from 'leaflet';
import './location.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRemove } from '@fortawesome/free-solid-svg-icons';

export default function LocationPicker({ onClose, onSave, defaultLocation }) {
  const [center, setCenter] = useState(defaultLocation);
  const [zoomLevel, setZoomLevel] = useState(13); // Current zoom level
  const [currentLocation, setCurrentLocation] = useState(center);

  const [isLocating, setIsLocating] = useState(true);

  const mapRef = useRef(null);
  const animateRef = useRef(false);

  useEffect(() => {
    const savedLocation = JSON.parse(localStorage.getItem('savedLocation'));
    console.log('Loaded Saved Location:', savedLocation);
    if (defaultLocation) {
      setCenter(defaultLocation);
      setCurrentLocation(defaultLocation);
    } else {
      {
        setCenter(savedLocation);
        setCurrentLocation(savedLocation);
      }
    }
  }, []);

  useEffect(() => {
    const savedLocation = JSON.parse(localStorage.getItem('savedLocation'));

    if (!savedLocation && isLocating) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const { latitude, longitude } = position.coords;
          setCenter({ lat: latitude, lng: longitude });
          setCurrentLocation({ lat: latitude, lng: longitude });
          setIsLocating(false);
        },
        (error) => {
          console.error('Error getting location:', error);
          setIsLocating(false);
        }
      );
    }
  }, [isLocating]);

  useEffect(() => {
    if (mapRef.current && currentLocation) {
      mapRef.current.setView(currentLocation, mapRef.current.getZoom());
    }
  }, [currentLocation]);

  animateRef.current = true;

  const handleLocationSelect = (location) => {
    setCenter(location);
    setCurrentLocation(location);
  };

  const handleLocateMeClick = () => {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        const { latitude, longitude } = position.coords;
        setCenter({ lat: latitude, lng: longitude });
        setCurrentLocation({ lat: latitude, lng: longitude });
        setIsLocating(false);
      },
      (error) => {
        console.error('Error getting location:', error);
        setIsLocating(false);
      }
    );
  };

  const handleSave = () => {
    onSave(currentLocation);
    localStorage.setItem('savedLocation', JSON.stringify(currentLocation));

    console.log('Saved Location:', currentLocation);
  };

  const handleCancel = () => {
    onClose();
  };
  const customIcon = new Icon({
    iconUrl: 'https://cdn-icons-png.flaticon.com/512/5048/5048308.png',
    iconSize: [50, 50],
  });

  const markers = [
    {
      key: 'marker1',
      position: [51.505, -0.09],
      content: 'My first popup',
    },
    {
      key: 'marker2',
      position: [51.51, -0.1],
      content: 'My second popup',
    },
    {
      key: 'marker3',
      position: [42, 24],
      content: 'My Home',
    },
  ];

  return (
    <div
      className='map-overlay'
      style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0 }}
    >
      <MapContainer
        attributionControl={false}
        key={`${center?.lat}-${center?.lng}`}
        center={center}
        zoom={zoomLevel}
        scrollWheelZoom={true}
        style={{ height: '100%', width: '100%' }}
        animate={true}
        ref={mapRef}
      >
        <TileLayer
          attribution='&copy; <a href="#">TripMates</a>'
          url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
        />

        <div className='leaflet-top leaflet-center '>
          <div className='searchContainer m-4 flex flex-row items-center'>
            <SearchBar
              onLocationSelect={handleLocationSelect}
              handleLocateMeClick={handleLocateMeClick}
            />
          </div>
        </div>

        <div className='leaflet-center leaflet-bottom  '>
          <div className='full-screen-buttons flex max-sm:flex-col gap-2'>
            <button
              className=' text-base mr-4 text-center  px-2 py-1 border border-transparent rounded-md shadow-sm  font-bold text-white dark:text-black  focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500  dark:focus:ring-[#9dad48] bg-blue-600 hover:bg-blue-700 dark:bg-[#c2df2b] dark:hover:bg-[#cee071]'
              onClick={handleSave}
            >
              Save
            </button>

            <button
              className=' text-base mr-4 text-center  px-2 py-1 border border-transparent rounded-md shadow-sm  font-bold text-white dark:text-black  focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500  dark:focus:ring-[#9dad48] bg-blue-600 hover:bg-blue-700 dark:bg-[#c2df2b] dark:hover:bg-[#cee071]'
              onClick={handleCancel}
            >
              <FontAwesomeIcon icon={faRemove} /> Remove
            </button>
          </div>
        </div>

        <Marker
          draggable={true}
          eventHandlers={{
            dragend: (e) => {
              setZoomLevel(e.target._map._zoom);
              setCurrentLocation(e.target.getLatLng());
            },
          }}
          position={currentLocation}
        >
          <Popup>
            {`Latitude: ${currentLocation?.lat?.toFixed(
              6
            )}, Longitude: ${currentLocation?.lng?.toFixed(6)}`}
          </Popup>
        </Marker>
        {markers.map((marker) => (
          <Marker key={marker.key} position={marker.position} icon={customIcon}>
            <Popup>{marker.content}</Popup>
          </Marker>
        ))}
      </MapContainer>
    </div>
  );
}
