import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapLocationDot } from '@fortawesome/free-solid-svg-icons';

function SearchBar({ onLocationSelect, handleLocateMeClick }) {
  const [searchText, setSearchText] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [selectedResultIndex, setSelectedResultIndex] = useState(-1);

  const handleSearch = async () => {
    if (searchText.trim() === '') return;

    try {
      const response = await axios.get(
        `https://nominatim.openstreetmap.org/search?q=${searchText}&format=json`
      );

      setSearchResults(response.data);
      setSelectedResultIndex(-1); // Reset selected result index
    } catch (error) {
      console.error('Error fetching search results:', error);
    }
  };

  const handleLocationSelect = (result) => {
    const { lat, lon } = result;
    onLocationSelect({ lat: parseFloat(lat), lng: parseFloat(lon) });
    setSearchText('');
    setSearchResults([]);
  };

  const handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      e.preventDefault();
      if (
        selectedResultIndex >= 0 &&
        selectedResultIndex < searchResults.length
      ) {
        handleLocationSelect(searchResults[selectedResultIndex]);
      } else {
        handleSearch();
      }
    }
  };

  useEffect(() => {
    const handleArrowNavigation = (e) => {
      if (e.key === 'ArrowDown') {
        setSelectedResultIndex((prevIndex) =>
          prevIndex < searchResults.length - 1 ? prevIndex + 1 : prevIndex
        );
      } else if (e.key === 'ArrowUp') {
        setSelectedResultIndex((prevIndex) =>
          prevIndex > 0 ? prevIndex - 1 : prevIndex
        );
      }
    };

    window.addEventListener('keydown', handleArrowNavigation);

    return () => {
      window.removeEventListener('keydown', handleArrowNavigation);
    };
  }, [selectedResultIndex, searchResults]);

  const handleInputClick = (e) => {
    e.stopPropagation(); // Prevent click event from propagating to parent elements
  };

  return (
    <div className='search-bar'>
      <div className='flex gap-4 items-center'>
        <button
          className='h-10 w-10 text-lg  text-center  border border-transparent rounded-full shadow-sm  font-bold text-white dark:text-black  focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500  dark:focus:ring-[#9dad48] bg-blue-600 hover:bg-blue-700 dark:bg-[#c2df2b] dark:hover:bg-[#cee071]'
          onClick={handleLocateMeClick}
        >
          <FontAwesomeIcon icon={faMapLocationDot} />
        </button>
        <input
          type='text'
          placeholder='Search for a city...'
          className='  max-w-lg bg-white border border-gray-300 rounded-md shadow-sm py-3 px-3 focus:outline-none focus:ring-gray-900 focus:border-gray-900 sm:text-sm'
          value={searchText}
          onChange={(e) => setSearchText(e.target.value)}
          onKeyDown={handleKeyPress}
          onClick={handleInputClick}
        />
        <button
          className='block h-10  text-center px-3 border border-transparent rounded-md shadow-sm text-sm  font-bold text-white dark:text-black  focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500  dark:focus:ring-[#9dad48] bg-blue-600 hover:bg-blue-700 dark:bg-[#c2df2b] dark:hover:bg-[#cee071]'
          onClick={handleSearch}
        >
          Search
        </button>
      </div>
      <ul className='search-results mt-1 block  max-w-lg bg-white  rounded-md shadow-sm  focus:outline-none focus:ring-gray-900 focus:border-gray-900 sm:text-sm'>
        {searchResults.map((result, index) => (
          <li
            key={result.place_id}
            className={index === selectedResultIndex ? 'selected' : ''}
            onMouseEnter={() => setSelectedResultIndex(index)}
            onClick={() => handleLocationSelect(result)}
          >
            {result.display_name}
          </li>
        ))}
      </ul>
    </div>
  );
}

export default SearchBar;
