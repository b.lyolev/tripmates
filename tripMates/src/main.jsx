import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.jsx';
import './index.css';
import { ChakraProvider } from '@chakra-ui/react';
import 'primereact/resources/themes/lara-light-indigo/theme.css';
import 'primereact/resources/primereact.min.css';
import { PrimeReactProvider } from 'primereact/api';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store/index.js';
import { extendTheme } from '@chakra-ui/react';
// import ScrollToTop from './components/ScrollToTop/ScrollToTop';

const theme = extendTheme({
  styles: {
    global: () => ({
      body: {
        background: '',
      },
    }),
  },
});

ReactDOM.createRoot(document.getElementById('root')).render(
  <PrimeReactProvider>
    <BrowserRouter>
      <ChakraProvider theme={theme}>
        <Provider store={store}>
          {/* <ScrollToTop/> */}
          <App />
        </Provider>
      </ChakraProvider>
    </BrowserRouter>
  </PrimeReactProvider>
);
