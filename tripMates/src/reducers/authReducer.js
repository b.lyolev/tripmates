const initialState = {
  user: null,
  userData: null,
  events: null,
  userRequests: null,
  userSentRequests: null,
  userFriends: null,
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_USER':
      return { ...state, user: action.payload };
    case 'SET_USER_DATA':
      return { ...state, userData: action.payload };
    case 'SET_USER_REQUESTS':
      return { ...state, userRequests: action.payload };
    case 'SET_USER_SENT_REQUESTS':
      return { ...state, userSentRequests: action.payload };
    case 'SET_USER_FRIENDS':
      return { ...state, userFriends: action.payload };
    case 'SET_ALL_EVENTS':
      return { ...state, events: action.payload };
    default:
      return state;
  }
};

export default authReducer;
