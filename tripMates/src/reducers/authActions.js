export const setUser = (user) => ({
  type: 'SET_USER',
  payload: user ? serializeUser(user) : null,
});
export const setUserData = (userData) => ({
  type: 'SET_USER_DATA',
  payload: userData,
});

export const setUserRequests = (requests) => ({
  type: 'SET_USER_REQUESTS',
  payload: requests,
});

export const setUserSentRequests = (requests) => ({
  type: 'SET_USER_SENT_REQUESTS',
  payload: requests,
});

export const setUserFriends = (friends) => ({
  type: 'SET_USER_FRIENDS',
  payload: friends,
});
function serializeUser(user) {
  return {
    uid: user.uid,
    displayName: user.displayName,
    email: user.email,
    photoURL: user.photoURL,
    createdOn: user?.metadata?.createdAt,
    creationTime: user?.metadata?.creationTime,
    lastLoginTime: user?.metadata?.lastLoginAt,
  };
}
