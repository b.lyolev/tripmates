const initialState = new Date().getTime(); // Initial state can be the current date

export const currentWeekReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_CURRENT_WEEK':
      return action.payload;
    default:
      return state;
  }
};
