export const setSelectedDate = (date) => {
  return {
    type: 'SET_SELECTED_DATE',
    payload: date,
  };
};
