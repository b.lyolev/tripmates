const initialState = {
  selectedView: 'Week View', // Set an initial selected view
};

const viewsReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_SELECTED_VIEW':
      return {
        ...state,
        selectedView: action.payload,
      };
    default:
      return state;
  }
};

export default viewsReducer;
