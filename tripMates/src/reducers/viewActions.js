export const setSelectedView = (view) => ({
  type: 'SET_SELECTED_VIEW',
  payload: view,
});
