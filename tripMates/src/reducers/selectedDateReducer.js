const day = new Date();
day.setHours(0, 0, 0, 0);
day.setDate(day.getDate() + 1);
day.setHours(day.getHours() - 3);
const initialState = day.getTime();

const selectedDateReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_SELECTED_DATE':
      return action.payload; // The payload is now a serialized date string
    default:
      return state;
  }
};

export default selectedDateReducer;
