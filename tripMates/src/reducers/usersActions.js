export const setAllUsers = (allUsers) => ({
  type: 'SET_ALL_USERS',
  payload: allUsers,
});
