export const setCurrentWeek = (week) => ({
  type: 'SET_CURRENT_WEEK',
  payload: week,
});
