import { createSlice } from '@reduxjs/toolkit';

const filtersSlice = createSlice({
  name: 'filters',
  initialState: {
    category: {
      hotels: false,
      villas: false,
      cottages: false,
      apartments: false,
      resorts: false,
    },
  },
  reducers: {
    setSelectedFilters: (state, action) => {
      const { sectionId, optionValue, isSelected } = action.payload;

      if (!state[sectionId]) {
        state[sectionId] = {};
      }

      state[sectionId][optionValue] = isSelected;
    },
  },
});

export const { setSelectedFilters } = filtersSlice.actions;
// export default filtersSlice.reducer;

const priceRangeSlice = createSlice({
  name: 'priceRange',
  initialState: [0, 5000],
  reducers: {
    setPriceRange: (state, action) => {
      return action.payload;
    },
  },
});

export const { setPriceRange } = priceRangeSlice.actions;

const sortSlice = createSlice({
  name: 'sort',
  initialState: 'Best Rating', // Set the initial sort option
  reducers: {
    setSelectedSortOption: (state, action) => {
      return action.payload;
    },
  },
});

export const { setSelectedSortOption } = sortSlice.actions;
export const filtersReducer = filtersSlice.reducer;
export const priceRangeReducer = priceRangeSlice.reducer;
export const sortReducer = sortSlice.reducer;
