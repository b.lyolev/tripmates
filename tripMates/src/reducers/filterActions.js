export const SET_SELECTED_OPTION = 'SET_SELECTED_OPTION';

export const setSelectedOption = (sectionId, optionValue, isSelected) => ({
  type: SET_SELECTED_OPTION,
  payload: { sectionId, optionValue, isSelected },
});
