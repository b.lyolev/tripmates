export const setAllEvents = (allEvents) => ({
  type: 'SET_ALL_EVENTS',
  payload: allEvents,
});
