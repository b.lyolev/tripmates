const colVariants = {
  1: 'sm:col-start-1',
  2: 'sm:col-start-2',
  3: 'sm:col-start-3',
  4: 'sm:col-start-4',
  5: 'sm:col-start-5',
  6: 'sm:col-start-6',
};

export const colors = [
  {
    name: 'pink',
    bgColor: 'bg-pink-200',
    hoverColor: 'hover:bg-pink-400',
    border: 'border-pink-500',
    ringColor: 'ring-pink-400',
  },
  {
    name: 'red',
    bgColor: 'bg-red-200',
    hoverColor: 'hover:bg-red-400',
    border: 'border-red-500',
    ringColor: 'ring-red-400',
  },
  {
    name: 'orange',
    bgColor: 'bg-orange-200',
    hoverColor: 'hover:bg-orange-400',
    border: 'border-orange-500',
    ringColor: 'ring-orange-400',
  },
  {
    name: 'green',
    bgColor: 'bg-green-200',
    hoverColor: 'hover:bg-green-400',
    border: 'border-green-500',
    ringColor: 'ring-green-400',
  },
  {
    name: 'blue',
    bgColor: 'bg-blue-200',
    hoverColor: 'hover:bg-blue-400',
    border: 'border-blue-500',
    ringColor: 'ring-blue-400',
  },
  {
    name: 'indigo',
    bgColor: 'bg-indigo-200',
    hoverColor: 'hover:bg-indigo-400',
    border: 'border-indigo-500',
    ringColor: 'ring-indigo-400',
  },
];
