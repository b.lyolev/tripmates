// options: [
//   { value: 'hotels', label: 'Hotels' },
//   { value: 'villas', label: 'Villas' },
//   { value: 'cottages', label: 'Cottages' },
//   { value: 'apartments', label: 'Apartments' },
//   { value: 'resorts', label: 'Resorts' },
// ],

export const hotels = [
  {
    id: 17459632,
    name: 'Minoas Sea Villa Heated Pool',
    category: 'villas',
    price: 674,
    location: 'Chania, Greece',
    geoLocation: {
      lat: 35.516239,
      lng: 24.01881,
    },
    images: [
      {
        id: '1',
        image:
          'https://a0.muscache.com/im/pictures/8bd84f5a-b13f-45e0-95e3-6f0601b9f163.jpg?im_w=1200',
      },
      {
        id: '2',
        image:
          'https://a0.muscache.com/im/pictures/d8c824ac-cce3-49ef-9554-2a77e7fe868f.jpg?im_w=1440',
      },
      {
        id: '3',
        image:
          'https://a0.muscache.com/im/pictures/30c1b2de-3cb5-45d2-838d-abd45c4226a9.jpg?im_w=1440',
      },
      {
        id: '4',
        image:
          'https://a0.muscache.com/im/pictures/f708aaf4-ff55-4421-bbaa-6bb9e2f936d0.jpg?im_w=1440',
      },
      {
        id: '5',
        image:
          'https://a0.muscache.com/im/pictures/50f3b33c-629d-40c1-8182-704d7f48c742.jpg?im_w=1440',
      },
      {
        id: '6',
        image:
          'https://a0.muscache.com/im/pictures/e7968d26-4316-4b70-8777-120b6fec3a2d.jpg?im_w=1440',
      },
    ],
    description:
      'Nuzzled in a gorgeous setting, above the sun kissed beach of Georgioupolis in Crete, Minoas Villas captures the essence of chic and luxurious living only few minutes away from the shimmering sea. Built amphitheatrically with direct sea views, this exceptional built Villas, member of the HotelPraxis Group evokes the island’s Cretan spirit and cosmopolitan charm in its elegant environs while ensuring a holiday beyond comparison.',
    rating: 4.7,
    reviews: 15,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],

    host: {
      name: 'Minoas',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 11269,
  },

  {
    id: 21547896,
    name: 'La Stalla - Casa San Gabriel',
    category: 'cottages',
    price: 235,
    location: 'Pierantonio, Umbria, Italy',
    geoLocation: {
      lat: 43.265,
      lng: 12.39281,
    },
    images: [
      {
        id: '1',
        image:
          'https://a0.muscache.com/im/pictures/2d502704-5db1-47c0-bfee-3816d04743fb.jpg?im_w=1200',
      },
      {
        id: '2',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-2922931/original/e2e5bb39-d1b5-4055-ba63-05b33bef0f1d.jpeg?im_w=1440',
      },
      {
        id: '3',
        image:
          'https://a0.muscache.com/im/pictures/7fe8183d-db7c-4f03-8d33-cbdae6266105.jpg?im_w=1440',
      },
      {
        id: '4',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-2922931/original/d94521c6-c92d-4584-abf4-6697269e627d.jpeg?im_w=1440',
      },
      {
        id: '5',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-2922931/original/9d2d6db7-eb9d-46ca-abfd-fee639c5053e.jpeg?im_w=1440',
      },
      {
        id: '6',
        image:
          'https://a0.muscache.com/im/pictures/094fc444-4b11-46e2-bbdb-0d74ce12167d.jpg?im_w=1440',
      },
    ],
    description: `
Set in the hills overlooking a beautiful Umbrian Valley, with uninterrupted views as far as Assisi, Casa San Gabriel is a collection of 4 farmhouses dating back to the 16th Century. Lovingly restored by the present owners, the houses feature original beams, terracotta tiles and the latest technology giving ultimate comfort.

It's position set among 3 acres of its own land and surrounded by olive groves and wooded hillsides, gives guests the utmost peace and tranquillity to relax and unwind and the opportunity to walk in the beautiful Umbrian countryside.

There is accommodation for 10 in 3 totally independent cottages (2,4,4), all with private terraces, fully equipped kitchen, sitting / dinning room, central heating, bathroom with shower and wifi access.

The main house offers a communal room with a well stocked library including books, guidebooks and dvds. Guests can enjoy the 14.5m * 4.5m in-ground pool, a wood-burning pizza oven and herb garden.

Swimming Pool: The 14.5m x 4.5m swimming pool is situated on a terrace, with sun beds and umbrellas, at the top of the property with magnificent views across the valley.

The south facing aspect ensures the area takes advantage of the sun throughout the day and its elevated position often brings a cooling breeze for a welcome relief to the heat of the summer.

It is the perfect place to enjoy a cool glass of 'Vino Bianco' and watch the evening sun set across the mountains.

The whole area has been fenced to provide security for children but in such a way that this does not affect the stunning views from the swimming pool.`,
    rating: 4.8,
    reviews: 5,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Pets allowed',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],

    host: {
      name: 'Gabriel',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 13783,
  },
  {
    id: 33982475,
    name: "Sully's Big bear Lodge",
    category: 'cottages',
    price: 750,
    location: 'Sevierville, Tennessee, US',
    geoLocation: {
      lat: 35.86838,
      lng: -83.56298,
    },
    images: [
      {
        id: '1',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-698416417074337858/original/aa069757-92fa-461a-91f1-d50e28574c60.jpeg?im_w=1200',
      },
      {
        id: '2',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-698416417074337858/original/c9ad959d-2dd5-4580-8871-6bff69c32aa7.jpeg?im_w=1440',
      },
      {
        id: '3',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-698416417074337858/original/81f06727-bc8c-4b65-892f-84dafb8ad8cb.jpeg?im_w=1440',
      },
      {
        id: '4',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-698416417074337858/original/89c834e6-8b23-4b04-b17a-f68c49f1f886.jpeg?im_w=1440',
      },
      {
        id: '5',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-698416417074337858/original/951e73e1-2fd6-4d71-9631-e2d9f56bd3d3.jpeg?im_w=1440',
      },
      {
        id: '6',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-698416417074337858/original/a398b390-57fe-4c88-9d07-dfcc9af0ba17.jpeg?im_w=1440',
      },
      {
        id: '7',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-698416417074337858/original/9249cd2a-fc2b-4fd0-89fa-809c8c254d53.jpeg?im_w=1440',
      },
    ],
    description: `Sully's Big bear Lodge is located in the heart of Sevierville, off Wears Valley Rd Tennessee, it is a Must-see luxurious new million-dollar cabin! cabin finished in August 2022. This cabin is 2000 square feet, 3 bedroom- 2 1/2 bath that sits on a secluded, peaceful, tranquil catch & release fishing pond with 2 outdoor picnic and fire pit area's a jacuzzi on back deck facing pond. The huge loft has a $15.000 Brunswick Monticello Pool Table for the serious players and a 1000 game arcade console.
      The space
      3 bedrooms, 2 bedrooms have king beds, and the 3rd bedroom has a queen bed. Sleeper/sofa in living room has an orthopedic mattress. The master bedroom has a full-size Walkin closet with full-size body mirror. can easily store several suitcases in this closet. full size mirrors in all bedrooms.`,
    rating: 4.9,
    reviews: 23,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],
    host: {
      name: 'Sully',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 10083,
  },
  {
    id: 48234519,
    name: 'Veluvana Bali - Scorpio House',
    category: 'villas',
    price: 1283,
    location: 'Ubud, Bali, Indonesia',
    geoLocation: {
      lat: -8.45523,
      lng: 115.285,
    },
    images: [
      {
        id: '1',
        image:
          'https://cf.bstatic.com/xdata/images/hotel/max1024x768/264072338.jpg?k=a45c827e9aa35d05432bb79220fd31187cc1b092a5763ebf537de4c20b4533d3&o=&hp=1',
      },
      {
        id: '2',
        image:
          'https://cf.bstatic.com/xdata/images/hotel/max1024x768/333042163.jpg?k=498ff0f801d07fa822dd9edb72caf76f93acb3d3a6a97e0e0241e439069c46ca&o=&hp=1',
      },
      {
        id: '3',
        image:
          'https://cf.bstatic.com/xdata/images/hotel/max1024x768/333042051.jpg?k=31b98ea7dc97243577c71ed0a4dbe68dd3d0d8fc46c1c4b1d25f7f57821dcc8c&o=&hp=1',
      },
      {
        id: '4',
        image:
          'https://cf.bstatic.com/xdata/images/hotel/max1024x768/333042055.jpg?k=ca9a17298425b6e072683bb373faa44a35b9d8e4c63ffc83eed939f7bf56e859&o=&hp=1',
      },
      {
        id: '5',
        image:
          'https://cf.bstatic.com/xdata/images/hotel/max1024x768/274880851.jpg?k=2c78e8bc3ca8e8c1a08206aac2522b2c49b72dfd9e6a3bc13bcd73da0390c63e&o=&hp=1',
      },
      {
        id: '6',
        image:
          'https://cf.bstatic.com/xdata/images/hotel/max1024x768/274880431.jpg?k=643eb79ef4799e4e404342817d266f3d54ee7175b5b39d23833132a574e3d5b5&o=&hp=1',
      },
    ],
    description: `
Veluvana is a unique bamboo house with magical Mount Agung and the lush Sidemen Valley. Each house is inspired by various animals’ structure as part of the creatures in the world and takes advantage of the open space concept which provides a deep connection with nature that aim to give positive energy to recover your soul. From the concept to material selection, we are trying to contribute to the sustainability principle

The space
Scorpio House welcomes you with mysterious beauty through a giant and iconic design where you will be surprised by the hidden ground pool beneath the main bedroom as you walk down the staircase. Take a moment to walk or hang along the iconic and sharp pool edge, then indulge in a floating breakfast in the morning while taking in the view in restful contemplation with the sounds of nature.

`,
    rating: 4.9,
    reviews: 63,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],
    host: {
      name: 'Veluvana',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 383,
  },
  {
    id: 52369874,
    name: 'Dubai Burj Al Arab',
    category: 'apartments',
    price: 3345,
    location: 'Dubai, United Arab Emirates',
    geoLocation: {
      lat: 25.192291259765625,
      lng: 55.28144836425781,
    },
    images: [
      {
        id: '1',
        image:
          'https://a0.muscache.com/im/pictures/a6ab0929-fce2-49d2-886a-5f3b348a4700.jpg?im_w=1200',
      },
      {
        id: '2',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-627358894389945121/original/bd856723-b4ed-42b7-b76a-15172a0193c9.jpeg?im_w=1440',
      },
      {
        id: '3',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-627358894389945121/original/c54daf73-9093-44f3-85c3-85f71653cc3c.jpeg?im_w=1440',
      },
      {
        id: '4',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-627358894389945121/original/3ebf9929-6c22-4e50-9785-e78bb897b6f1.jpeg?im_w=1440',
      },
      {
        id: '5',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-627358894389945121/original/bee0f479-10b5-4a0e-be50-d38f2e1e1cdf.jpeg?im_w=1440',
      },
      {
        id: '6',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-627358894389945121/original/12cdea21-6b54-4a89-a2d0-84b57ba697c6.jpeg?im_w=1440',
      },
      {
        id: '7',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-627358894389945121/original/e551932c-60d9-4bef-85bc-c3d12b2f66af.jpeg?im_w=1440',
      },
    ],
    description: `
This apartment offers a direct view of the world-famous Burj Khalifa and dancing fountains from the balcony.

It has direct access to Burj Park, two swimming pools, and a gym.
    
The facilities include a fitness center, kids' area, squash courts, and more.
    
Step inside and enjoy the refreshing color palette that energizes and rejuvenates.
    
We prioritize your privacy and security with video intercom access, a pool, gym, and 24-hour security.
    
Use your key card to access private parking and take the elevator straight to your apartment.
    
Our team will contact you one day before departure to facilitate the check-out process.
    
Experience luxury in the heart of Dubai!
`,

    rating: 4.9,
    reviews: 106,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],
    host: {
      name: 'Burj Al Arab',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 6083,
  },

  {
    id: 64617283,
    name: 'Park & Spa Hotel Markovo',
    category: 'hotels',
    price: 185,
    location: 'Markovo, Plovdiv, Bulgaria',
    geoLocation: {
      lat: 42.1275,
      lng: 24.7935,
    },
    images: [
      {
        id: '1',
        image:
          'https://hotelmarkovo.bg/wp-content/uploads/2022/10/%D0%9F%D0%B0%D1%80%D0%BA-%D0%B8-%D0%A1%D0%9F%D0%90-%D0%A5%D0%BE%D1%82%D0%B5%D0%BB-%D0%9C%D0%B0%D1%80%D0%BA%D0%BE%D0%B2%D0%BE-%D0%B4%D0%BE-%D0%9F%D0%BB%D0%BE%D0%B2%D0%B4%D0%B8%D0%B2-1030x686.jpg',
      },
      {
        id: '2',
        image:
          'https://hotelmarkovo.bg/wp-content/uploads/2022/05/DJI_0690-1030x686.jpg',
      },
      {
        id: '3',
        image:
          'https://hotelmarkovo.bg/wp-content/uploads/2022/10/%D0%9F%D0%B0%D1%80%D0%BA-%D0%92%D1%8A%D0%B6%D0%B5%D0%BB%D0%B0%D0%BD%D0%B4%D0%B8%D1%8F-%D0%B7%D0%B0%D0%BB%D0%B5%D0%B7-%D0%A5%D0%BE%D1%82%D0%B5%D0%BB-%D0%9C%D0%B0%D1%80%D0%BA%D0%BE%D0%B2%D0%BE-1030x686.jpg',
      },
      {
        id: '4',
        image:
          'https://hotelmarkovo.bg/wp-content/uploads/2020/07/%D0%94%D0%B5%D0%BB%D1%83%D0%BA%D1%81-%D0%A1%D1%82%D1%83%D0%B4%D0%B8%D0%BE-1030x686.jpg',
      },
      {
        id: '5',
        image:
          'https://hotelmarkovo.bg/wp-content/uploads/2020/07/%D0%92%D0%98%D0%9F-%D0%B0%D0%BF%D0%B0%D1%80%D1%82%D0%B0%D0%BC%D0%B5%D0%BD%D1%82-%D0%9F%D0%B0%D1%80%D0%BA-%D0%B8-%D0%A1%D0%9F%D0%90-%D0%A5%D0%BE%D1%82%D0%B5%D0%BB-%D0%9C%D0%B0%D1%80%D0%BA%D0%BE%D0%B2%D0%BE-1030x686.jpg',
      },
      {
        id: '6',
        image:
          'https://hotelmarkovo.bg/wp-content/uploads/2021/05/%D0%92%D1%8A%D1%82%D1%80%D0%B5%D1%88%D0%BD%D0%B8-%D0%B1%D0%B0%D1%81%D0%B5%D0%B9%D0%BD%D0%B8-%D0%B3%D0%B0%D0%BB%D0%B5%D1%80%D0%B8%D1%8F.jpg',
      },
      {
        id: '7',
        image:
          'https://hotelmarkovo.bg/wp-content/uploads/2021/05/%D0%94%D0%B6%D0%B0%D0%BA%D1%83%D0%B7%D0%B8-%D0%BD%D0%B0-%D0%BE%D1%82%D0%BA%D1%80%D0%B8%D1%82%D0%BE-%D0%B3%D0%B0%D0%BB%D0%B5%D1%80%D0%B8%D1%8F.jpg',
      },
    ],
    description: `
Park and Spa Hotel Markovo is located in a quiet place in Markovo, a residential area in Plovdiv. Guests can admire the beautiful view of the Rhodopes or the seven hills in the city from their rooms. Free private parking is available on site.

Each room offers free WiFi, air conditioning, a flat-screen TV with cable channels, a sofa, a desk, a telephone and a refrigerator. The bathroom in each is equipped with a shower, hairdryer and free toiletries.

The spa area at the Markovo Hotel has an outdoor swimming pool in the summer, a Finnish sauna, an infrared sauna, a herbal sauna, an outdoor hydromassage bath, a cold room, a steam bath and a relaxation area. There are 4 indoor heated swimming pools on site that guests can use free of charge. Facilities also include a gym and tennis court. Massage procedures are organized upon request.

European cuisine and a selection of fine wines are served in the bar and restaurant of Park and Spa Hotel Markovo. The facilities are complemented by a panoramic terrace overlooking the city and a children's playground. Guests can use meeting rooms, as well as transfer services upon request and at an additional cost.
`,
    rating: 4.9,
    reviews: 76,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],
    host: {
      name: 'Park & Spa',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 11673,
  },
  {
    id: 17459650,
    name: 'Entire villa hosted by Mega',
    category: 'villas',

    price: 211,
    location: 'Ubud, Bali',
    geoLocation: {
      lat: -8.519268,
      lng: 115.263298,
    },
    images: [
      {
        id: '1',
        image:
          'https://a0.muscache.com/im/pictures/11dec279-3c90-4d6f-83f3-8fc8995ae28e.jpg?im_w=1200',
      },
      {
        id: '2',
        image:
          'https://a0.muscache.com/im/pictures/39794e4d-7ff1-4a20-9883-f681e07c931a.jpg?im_w=1200',
      },
      {
        id: '3',
        image:
          'https://a0.muscache.com/im/pictures/307e8251-b9c7-4cd3-99dc-8a4be2f49826.jpg?im_w=1200',
      },
      {
        id: '4',
        image:
          'https://a0.muscache.com/im/pictures/da2818d8-1b7b-4bff-855f-75f806076c85.jpg?im_w=1200',
      },
      {
        id: '5',
        image:
          'https://a0.muscache.com/im/pictures/6943ce46-c64f-43b4-824b-5551735befcc.jpg?im_w=1200',
      },
      {
        id: '6',
        image:
          'https://a0.muscache.com/im/pictures/6d0d8eb8-4544-4ce2-89d5-3d07fe6e318a.jpg?im_w=1200',
      },
    ],
    description:
      'Are you bored and tired of quarantine and longing for a new place and a new atmosphere to just break away for a couple of days, week or month? the megananda has the answer, Our private pool villa has a spectacular Sunset Private Infinity Pool overlooking to the green rice field view,Perfect combination of the comfort of modern living and the exotic of tropical living with touches of Balinese art philosophy, Its dedicated for the one who appreciate quality time and love to blend with nature.',
    rating: 4.6,
    reviews: 15,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],

    host: {
      name: 'Mega',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 11370,
  },

  {
    id: 21545317,
    name: 'Blue Oasis',
    category: 'hotels',
    price: 4574,
    location: 'Punta Cana, La Altagracia, Dominican Republic',
    geoLocation: {
      lat: 18.58201,
      lng: -68.405473,
    },
    images: [
      {
        id: '1',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-777820817491139442/original/8a522a57-904a-4f6d-bec3-e36fd76191dd.jpeg?im_w=1200',
      },
      {
        id: '2',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-777820817491139442/original/55e48a6f-021a-4e4b-9147-01b5c9888fb0.jpeg?im_w=1200',
      },
      {
        id: '3',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-777820817491139442/original/b6704a06-8929-4fd8-84e6-a2ede07742eb.jpeg?im_w=1200',
      },
      {
        id: '4',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-777820817491139442/original/6616a440-b19c-4a15-9c2f-5cc8d91ea142.jpeg?im_w=1200',
      },
      {
        id: '5',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-777820817491139442/original/79e81198-25d7-4a7b-a6f4-21c90f952278.jpeg?im_w=1200',
      },
      {
        id: '6',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-777820817491139442/original/b0d8ddfa-338e-4c95-b36c-afb8960192bc.jpeg?im_w=1200',
      },
    ],
    description: `The space

Nestled in the Cap Cana gated village, this incredible villa enjoys endless ocean views from its beachside location just south of the bustling marina. Step outside to the gorgeous pool and hot tub. Beyond, only a line of palm trees separates you from the soothing sands of your private beach. These water are perfect for first-time snorkelers and paddleboarders if you're feeling inspired.

BEDROOM & BATHROOM
• Bedroom 1 - Primary: King size bed, Ensuite bathroom with stand-alone shower and jetted tub, Balcony, Ocean view
• Bedroom 2: King size bed, Ensuite bathroom with stand-alone shower and jetted tub, Television, Balcony
• Bedroom 3: King size bed, Ensuite bathroom with stand-alone shower, Balcony
• Bedroom 4: 2 Queen size beds, Ensuite bathroom with stand-alone shower and bathtub, Television, Balcony
• Bedroom 5: 2 Queen size beds, Ensuite bathroom with stand-alone shower and bathtub, Television, Balcony
• Bedroom 6: King size bed, Ensuite bathroom with stand-alone shower, Television, Ocean view
• Bedroom 7: Queen size bed, Ensuite bathroom with stand-alone shower, Television, Ocean view

OUTDOOR FEATURES
• Terrace
    `,
    rating: 4.9,
    reviews: 30,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Pets allowed',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],

    host: {
      name: 'Gabriel',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 10783,
  },
  {
    id: 3398432,
    name: 'Villa Amylia',
    category: 'resorts',
    price: 4509,
    location: 'Koh Samui, Surat Thani, Thailand',
    geoLocation: {
      lat: 35.86838,
      lng: -83.56298,
    },
    images: [
      {
        id: '1',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-820954888871612167/original/f37e01a9-cb1a-446d-be7e-c1d62d1f1c23.jpeg?im_w=1200',
      },
      {
        id: '2',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-820954888871612167/original/f7b1e303-27b4-40ab-b85c-5fdd5ce727f9.jpeg?im_w=1200',
      },
      {
        id: '3',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-820954888871612167/original/0c084878-55bf-414c-aa5b-87c87c9d8781.jpeg?im_w=1200',
      },
      {
        id: '4',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-820954888871612167/original/ca4bbb29-5436-43d9-9911-a7a76f355774.jpeg?im_w=1200',
      },
      {
        id: '5',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-820954888871612167/original/221c51a2-fb0b-4b98-9342-0fc69834ffd0.jpeg?im_w=1200',
      },
      {
        id: '6',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-820954888871612167/original/fa8eef0d-db50-4cb7-85de-3ac95b6a0a0b.jpeg?im_w=1200',
      },
      {
        id: '7',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-820954888871612167/original/dcda6411-bb91-4f38-b539-797e2f4378fc.jpeg?im_w=1200',
      },
    ],
    description: `
Consisting of Villa Ruby and Villa Emerald, this beautiful vacation rental is perfect for large group getaways in Northern Chaweng. Located in the Narayan Heights neighborhood, Villa Amylia captures scenic vistas of the ocean, islands, and mountains. The villa is just 250m from the beach and has 2 swimming pools, space for 20, and a rooftop terrace that can host up to 40 guests.

The space

BEDROOM & BATHROOM

• Bedroom 1 - Primary: King size bed, Ensuite bathroom with stand-alone shower, Television, Sea view

• Bedroom 2: King size bed, Ensuite bathroom with stand-alone shower, Television, Balcony, Sea view

• Bedroom 3: King size bed, Ensuite bathroom with stand-alone shower, Television, Sea view

• Bedroom 4: 2 Twin size beds (can be converted to a king), Ensuite bathroom with stand-alone shower, Television, Sea view

• Bedroom 5: King size bed, Ensuite bathroom with stand-alone shower, Television, Terrace

• Bedroom 6: Queen size bed, Ensuite bathroom with stand-alone shower, Television, Sea view

• Bedroom 7: Queen size bed, Ensuite bathroom with stand-alone shower, Television, Sea view

• Bedroom 8: King size bed, Ensuite bathroom with stand-alone shower, Television, Sea view

• Bedroom 9: 1 upper bunk bed (4 m long-can sleep 2 children) and 1 oversized sofa (convertible to a 3x2 m bed - can sleep 2-4 young children), Ensuite bathroom with stand-alone shower, Television

FEATURES & AMENITIES

• Wine cooler`,
    rating: 4.7,
    reviews: 257,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],
    host: {
      name: 'Sully',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 4011,
  },
  {
    id: 48230051,
    name: 'Villa Always',
    category: 'villas',
    price: 2993,
    location: 'Les Terres Basses, Collectivité de Saint-Martin, St. Martin',
    geoLocation: {
      lat: 45.56407,
      lng: -73.481105,
    },
    images: [
      {
        id: '1',
        image:
          'https://a0.muscache.com/im/pictures/monet/Luxury-570972357022815576/original/893222cb-b63e-46f3-bae2-bf89472601ea?im_w=1200',
      },
      {
        id: '2',
        image:
          'https://a0.muscache.com/im/pictures/monet/Luxury-570972357022815576/original/cb549654-747c-4b08-9569-c07ce435ff9b?im_w=1200',
      },
      {
        id: '3',
        image:
          'https://a0.muscache.com/im/pictures/monet/Luxury-570972357022815576/original/99638847-297d-47d3-bc79-329710c85adc?im_w=1200',
      },
      {
        id: '4',
        image:
          'https://a0.muscache.com/im/pictures/monet/Luxury-570972357022815576/original/ae9a722f-9270-4866-8585-1c77796b23af?im_w=1200',
      },
      {
        id: '5',
        image:
          'https://a0.muscache.com/im/pictures/monet/Luxury-570972357022815576/original/8a4e273d-22a2-4e66-b428-afc2922ee1d0?im_w=720',
      },
      {
        id: '6',
        image:
          'https://a0.muscache.com/im/pictures/monet/Luxury-570972357022815576/original/e5721336-f504-4819-965b-3d9d1e30702b?im_w=1200',
      },
    ],
    description: `
  The space

  Gingerbread trim and blooming bougainvillea sweeten the deal at this bright, breezy St Martin home. The infinity pool points toward ocean views and is framed by loungers and a dining gazebo. Open-sided walls let breezes waft through open-concept living and dining areas and over trad pieces like roll-arm sofas. Drive 5 minutes to the beach or pop to the Sint Maarten side for restaurants and bars.
    
    `,
    rating: 4.8,
    reviews: 24,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],
    host: {
      name: 'Valiana',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 7083,
  },
  {
    id: 52369571,
    name: 'DTreehouse Goldsworthy',
    category: 'apartments',

    price: 3345,
    location: 'Halle, Gelderland, Netherlands',
    geoLocation: {
      lat: 52,
      lng: 5.83333,
    },
    images: [
      {
        id: '1',
        image:
          'https://a0.muscache.com/im/pictures/badc352d-e7f7-4c6e-b96b-e20f9bae9a08.jpg?im_w=960',
      },
      {
        id: '2',
        image:
          'https://a0.muscache.com/im/pictures/4fe4330e-5c26-4649-b675-615d495c22f1.jpg?im_w=1200',
      },
      {
        id: '3',
        image:
          'https://a0.muscache.com/im/pictures/badc352d-e7f7-4c6e-b96b-e20f9bae9a08.jpg?im_w=1200',
      },
      {
        id: '4',
        image:
          'https://a0.muscache.com/im/pictures/bec7bebf-4256-41bf-8e72-7fa5a4506d52.jpg?im_w=1200',
      },
      {
        id: '5',
        image:
          'https://a0.muscache.com/im/pictures/cc7fcf55-5055-47f2-b986-33ff65235d02.jpg?im_w=720',
      },
      {
        id: '6',
        image:
          'https://a0.muscache.com/im/pictures/63ec4e07-efa6-445b-b823-14507b9a1051.jpg?im_w=1200',
      },
      {
        id: '7',
        image:
          'https://a0.muscache.com/im/pictures/9b1c9a54-9ea4-47b9-b634-05c4e240b023.jpg?im_w=1200',
      },
    ],
    description: `
When you stay in a tree, you become part of the tree's life for a while. You feel very strongly the rhythm of day and night in nature. You will calm down if you move along at this rhythm. The birds ringing the day and the birds ringing the night. You can see the landscape from a higher perspective. You look at the world a different way, from above. Sleeping in a tree, literally sleeping between the branches is an indescribable experience.

The space

This space is unique, it's a unique sleeping experience. It is a Tiny house with 2 floors in an old oak. The tree house hangs in the tree like a cocoon with a structure attached to the trunk. The trunk is the center of the treehouse, its branches run right through the house. You come in with a staircase, once you're inside you can pick up the stairs with a rope. One side of the house has large windows so you can enjoy stunning views. On this 1st floor there is a kitchen, bathroom, dining area and sitting area with wood-burning stove on this floor. Climbing a super steep staircase brings you to the meadow. The loft is half of the space and has a double bed, stargazer and toilet.
    `,
    rating: 4.5,
    reviews: 100,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],
    host: {
      name: 'Haga',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 1570,
  },

  {
    id: 64617111,
    name: 'Le Ti',
    category: 'hotels',
    price: 3390,
    location: 'Bagnes, Valais, Switzerland',
    geoLocation: {
      lat: 46.0099809,
      lng: 7.3070055,
    },
    images: [
      {
        id: '1',
        image:
          'https://a0.muscache.com/im/pictures/ce30750b-0b91-4093-9763-aa942b5b672e.jpg?im_w=1200',
      },
      {
        id: '2',
        image:
          'https://a0.muscache.com/im/pictures/07293c96-e838-4291-ac93-a1dcb02fe704.jpg?im_w=1200',
      },
      {
        id: '3',
        image:
          'https://a0.muscache.com/im/pictures/2ab91d03-69b7-4e5c-b782-9132fa175ad3.jpg?im_w=1200',
      },
      {
        id: '4',
        image:
          'https://a0.muscache.com/im/pictures/e9bdb4e8-f0fb-49eb-8097-17c97ef09d16.jpg?im_w=720',
      },
      {
        id: '5',
        image:
          'https://a0.muscache.com/im/pictures/c1e6d3cf-ccd0-4eab-8754-e1299c8ab38c.jpg?im_w=1200',
      },
      {
        id: '6',
        image:
          'https://a0.muscache.com/im/pictures/3b195fed-d078-4da1-9629-e3ddd3ccc849.jpg?im_w=1200',
      },
      {
        id: '7',
        image:
          'https://a0.muscache.com/im/pictures/b75d59b0-9227-4d3a-a32e-0034b2247297.jpg?im_w=1200',
      },
    ],
    description: `
Le Ti chalet is a fine example of what happens when a keen sensibility for a traditional alpine home comes into balance with the luxury of high-end amenities in one of the most exclusive areas in the Swiss Alps. This private vacation rental is perfectly positioned on Chemin de Clambin, a short distance from the Médran and Savoleyres lift stations, and a short walk from town. Le Ti represents an ideal base for a family holiday or a group of friends looking to experience the best of Verbier.

After a fun-filled day on the mountain, you can relax in the soothing bubbles of the outdoor hot tub with a majestic view of the snow-capped mountains in the distance. The grounds also feature a barbeque, a balcony and an outdoor fireplace. Inside, you will find Wi-Fi and a convenient mud room. The consummate entertainer will be at home in no time with a wet bar, a sound system and a green felt billiard table. Your reservation includes housekeeping and lift pass delivery service. Feel free to ask our dedicated concierge about the fully catered option with daily breakfasts, chef services, ski instruction and 24-hour driving service.
    `,
    rating: 4.9,
    reviews: 156,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],
    host: {
      name: 'Gutierez',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 3512,
  },
  {
    id: 17459411,
    name: 'Lime Tree House',
    category: 'cottages',
    price: 911,
    location: 'Barbados, Saint James',
    geoLocation: {
      lat: 13.1861111,
      lng: -59.6377778,
    },
    images: [
      {
        id: '1',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-826800524178117410/original/3ed86cb2-4019-48d7-9b85-0c0cdf9fdb0a.jpeg?im_w=1200',
      },
      {
        id: '2',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-826800524178117410/original/6c358947-8f64-4725-a07c-a02356fcc45e.jpeg?im_w=1200',
      },
      {
        id: '3',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-826800524178117410/original/4ab49b88-6c3c-4855-a48f-e3993ef7f2c7.jpeg?im_w=1200',
      },
      {
        id: '4',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-826800524178117410/original/fe7510d0-75f4-4602-b16d-a4488ee08881.jpeg?im_w=1200',
      },
      {
        id: '5',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-826800524178117410/original/4d9f6879-a76a-4267-97bc-f393447d8195.jpeg?im_w=1200',
      },
      {
        id: '6',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-826800524178117410/original/95d4164a-6e04-4988-bb67-76a17c5c08ba.jpeg?im_w=720',
      },
    ],
    description: `
The space

This golf retreat in tropical Barbados inspires instant relaxation. The elegant interior is designed with warm hues and is drenched in natural light. Plunge into the refreshing pool before sunbathing on the spacious stone terrace. Lush foliage exudes natural beauty around the property as birds soar overhead. Sprawl out at the beach, explore local trails, or savour a taste of Caribbean cuisine.

BEDROOM & BATHROOM
• Bedroom 1 - Primary: King size bed, Ensuite bathroom with stand-alone shower & bathtub
• Bedroom 2: Queen size bed, Ensuite bathroom with stand-alone shower
• Bedroom 3: Queen size bed, Ensuite bathroom with stand-alone shower


STAFF & SERVICES

Extra Cost (advance notice may be required):
• Activities and excursions
    `,
    rating: 4.8,
    reviews: 15,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],

    host: {
      name: 'James',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 1800,
  },

  {
    id: 21549952,
    name: 'Villa Madera',
    category: 'hotels',
    price: 2012,
    location: 'Guanacaste Province, Costa Rica',
    geoLocation: {
      lat: 10.630573,
      lng: -85.439346,
    },
    images: [
      {
        id: '1',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-751335671048162395/original/f74e3a33-2a0d-4f8a-813d-e663a59139c6.jpeg?im_w=1200',
      },
      {
        id: '2',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-751335671048162395/original/4d5e2bb0-fd27-4279-b844-4d79bd4d8c53.jpeg?im_w=1200',
      },
      {
        id: '3',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-751335671048162395/original/f6b523c6-9fea-4f76-b8e7-6d8f42987dac.jpeg?im_w=1200',
      },
      {
        id: '4',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-751335671048162395/original/c36b19e9-2112-44b2-bf4a-ac1bef7d51a4.jpeg?im_w=1200',
      },
      {
        id: '5',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-751335671048162395/original/1705be8d-e17b-421e-a2ec-84e897d07c40.jpeg?im_w=1200',
      },
      {
        id: '6',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-751335671048162395/original/996b91fc-2b4e-4ae4-8782-57c6ed76af8a.jpeg?im_w=1200',
      },
    ],
    description: `
The space

Please note that this home is in close proximity to an ongoing construction project. Please inquire with a Trip Designer for more information.  

Islands beckon in the views from this glassed-in modern home in the hills above Hermosa Beach. Breezes off the Gulf of Papagayo sway a hammock next to the infinity pool, hot tub, and outdoor kitchen. Details like clerestory windows flood rooms with light, while wood paneling warms the sleek spaces. It’s 5 minutes to shops and restaurants in the village, and the tropical forest–fringed beach.
    `,
    rating: 4.9,
    reviews: 60,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Pets allowed',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],

    host: {
      name: 'Daniel',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 10112,
  },
  {
    id: 33989876,
    name: 'Water Bungalow Over Stilt',
    category: 'Resort',
    price: 963,
    location: 'Malé, Kaafu Atoll, Maldives',
    geoLocation: {
      lat: 4.175496,
      lng: 73.509347,
    },
    images: [
      {
        id: '1',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-852918482922249722/original/e9b05964-aa31-4af7-886a-211a07133127.jpeg?im_w=1200',
      },
      {
        id: '2',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-852899544635683289/original/c627f47e-8ca9-4471-90d4-1fd987dd2362.jpeg?im_w=1200',
      },
      {
        id: '3',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-852899544635683289/original/18fdf350-65a1-4a19-88ce-5902e251ea8e.jpeg?im_w=720',
      },
      {
        id: '4',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-852899544635683289/original/20d45b9a-5701-41ba-91d0-5935be1d6336.jpeg?im_w=720',
      },
      {
        id: '5',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-852918482922249722/original/71c84b84-2a68-4676-a7d7-ded763fb6e25.jpeg?im_w=1200',
      },
      {
        id: '6',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-698416417074337858/original/a398b390-57fe-4c88-9d07-dfcc9af0ba17.jpeg?im_w=1440',
      },
    ],
    description: `
A modern Maldivian resort escape for world-class travellers in search of adventure, adrenaline and a truly remarkable holiday
experience.

> Water Bungalow at 4 star private Island Resort

> Entire Place

> Place is accessible by 45 minutes speedboat ride,

> 63 SQM

> A number of Excursions & activities available

> Maximum Occupancy 2 Adults 1 Child or 3 Adults

Kindly, ping me before sending reservation request to arrange transportation to & from Male International Airport.`,
    rating: 4.6,
    reviews: 48,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],
    host: {
      name: 'Mikel',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 3792,
  },
  {
    id: 48236284,
    name: 'Villa Korcula Diamond',
    category: 'villas',
    price: 4401,
    location: 'Luxury stay in Split Riviera, Croatia',
    geoLocation: {
      lat: 42.962132,
      lng: 17.135351,
    },
    images: [
      {
        id: '1',
        image:
          'https://a0.muscache.com/im/pictures/929bb70a-a677-4b8d-9848-3bb428e12dd8.jpg?im_w=1200',
      },
      {
        id: '2',
        image:
          'https://a0.muscache.com/im/pictures/5ae0b75e-6138-43bd-9c43-d9e2fc7c4409.jpg?im_w=720',
      },
      {
        id: '3',
        image:
          'https://a0.muscache.com/im/pictures/2df95748-0258-4ddc-8f23-181d1d5d119b.jpg?im_w=1200',
      },
      {
        id: '4',
        image:
          'https://a0.muscache.com/im/pictures/bf32aa2b-a3a6-4800-9a40-ac24d0a9b8a5.jpg?im_w=720',
      },
      {
        id: '5',
        image:
          'https://a0.muscache.com/im/pictures/fec6f091-9f3c-49bb-8dcf-cbb1f6bc47e6.jpg?im_w=1200',
      },
      {
        id: '6',
        image:
          'https://a0.muscache.com/im/pictures/62187b83-7654-4c62-bde4-87cbe76ea757.jpg?im_w=1200',
      },
    ],
    description: `
Look out over dramatic mountain and sea views at this modern architectural masterpiece on the secluded island of Korcula in Croatia. Spend your afternoons lounging poolside, working on your tan from one of the custom designed sun chairs. At night, head 3 kilometers into downtown Korcula for dinner at one of their best seafood restaurants.

Upper Unit
• Bedroom 1: King size bed, Ensuite bathroom with stand-alone rain shower and alfresco bathtub, Dual vanity, Television, Sound system, Safe, Shared balcony with outdoor furniture
• Bedroom 2: King size bed, Ensuite bathroom with stand-alone rain shower, Television, Sound system, Safe, Shared balcony with outdoor furniture
• Bedroom 3: King size bed, Ensuite bathroom with stand-alone rain shower, Television, Sound system, Safe, Shared balcony with outdoor furniture
`,
    rating: 4.9,
    reviews: 59,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],
    host: {
      name: 'Nitza',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 1570,
  },
  {
    id: 52369883,
    name: 'Chalet Black Squirrel',
    category: 'cottages',
    price: 3772,
    location: 'Chamonix-Mont-Blanc, Auvergne-Rhône-Alpes, France',
    geoLocation: {
      lat: 45.92237091064453,
      lng: 6.8676886558532715,
    },
    images: [
      {
        id: '1',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-831741423443173223/original/e645468b-84e5-4cdc-9b8a-26cda79d8e8d.jpeg?im_w=1200',
      },
      {
        id: '2',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-831741423443173223/original/256f9f8c-5ea6-4385-bcfe-84195a1b5ce0.jpeg?im_w=1200',
      },
      {
        id: '3',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-831741423443173223/original/e787f251-7003-4249-90fd-1e3c6d51445a.jpeg?im_w=1200',
      },
      {
        id: '4',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-831741423443173223/original/70776550-2b3a-4171-a3be-d70a19f301f0.jpeg?im_w=1200',
      },
      {
        id: '5',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-831741423443173223/original/32a5b731-5a67-42e2-a310-1b95945108b6.jpeg?im_w=1200',
      },
      {
        id: '6',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-831741423443173223/original/9d59ce35-3361-4e68-beb4-42413e42f1a4.jpeg?im_w=1200',
      },
      {
        id: '7',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-831741423443173223/original/e51d5e46-d6ee-4798-a28a-97551da63b27.jpeg?im_w=1200',
      },
    ],
    description: `
Nestled in the exclusive Les Bois neighborhood, this luxurious ski lodge captures soaring views of Mont Blanc Massif from its spacious double-height living room. You’ll find the Flegere Cable Car three minutes away, which will have you on the slopes in no time. After an exciting day, relax in the hot tub, sauna, or pool. Later, curl up with someone special by the wood-burning fireplace. 

The space

BEDROOM & BATHROOM
• Bedroom 1 - Primary: Double size bed, Ensuite bathroom with stand-alone shower, Television
• Bedroom 2: Double size bed (can be converted to 2 Twins), Ensuite bathroom with stand-alone shower, Television
• Bedroom 3: Double size bed (can be converted to 2 Twins), Ensuite bathroom with stand-alone shower, Television
• Bedroom 4: 2 Twin size bunk beds, Ensuite bathroom with stand-alone shower, Television
• Bedroom 5: Double size bed, Ensuite bathroom with stand-alone shower & bathtub, Television
    `,
    rating: 4.7,
    reviews: 83,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],
    host: {
      name: 'Pier',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 5230,
  },

  {
    id: 64617451,
    name: 'Waterfront Mansion, Prince Island',
    category: 'hotels',
    price: 1275,
    location: 'Adalar, İstanbul, Turkey',
    geoLocation: {
      lat: 40.8741659,
      lng: 29.1293251,
    },
    images: [
      {
        id: '1',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-705465779536586583/original/2c77c97b-a9bd-43ee-89a9-6e8c7f0d1856.jpeg?im_w=1200',
      },
      {
        id: '2',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-705465779536586583/original/7ebc7673-9923-4aa5-b76e-90565f02f0f3.jpeg?im_w=720',
      },
      {
        id: '3',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-705465779536586583/original/23929ae1-74b0-4da7-afcb-024cbb1ab633.jpeg?im_w=1200',
      },
      {
        id: '4',
        image:
          'https://a0.muscache.com/im/pictures/9a196fd3-15e9-40bd-b655-9a766a121801.jpg?im_w=720',
      },
      {
        id: '5',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-705465779536586583/original/baf5816c-c510-4fe6-8575-1c795eda7018.jpeg?im_w=720',
      },
      {
        id: '6',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-705465779536586583/original/e47ff42e-d135-4e18-b2e6-40e34cf528a4.jpeg?im_w=720',
      },
      {
        id: '7',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-705465779536586583/original/b04da629-ccc2-41a8-8f3a-2a3d93054f8f.jpeg?im_w=720',
      },
    ],
    description: `
Here is one of the greatest mansions of Prince Island, Istanbul. 

9 bedrooms, 
8 bathrooms, 
4 different gardens, 
2 spacious terraces, 
Turkish Hamam
Pool, and a special port for the house with water access and an amazing view of Istanbul. 

30 mins distance to Istanbul by ferry. You can enjoy both Istanbul and the island experience at the same time. Create unforgettable memories here at Valentin Mansion.
The space
There is a dedicated gardener for the gardens and port. The staff who have worked for this mansion for over 7 years will be around for any need and everyone on the island are super friendly. 

The mansion is cleaned just before you come. Also, cleaning can be provided if needed

There is a laundry and ironing room inside the house.
Vintage game room (Wii, ps2...)
10-person indoor dining table
14-person outdoor dining table
Playroom for kids
A dedicated workspace
1 bathhouse in the house
2 fireplaces
Outdoor plunge pool and lounge area
Pets are allowed in the garden only
20-minute walk to the center of the Island.

    `,
    rating: 4.4,
    reviews: 31,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],
    host: {
      name: 'Mohamed',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 1034,
  },
  {
    id: 91243765,
    name: 'Hayatt Regency',
    category: 'hotels',
    price: 324,
    location: 'Sofia, Bulgaria',
    geoLocation: {
      lat: 42.6629,
      lng: 23.3238,
    },
    images: [
      {
        id: '1',
        image:
          'https://assets.hyatt.com/content/dam/hyatt/hyattdam/images/2020/11/09/0401/Hyatt-Regency-Sofia-P069-Reception.jpg/Hyatt-Regency-Sofia-P069-Reception.16x9.jpg?imwidth=1280',
      },
      {
        id: '2',
        image:
          'https://assets.hyatt.com/content/dam/hyatt/hyattdam/images/2020/11/09/0403/Hyatt-Regency-Sofia-P073-Regency-Club-Fireplace.jpg/Hyatt-Regency-Sofia-P073-Regency-Club-Fireplace.16x9.jpg?imwidth=1280',
      },
      {
        id: '3',
        image:
          'https://assets.hyatt.com/content/dam/hyatt/hyattdam/images/2020/11/09/0403/Hyatt-Regency-Sofia-P071-Lobby.jpg/Hyatt-Regency-Sofia-P071-Lobby.16x9.jpg?imwidth=1280',
      },
      {
        id: '4',
        image:
          'https://assets.hyatt.com/content/dam/hyatt/hyattdam/images/2020/11/09/0401/Hyatt-Regency-Sofia-P068-Night-Exterior.jpg/Hyatt-Regency-Sofia-P068-Night-Exterior.16x9.jpg?imwidth=1280',
      },
      {
        id: '5',
        image:
          'https://assets.hyatt.com/content/dam/hyatt/hyattdam/images/2020/11/09/0406/Hyatt-Regency-Sofia-P076-The-Conservatory.jpg/Hyatt-Regency-Sofia-P076-The-Conservatory.16x9.jpg?imwidth=1280',
      },
      {
        id: '6',
        image:
          'https://assets.hyatt.com/content/dam/hyatt/hyattdam/images/2020/11/09/0410/Hyatt-Regency-Sofia-P087-Rooftop-Terrace.jpg/Hyatt-Regency-Sofia-P087-Rooftop-Terrace.16x9.jpg?imwidth=1280',
      },
    ],
    description: `
Hyatt Regency Sofia is located within the heart of the city on Vasil Levski Square. It is ideal for exploring and encountering a modern city surrounded by authentic Eastern European history. Set within walking distance of many of Sofia’s cultural institutions and tourist attractions, you are never far away from an unforgettable experience.

Hyatt Regency Sofia features 183 well-appointed guest rooms and suites, many of which include Regency Club access rooms and beautiful views of the lush and green courtyard. The bathrooms provide the perfect place for one to unwind and refresh.
`,
    rating: 4.8,
    reviews: 243,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],
    host: {
      name: 'Hayatt Regency',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 6980,
  },

  {
    id: 75604398,
    name: 'Traditional Serbian homestay " Stanojevic"',
    category: 'hotels',
    price: 150,
    location: 'Boljevac, Serbia',
    geoLocation: {
      lat: 43.8304776,
      lng: 21.9587438,
    },
    images: [
      {
        id: '1',
        image:
          'https://a0.muscache.com/im/pictures/048e3bce-62be-4d14-a2af-5a5a034a627e.jpg?im_w=960',
      },
      {
        id: '2',
        image:
          'https://a0.muscache.com/im/pictures/c29f59ed-d4d6-42e1-bc0b-5c95db78c587.jpg?im_w=1200',
      },
      {
        id: '3',
        image:
          'https://a0.muscache.com/im/pictures/6a76f986-21c1-4df3-b684-382e6f9aa9d3.jpg?im_w=1200',
      },
      {
        id: '4',
        image:
          'https://a0.muscache.com/im/pictures/987a4ea3-a5bc-4fd4-b828-5734c7387f0c.jpg?im_w=1200',
      },
      {
        id: '5',
        image:
          'https://a0.muscache.com/im/pictures/8c02dc74-ea62-46b7-b01b-a512e0f03e80.jpg?im_w=1200',
      },
      {
        id: '6',
        image:
          'https://a0.muscache.com/im/pictures/204d5755-d96c-48c6-9a2c-b3f9dacef680.jpg?im_w=1200',
      },
    ],
    description: `
Etno House Stanojevic is an ideal vacation home that brings you authentic charm and magic of Eastern Serbia.

Thanks to the love Zika Stanojevic had for his homeland and its past made it possible for him to preserve his birth-house and protect it from being forgotten. He managed to transfer all that love to his family. Today we open our doors to you!

Welcome to the Stanojevic Family!

The space

The "Stanojevic" etno house is located in Boljevac in eastern Serbia in the valley of the Crni Timok River in Timok region, dwelling among mountain ranges of the Kucaj Mountains, Mt.Rtanj and Mt.Tupiznica.

Etno House Stanojevic offers 4 bedrooms, 8 beds, a kitchen, bathroom, TV and free internet. There is a historical museum within the household, containing traditional items and handcrafts which revive memories, turning you 500 years back to the rich history of the Balkans, which is also used as a dining room. The visitors are free to use the swiming pool in the back yard as well as a football pitch, basketball court and childrens playground comprising a slide, a teeter, swings etc.
    `,

    rating: 4.5,
    reviews: 76,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],
    host: {
      name: 'Stanojevic',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },

    views: 1470,
  },

  {
    id: 94673982,
    name: 'Country House Djurisic',
    category: 'villages',
    price: 150,
    location: 'Virpazar, Bar, Montenegro',
    geoLocation: {
      lat: 42.2462724,
      lng: 19.0906057,
    },
    images: [
      {
        id: '1',
        image:
          'https://a0.muscache.com/im/pictures/9e43636f-94e7-4982-bd78-a16e667c6ef6.jpg?im_w=1200',
      },
      {
        id: '2',
        image:
          'https://a0.muscache.com/im/pictures/181718bf-f1f3-4da6-a564-afce13be740c.jpg?im_w=1200',
      },
      {
        id: '3',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-9725424/original/5e7c50f6-ab46-4318-9c5a-5accd2edfe36.jpeg?im_w=1200',
      },
      {
        id: '4',
        image:
          'https://a0.muscache.com/im/pictures/3cc169e8-2f44-4527-b482-5fe5eaecb5c2.jpg?im_w=720',
      },
      {
        id: '5',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-9725424/original/d6ea8632-cc7e-46f5-956b-16c671a82c5a.jpeg?im_w=1200',
      },
      {
        id: '6',
        image:
          'https://a0.muscache.com/im/pictures/f952b05d-e22b-4142-8928-f9b7a55dc813.jpg?im_w=1200',
      },
    ],
    description: `
The story of Country House Djurisic begins at our family estate. It is the estate of family Đurišić, which has been here in Skadar Lake region for more than 500 years. For centuries taking care of estate has been passed from generation to generation. The estate extends to 3000 m2 and there is abundance of various plant species and most of all vines, out of which family produces vine for 5 centuries.The spring with clean and fresh water is also a part of this estate.There are also family wineries, which are one of the most beautiful in the region, one is on top of the estate and the other in the house, which is envisaged to accommodate guests.The family is also engaged in hunting and fish processing, which is distributed in the largest markets throughout the country. What you can expect is that the family Đurišić will do everything to make your holiday be the one to remember, one of the best in your life. We will help you get to know our culture, history, traditions through our eyes, the eyes of the local people. We organize various activities: kayaking, hiking, bird watching, boat trips...We also preparing amazing dinner for our quests, with ingredients from our own garden.
    `,

    rating: 4.4,
    reviews: 16,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],
    host: {
      name: 'Country House Djurisic',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 3311,
  },
  {
    id: 84956734,
    name: 'Royal Spa VILLA LA VIDA LOCA',
    category: 'hotels',
    price: 1184,
    location: 'Playa del Carmen, Quintana Roo, Mexico',
    geoLocation: {
      lat: 20.6308643,
      lng: -87.0779503,
    },
    images: [
      {
        id: '1',
        image:
          'https://a0.muscache.com/im/pictures/619f3a07-ebaf-419d-9199-c30d4576db26.jpg?im_w=1200',
      },
      {
        id: '2',
        image:
          'https://a0.muscache.com/im/pictures/88b4bed3-b5e0-46da-8d02-c24e3036a6c3.jpg?im_w=1200',
      },
      {
        id: '3',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-29205221/original/15667870-8689-42de-a92b-414d9f74ee5a.jpeg?im_w=1200',
      },
      {
        id: '4',
        image:
          'https://a0.muscache.com/im/pictures/ad80a0a2-820a-4fe6-b44c-c848acf6dc25.jpg?im_w=1200',
      },
      {
        id: '5',
        image:
          'https://a0.muscache.com/im/pictures/2ba6f4fa-d88f-49e6-b4fb-456ff4def5cf.jpg?im_w=1200',
      },
      {
        id: '6',
        image:
          'https://a0.muscache.com/im/pictures/5f727fef-a48f-4e09-8302-005dd1c51709.jpg?im_w=1200',
      },
    ],
    description: `
Villa La Vida Loca" is a private, residential vacation home located in the most exclusive area of Playa del Carmen, Playacar. This home is peaceful and quiet, with lots of room for entertainment, private pool, yet only a few minutes walk from beach and town. 

This 5 bedroom vacation home sleeps up to 14 people.

We can arrange airport transfer, private Chef services, home spa and many other things you may like to do. Just ask!

MAID SERVICE INCLUDED

SECURED GATED COMMUNITY

The space

Beautifully designed and executed by local craftsmen, Villa La Vida Loca has 5 large bedrooms and 6.5 baths, a den with 2 full futons, an office with desk and sofa bed, several terraces, and a private yard with a giant pool.

The palm tree theme is evident everywhere - from the carving on the front door to the unique wrought iron railings. Enter into the grand foyer, with a view through dining and living room right out to the pool. The private outdoor area with a Spanish design wall and mature landscaping leads to an intimate green space for hammocks in the shade on the other side of the waterfall. Two covered outdoor rooms surround the pool, with an outdoor bar and grill area to supplement the full kitchen with eat-in island.
    `,

    rating: 4.8,
    reviews: 163,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],
    host: {
      name: 'VILLA LA VIDA LOCA ',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 6834,
  },

  {
    id: 84956777,
    name: 'Villa - San Vito lo Capo',
    category: 'villas',
    price: 224,
    location: 'San Vito Lo Capo, Sicilia, Italy',
    geoLocation: {
      lat: 38.172157287597656,
      lng: 12.734055519104004,
    },
    images: [
      {
        id: '1',
        image:
          'https://a0.muscache.com/im/pictures/7ad80cd3-a383-4e07-8e8b-4438ff759adc.jpg?im_w=1200',
      },
      {
        id: '2',
        image:
          'https://a0.muscache.com/im/pictures/bc77bc77-41a4-4308-891f-b9fe5132a9a1.jpg?im_w=1200',
      },
      {
        id: '3',
        image:
          'https://a0.muscache.com/im/pictures/81d8984c-efae-4733-bd43-131aded7813c.jpg?im_w=1200',
      },
      {
        id: '4',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-33516283/original/e76df769-9349-42a6-a69e-413d7c5ed297.jpeg?im_w=1200',
      },
      {
        id: '5',
        image:
          'https://a0.muscache.com/im/pictures/83be2c3b-09ad-49d7-9d45-519d5ad5a8af.jpg?im_w=720',
      },
      {
        id: '6',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-48460088/original/c3cb1e02-bdbf-4368-ad85-56f82ef48312.jpeg?im_w=1200',
      },
    ],
    description: `
Villa Zingaro - San Vito Lo Capo is a magnificent stone house located along the coast that extends from the Zingaro reserve to San Vito Lo Capo, consisting of two apartments, independent and immersed in the Mediterranean scrub with the possibility of reaching Cala Firriato on foot.
    
The space
    
Two apartments, located in a single rural structure in living stone, typical of the "firriato" area, independent and immersed in the Mediterranean scrub. Located along the scenic route that connects the tourist village of San Vito Lo Capo to the orientation of the Zingaro natural reserve, the two houses, which are in a single structure, have one of 4 beds (Villa Scupazzo) and the other with 2 seats (Villa Giummara) with the possibility of adding brandina/crib, offer a breathtaking view of the sea by extending the view on a golf course that goes from the Tonnara del Secco to the small bay where the "Lago di Venere" is located and where often they dock you, also famous boats to enjoy the beautiful sea and the vegetation offered by the enchanting cliffside coast.
    `,

    rating: 4.6,
    reviews: 112,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],
    host: {
      name: 'Villa Scupazzo Zingaro - San Vito lo Capo',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 3724,
  },

  {
    id: 84958888,
    name: 'Casa Hacienda "Villa Mercedes"',
    category: 'villas',
    price: 82,
    location: 'San Vito Lo Capo, Sicilia, Italy',
    geoLocation: {
      lat: -13.5170887,
      lng: -71.9785356,
    },
    images: [
      {
        id: '1',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-20191792/original/1d17a02a-51c5-4146-88a6-751b990222fd.jpeg?im_w=1200',
      },
      {
        id: '2',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-20191792/original/1fb5d237-535f-4558-b458-efb293ada9fa.jpeg?im_w=720',
      },
      {
        id: '3',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-20191792/original/80347923-6160-467b-8628-d36cacf1d605.jpeg?im_w=720',
      },
      {
        id: '4',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-20191792/original/68a4d4e7-0158-4f62-aa40-7116f08ec554.jpeg?im_w=1200',
      },
      {
        id: '5',
        image:
          'https://a0.muscache.com/im/pictures/87403657-e37b-4da6-aa08-55a415247f9e.jpg?im_w=1200',
      },
      {
        id: '6',
        image:
          'https://a0.muscache.com/im/pictures/miso/Hosting-20191792/original/9d793344-d34c-49fc-a7c6-0f4e803b90fa.jpeg?im_w=1200',
      },
    ],
    description: `
The space
    
Villa Mercedes is located at the beginning of the Sacred Valley of the Incas, in a magical and charming environment surrounded by nature and vegetation, ideal for spending days relaxing and starting your visit to the Inca ruins.
    
Guest access
    
Private museum, colonial hall, kitchen, games and green areas.
    
Other things to note
    
We have rooms inside the house and also bungalows that are located in an elevated part of the house, which makes the view of the sacred valley perfect and allows total privacy surrounded by nature.
    `,

    rating: 4.6,
    reviews: 173,
    amenities: [
      'Air conditioning',
      'Kitchen',
      'Wifi',
      'Free parking',
      'Pool',
      'Washer',
    ],
    host: {
      name: 'Casa Hacienda "Villa Mercedes"',
      isSuperhost: true,
      avatar:
        'https://a0.muscache.com/im/pictures/user/2f5a4e4a-6c7b-4f8e-9a1a-9e2e8f4f8b7b.jpg?im_w=240',
    },
    views: 4723,
  },
];




