export const userSearchOptions = [
    {name: 'handle', label: 'Username'},
    {name: 'email', label: 'Email'},
    {name: 'phoneNumber', label: 'PhoneNumber'},
  ];

export const eventImage =
  'https://img.freepik.com/premium-vector/meeting-office-interior-business-conference-room-with-people-managers-working-team-cartoon-interior_80590-7766.jpg?w=2000';