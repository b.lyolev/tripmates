export const pageTitles = Object.freeze({
  landing: 'TripMates - Explore the world with your friends',

  home: 'АZmood - Начало',
  events: 'TripMates - Events',
  createEvent: 'АZmood - Създайте събитие',
  weather: 'AZmood - Времето',
  contacts: 'АZmood - Контакти',
  views: 'АZmood - Гледки',
});
