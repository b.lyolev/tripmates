import { Route, Routes, useLocation } from 'react-router-dom';
import { useAuthState } from 'react-firebase-hooks/auth';
import { app, auth } from './config/firebase-config';
import { useEffect, useState } from 'react';
import MonthView from './components/Calendar/MonthView/MonthView';
import { getAllUsers, getUserData } from './services/user.service';
import Events from './views/Events/Events';
import NavBar from './components/NavBar/NavBar';
import Contacts from './views/Contacts/contacts';
import ImageGrid from './components/ImageGrid/ImageGrid';
import { useDispatch, useSelector } from 'react-redux';
import { setUser, setUserData } from './reducers/authActions';
import useEventFetching from './hooks/useEventFetching';
import HotelBookingForm from './components/ImageGrid/HotelBookingForm';
import HotelPage from './components/ImageGrid/HotelPage';
// import EditProfile from './components/EditProfile/EditProfile';
import Home from './views/Home/Home';
import Footer from './components/Footer/Footer';
import Calendar from './views/Calendar/Calendar';
import WeekView from './components/Calendar/WeekView/WeekView';
import Profile from './views/Profile/Profile';
import AboutUs from './views/About/AboutUs';
import useAuthData from './hooks/useAuthData';
import useAllUsersFetching from './hooks/useAllUsersFetch';
import useAllRequestsFetch from './hooks/useAllRequestsFetch';
import DayView from './components/Calendar/DayView/DayView';
import DayViewWithMonth from './components/Calendar/DayView/DayViewWithMonth';
import InvalidPage from './views/InvalidPage/InvalidPage';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App() {
  const [userLoaded, loading] = useAuthState(auth);
  const [isInitialized, setIsInitialized] = useState(false);
  const user = useSelector((state) => state.auth.user);

  const dispatch = useDispatch();

  useEffect(() => {
    if (loading) return;
    setIsInitialized(true);
  }, [loading]);

  const { userData } = useAuthData(userLoaded);
  const allUsers = useSelector((state) => state.allUsers);

  useEventFetching(userData);
  useAllRequestsFetch(userData);
  useAllUsersFetching();

  const location = useLocation();
  useEffect(() => {
    const routeTitles = {
      '/': 'TripMates - Explore the world with your friends',
      '/events': 'Events - TripMates',
      '/calendar/month-view': 'Calendar Month View - TripMates',
      '/calendar/week-view': 'Calendar Week View - TripMates',
      '/calendar/day-view': 'Calendar Day View - TripMates',
      '/profile': 'Profile - TripMates',
      '/friends': 'Friends - TripMates',
      '/about-us': 'About Us - TripMates',
      '/hotel-page/': 'Hotel - TripMates',
    };

    const currentPath = window.location.pathname;

    if (routeTitles[currentPath]) {
      document.title = routeTitles[currentPath];
    }
  }, [location]);

  return (
    <>
      {!loading && (
        <>
          <NavBar />
          <ToastContainer position="bottom-right" autoClose={2000} theme='dark' />
          <Routes>
            <Route index element={<Home />} />
            {/* <Route exact path='/calendar/week-view' element={<Calendar />} /> */}
            <Route
              exact
              path='/events'
              element={user ? <Events /> : <InvalidPage />}
            />
            <Route exact path='/calendar/month-view' element={user ? <MonthView /> : <InvalidPage/>} />
            <Route exact path='/calendar/week-view' element={user ? <WeekView /> : <InvalidPage/>} />
            <Route
              exact
              path='/calendar/day-view'
              element={user ? <DayViewWithMonth /> : <InvalidPage/>}
            />
            <Route exact path='/profile' element={user ? <Profile /> : <InvalidPage/>} />
            <Route exact path='/hotel-page/:hotelId' element={user ? <HotelPage /> : <InvalidPage/>} />
            <Route exact path='/friends' element={user ? <Contacts /> : <InvalidPage/>} />
            <Route exact path='/about-us' element={<AboutUs />} />
            <Route
              exact
              path='/date-picker'
              element={ user ?
                (<div className='min-h-screen flex items-center justify-center bg-gray-100'>
                  <HotelBookingForm />
                </div>) : <InvalidPage/>
              }
            />
            <Route path='*' element={<InvalidPage />} />
          </Routes>
          <Footer />
        </>
      )}
    </>
  );
}

export default App;
