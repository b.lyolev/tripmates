export function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export function isDifferentDay(startDate, endDate) {
  return (
    startDate.getDate() !== endDate.getDate() ||
    startDate.getMonth() !== endDate.getMonth() ||
    startDate.getFullYear() !== endDate.getFullYear()
  );
}

export function calculateMinTime(startDate, endDate) {
  if (startDate && endDate && !isDifferentDay(startDate, endDate)) {
    return new Date(startDate.getTime() + 30 * 60 * 1000);
  }
  return null;
}

export function calculateMaxTime(startDate) {
  if (startDate) {
    const maxEndDate = new Date(startDate.getTime() + 24 * 60 * 60 * 1000);
    return new Date(maxEndDate.setHours(23, 59, 59, 999)); // Set to the end of the day
  }
  return null;
}

export function unixTimeToCustomFormat(unixTimestamp, timezoneOffset = 0) {
  // Input validation
  if (isNaN(unixTimestamp)) {
    return 'Invalid timestamp';
  }

  const days = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
  const months = [
    'JAN',
    'FEB',
    'MAR',
    'APR',
    'MAY',
    'JUN',
    'JUL',
    'AUG',
    'SEP',
    'OCT',
    'NOV',
    'DEC',
  ];

  // Adjust timestamp for the timezone offset
  const adjustedTimestamp = unixTimestamp + timezoneOffset * 60 * 60 * 1000;

  const date = new Date(adjustedTimestamp);
  const dayOfWeek = days[date.getUTCDay()];
  const dayOfMonth = date.getUTCDate().toString().padStart(2, '0');
  const monthName = months[date.getUTCMonth()];
  const year = date.getFullYear();
  const hours = date.getUTCHours().toString().padStart(2, '0');
  const minutes = date.getUTCMinutes().toString().padStart(2, '0');

  const formattedTime = `${dayOfMonth} ${monthName} ${year} | ${hours}:${minutes}`;

  return formattedTime;
}

export function formatTime(dateTime) {
  const dateObject = new Date(dateTime);
  let hours = dateObject.getHours();
  let minutes = dateObject.getMinutes();

  if (hours === 24) {
    hours = 0;
  }

  const formattedHours = hours.toString().padStart(2, '0');
  const formattedMinutes = minutes.toString().padStart(2, '0');

  return `${formattedHours}:${formattedMinutes}`;
}

export function formatDate(dateTime) {
  return new Date(dateTime).toLocaleDateString('en-US', {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
  });
}
