// utils/validationRules.js

export function validateFirstName(value) {
  if (value.length < 1 || value.length > 30) {
    return 'First name must be between 1 and 30 characters!';
  }
  if (!/^[A-Za-z]+$/.test(value)) {
    return 'First name can only contain letters!';
  }
  return '';
}

export function validateLastName(value) {
  if (value.length < 1 || value.length > 30) {
    return 'Last name must be between 1 and 30 characters!';
  }
  if (!/^[A-Za-z\s]+$/.test(value)) {
    return 'Last name can only contain letters and spaces!';
  }
  return '';
}

export function validateEmail(value) {
  if (!value) {
    return 'Please enter an email!';
  }
  if (!/^\S+@\S+\.\S+$/.test(value)) {
    return 'Please enter a valid email address!';
  }
  return '';
}

export function validatePassword(value) {
  if (value.length < 8 || value.length > 30) {
    return 'Password should be between 8 and 30 characters!';
  }
  if (!/[A-Za-z]/.test(value)) {
    return 'Password should contain at least one letter!';
  }
  if (!/\d/.test(value)) {
    return 'Password should contain at least one digit!';
  }
  if (!/[!@#$%^&.*]/.test(value)) {
    return 'Password should contain at least one special character!';
  }
  return '';
}

export function validateHandle(value) {
  if (value.length < 3) {
    return 'Username should be at least 3 characters long!';
  }
  if (!/^[A-Za-z0-9_]+$/.test(value)) {
    return 'Username can only contain letters, digits, and underscores!';
  }
  return '';
}

export function validatePhoneNumber(value) {
  if (value.length !== 10) {
    return 'Phone number should be exactly 10 digits long!';
  }
  if (!/^[0-9]+$/.test(value)) {
    return 'Phone number can only contain digits!';
  }
  return '';
}

export function validateTitle(value) {
  if (value.length < 3) {
    return 'Title should be at least 3 characters long!';
  }
  if (value.length > 30) {
    return 'Title should be less than 30 characters long!';
  }
  return '';
}

export function validateDescription(value) {
  if (value.length > 500) {
    return 'Description cannot be longer than 500 symbols long!';
  }
  if (value.length < 3) {
    return 'Description should be at least 3 characters long!';
  }
  return '';
}

export function validateEventDate(startDate, endDate) {
  if (!startDate) {
    return 'Start date is required!';
  }
  if (!endDate) {
    return 'End date is required!';
  }
  if (startDate > endDate) {
    return 'Start date cannot be after end date!';
  }

  return '';
}

export function isFormInvalid(validationMessages, form) {
  return (
    validationMessages.firstName ||
    validationMessages.lastName ||
    validationMessages.email ||
    validationMessages.password ||
    validationMessages.handle ||
    validationMessages.phoneNumber ||
    !form.firstName ||
    !form.lastName ||
    !form.email ||
    !form.password ||
    !form.handle ||
    !form.phoneNumber
  );
}

export function isEventValid(validationMessages, form) {
  return (
    validationMessages.title ||
    validationMessages.description ||
    validationMessages.startDate ||
    validationMessages.endDate ||
    validationMessages.eventDate ||
    !form.title ||
    !form.description ||
    !form.startDate ||
    !form.endDate
  );
}
