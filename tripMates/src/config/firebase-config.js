import { initializeApp } from 'firebase/app';
import { getAnalytics } from 'firebase/analytics';
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';
import { getStorage } from 'firebase/storage';

const firebaseConfig = {
  apiKey: 'AIzaSyCewqS2HgU4UFfCTbUYXkXj_UQGriM56EQ',
  authDomain: 'tripmates-eca93.firebaseapp.com',
  projectId: 'tripmates-eca93',
  storageBucket: 'tripmates-eca93.appspot.com',
  messagingSenderId: '1066393745058',
  appId: '1:1066393745058:web:d210f369091f024e0bc9fb',
  measurementId: 'G-EF419RXC3Y',
  databaseURL:
    'https://tripmates-eca93-default-rtdb.europe-west1.firebasedatabase.app/',
};

export const app = initializeApp(firebaseConfig);
export const analytics = getAnalytics(app);
export const auth = getAuth(app);
export const db = getDatabase(app);
export const storage = getStorage(app);
