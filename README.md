<h1 align="center">
    <a href="https://amplication.com/#gh-light-mode-only">
    <img src="./readme-images/logo-tripmates.png">
    </a>

</h1>

<p align="center">
  <i align="center">Bringing Friends, Adventures, and Personal Events Together. 🚀</i>
</p>

<p align="center">
    <img src="./readme-images/tripmates-promo.png" alt="calendar-month"/>
</p>

## Introduction

Welcome to `TripMates`, where we've reimagined the way you plan, organize, and experience group adventures and personal events. Our mission is to empower you to create unforgettable memories with friends and loved ones, without the hassle of traditional event coordination and travel planning.

At TripMates, we believe that every adventure should be accessible, personalized, and stress-free. That's why we've meticulously crafted an all-encompassing web application that seamlessly blends travel discovery, event coordination, and hotel booking into a single, user-friendly platform.

## Discover and Plan

<details open>
<summary><b>
Endless Adventures</b>
<br />
</summary>
<p>
Explore a world of events, from music festivals to private gatherings, all in one place. TripMates offers curated experiences tailored to your interests.
</p>

<p align="center">
    <img width="49%" src="./readme-images/homepage.gif" alt="apis"/>
&nbsp;
    <img width="49%" src="./readme-images/hotelpage.gif" alt="data-models"/>
</p>

</details>

<details open>
<summary>
 <b>Effortless Scheduling</b>
</summary>
<p>
Manage group adventures and personal events seamlessly with our interactive calendar.
Easily plan your own events, invite friends, and add details with our intuitive event creation form.
</p>

<p align="center">
    <img width="49%" src="./readme-images/monthview.png" alt="calendar-month-view"/>
&nbsp;
    <img width="49%" src="./readme-images/weekview.png" alt="calendar-week-view"/>
</p>
<p align="center">
    <img width="49%" src="./readme-images/dayview.png" alt="calendar-day-view"/>
&nbsp;
    <img width="49%" src="./readme-images/creating-event.png" alt="creating-event"/>
</p>
</details>

<details open>
<summary>
 <b>Connect with Friends</b>
</summary>
<p>
Search, add, and connect with friends. Share your adventures and plan together effortlessly.
</p>
<p align="center">
    <img src="./readme-images/friends-suggestions.png" alt="friends-suggestions"/>
</p>
</details>
<details open>
<summary>
 <b>Personalize Your Details</b>
</summary>
<p>
Customize your user profile with ease. Update your information, add a profile picture, and make your profile truly your own.
</p>
<p align="center">
    <img src="./readme-images/profile-details.png" alt="profile-details"/>
</p>
</details>
<details open>
<summary>
 <b>On-the-Go Planning</b>
</summary>
<p>
TripMates is fully mobile-optimized, allowing you to plan from anywhere.
</p>
<p align="center">
    <img src="./readme-images/mobile-version.gif" alt="mobile-version"/>
</p>
</details>
<details open>
<summary>
 <b>Customizable Themes</b>
</summary>
<p>
Choose from light or dark themes to match your style.
</p>
<p align="center">
    <img src="./readme-images/themes.png" alt="themes"/>
</p>
</details>

## Usage

### Hosted Version

To start using TripMates (Note: Hosted version coming soon), you can follow these steps:

1. Visit [tripmates.com](https://tripmates-eca93.web.app/)

2. After logging in, you'll be guided through the process of creating your first adventure or event.

<details open>
<summary>
  <b>Tutorials (Work in Progress)</b>
</summary>

- [Creating a Group Adventure with TripMates](#) (Under Development)
- [Planning a Personal Event on TripMates](#) (Under Development)
</details>

### Local Development

If you prefer to run TripMates locally for code generation or contributions, follow these steps:

<details open>

<summary>
  <b>Pre-requisites</b>
</summary>

Before you begin development on TripMates, ensure you have the following pre-requisites installed:

- **Node.js**: Make sure you have Node.js installed, preferably version 16 or above. You can download it from [nodejs.org](https://nodejs.org/).

- **Git**: You'll need Git for version control and collaboration. Download Git from [git-scm.com](https://git-scm.com/).

- **React**: Since TripMates is built with React, you should have a basic understanding of React development.

- **Redux**: Familiarize yourself with Redux, which is used for state management in the app. You can learn more about Redux at [redux.js.org](https://redux.js.org/).

- **Tailwind CSS**: TripMates uses Tailwind CSS for styling. Explore Tailwind CSS documentation at [tailwindcss.com](https://tailwindcss.com/).

</details>

For detailed instructions on setting up TripMates locally, please refer to our [Contributing Guide](#authors).

<details open>

<summary><b>
Running TripMates
</b>
</summary> <br />

TripMates uses the Vite build tool for fast and efficient development. Follow these steps to set up your local development environment.

###

1. Clone the TripMates repository and navigate to the project directory:

```shell
git clone https://gitlab.com/tripmates/tripmates.git && cd tripmates
```

2. Install project dependencies:

```shell
npm install
```

3. Start the local development server with Vite:

```shell
npm run dev
```

This command will launch the development server and open TripMates in your default web browser.

**The TripMates development environment is now set up with Vite.**
**Happy coding! 🚀**

</details>

## Authors

<a id="authors">

[//]: contributor-faces

<a href="https://gitlab.com/b.lyolev"><img title="Bozhidar Lyolev" src="./readme-images/Bozhidar-photo.png" title="yuval-hazaz" width="150" height="160"></a>
<a href="https://gitlab.com/RiazBhuiyan"><img src="./readme-images/Riaz-photo.jpg" title="Riaz Bhuiyan" width="120" height="160"></a>
<a href=""><img src="./readme-images/Valio-photo.jpg" title="Valentin Petkov" width="160" height="160"></a>

[//]: contributor-faces

</a>

## License

TripMates Custom License

By using this software, you agree to the following terms:

1. Usage Restrictions:
   You may not use, copy, modify, or distribute this software without first obtaining explicit written permission from the copyright holder, `Bozhidar Lyolev`, reachable at `b.lyolev@gmail.com`.

2. Contact Requirement:
   Contact `Bozhidar Lyolev` at `b.lyolev@gmail.com` to request permission before using, copying, modifying, or distributing this software for any purpose.

3. No Warranty:
   This software is provided "as is," without any warranty or guarantee of any kind.

4. Limitation of Liability:
   In no event shall `Bozhidar Lyolev` be liable for any claim or damages arising from the software or its use.

5. Reservation of Rights:
   `Bozhidar Lyolev` reserves all rights not expressly granted herein.

If you do not agree to these terms, you are not allowed to use, copy, modify, or distribute this software in any way. Contact `Bozhidar Lyolev` for inquiries regarding usage permissions.
